﻿using SQLiteDB.Initializers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLiteDB
{
    public class SqliteConnectionFactory : IDbConnectionFactory
    {
        public System.Data.Common.DbConnection CreateConnection(string nameOrConnectionString)
        {
            nameOrConnectionString = nameOrConnectionString.Replace("DQM.s3db", System.IO.Directory.GetCurrentDirectory() + "\\DB\\DQM.s3db");
            SQLiteConnection dbConnection = new SQLiteConnection(nameOrConnectionString);
            dbConnection.Open();
            return dbConnection;
            
        }
        public SqliteConnectionFactory()
        {
            
        }
    }
}
