﻿using DB;
using IMAIL.Models;
using Models;
using SQLiteDB.Helpers;
using SQLiteDB.Initializers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
namespace SQLiteDB
{
    public class SQLiteRepository<T> : DbContext, IContext<T> where T : BaseEntity
    {
        public SQLiteRepository(string connectionstring)
            : base(connectionstring)
        {
            var init = new CreateSQLiteDatabaseIfNotExists<SQLiteRepository<T>>();
            Database.SetInitializer(init);
            if (!FileHelper.CheckSQLiteFileExists(connectionstring))
            {
               // this.Database.Create();
                SQLIteHelper helper = new SQLIteHelper(connectionstring);
                helper.InitializeDB();
            }

        }
        public SQLiteRepository()
            : base("BaseDB")
        {
            this.Database.CreateIfNotExists();

        }
        DbSet<MailProfile> Profiles { get; set; }
        DbSet<EmailMessage> Emails { get; set; }
        DbSet<EmailAttachment> Attachments { get; set; }

        public T Add(T item)
        {

            this.Set<T>().Add(item);
            this.SaveChanges();
            return item;
        }

        public T Update(T item)
        {
            this.Entry(item).State = EntityState.Modified;
            this.SaveChanges();
            return item;
        }

        public bool Delete(T item)
        {
            this.Entry(item).State = EntityState.Deleted;
            this.SaveChanges();
            return true;
        }

        public T GetByID(int ID)
        {
            return this.Set<T>().Find(ID);
        }




        public IEnumerable<T> Get(Expression<Func<T, bool>> expression, Expression<Func<T, string>> orderby = null)
        {
            var query = this.Set<T>().Where(expression);
            if (orderby != null)
            {
                query = query.OrderBy(orderby);
            }
            return query.AsEnumerable();
        }

        public IEnumerable<T> GetPaged(Expression<Func<T, string>> orderby, Expression<Func<T, bool>> expression = null, int page = 1, int itemcount = 10)
        {
            var query = this.Set<T>().AsQueryable();
            if (expression != null)
            {
                query = query.Where(expression);
            }
            Type entityType = typeof(T);
            var items = query.OrderBy(orderby).Skip(page * itemcount).Take(itemcount).AsEnumerable().ToList();
            var entitySetElementType = ((System.Data.Entity.Infrastructure.IObjectContextAdapter)this).ObjectContext.CreateObjectSet<T>().EntitySet.ElementType;
            foreach (var navigationProperty in entitySetElementType.NavigationProperties)
            {
                items.ForEach(a => this.Entry(items).Collection(navigationProperty.Name).Load());
            }
            return items;

        }

        public T GetSingle(Expression<Func<T, bool>> expression)
        {
            return this.Set<T>().Single(expression);
        }

        public bool CreateDB(string connstring)
        {
            try
            {
                new SQLIteHelper(connstring).InitializeDB();
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }
    }
}
