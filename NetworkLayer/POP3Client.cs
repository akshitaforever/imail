﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.ComponentModel;
using IMAIL.Models;
namespace NetworkLayer
{
    public class POP3Client
    {
        public enum LoginType { PLAIN, MD5 }
        private Boolean abort = false;
        
        StreamReader rdr;
        Stream inp;
        TcpClient sck = null;
        SslStream sslStream = null;
        Dictionary<int, int> muid = new Dictionary<int, int>();
        Dictionary<int, List<EmailAttachment>> attch = new Dictionary<int, List<EmailAttachment>>();
        private string _username;
        private string _password;
        private string _tag="xm00";
        private int _tag_no=1;
        private MailShared.MailServerHostEntry _hostinfo;
        private Boolean _SSL;
        private Boolean _login;
        private Boolean _connected;
        private LoginType _login_type;
        private const string USER_COMMAND = "USER";
        private const string RETR_COMMAND = "RETR";
        private const string LIST_COMMAND = "LIST";
        private const string PASS_COMMAND = "PASS";
        private const string QUIT_COMMAND = "QUIT";
        private const string STAT_COMMAND = "STAT";
        private const string EOL = "\r\n";
      
        public string UserName
        {
            get { return this._username; }
            set { this._username = value; }
        }
        public string Password
        {
            set { this._password = value; }
        }
        public MailShared.MailServerHostEntry Host
        {
            get { return this._hostinfo; }
            set { this._hostinfo = value; }
        }
        public Boolean UseSSL
        {
            get { return this._SSL; }
            set { this._SSL = value; }
        }
        public LoginType LoginMethod
        {
            get { return this._login_type; }
            set { this._login_type = value; }
        }
        public string Tag
        {
            get { return _tag + _tag_no++.ToString(); }
        }
        public Boolean IsLogin
        {
            get { return this._login; }
            set { this._login = value; }
        }
        public Boolean IsConnected
        {
            get { return this._connected; }
            set { this._connected = value; }
        }
        public Boolean Logout()
        {
            Boolean flag = false;
            byte[] data = Encoding.ASCII.GetBytes(QUIT_COMMAND + EOL);
            inp.Write(data, 0, data.Length);
            string str = rdr.ReadLine();
            if (str.StartsWith("+OK"))
            {
                flag = true;
                _login = false;
            }
            return flag;
        }

        public void Dispose()
        {
            if (IsLogin == true)
            {
                Logout();
            }
            rdr.Close();
            inp.Close();
            sck.Close();
        }
        public void AbortDownload()
        {
            this.abort = true;
        }
        public Boolean Login(string username, string password)
        {
            this.UserName = username;
            this.Password = password;
            Boolean flag = false;

        // 

            sck = new TcpClient(_hostinfo.HostName, _hostinfo.Port);

            inp = sck.GetStream();


            rdr = new StreamReader(inp, Encoding.ASCII);

            string str = rdr.ReadLine();
            byte[] data;
            if (str.StartsWith("+OK"))
            {
                flag = true;
                _connected = true;
                string l_tag = this.Tag;
                string _command = "";
                switch (_login_type)
                {
                    case LoginType.PLAIN: _command =  USER_COMMAND + " " + _username + EOL;
                        break;
                    case LoginType.MD5:// _command = "AUTHENTICATE CRAM-MD5"+EOL;
                        data = Encoding.ASCII.GetBytes(_command);
                        inp.Write(data, 0, data.Length);
                        string _res = rdr.ReadLine();
                        _command = HmacMd5(username, password, _res);
                        break;
                }
                data = Encoding.ASCII.GetBytes(_command);
                inp.Write(data, 0, data.Length);
                str = rdr.ReadLine();
                if(str.StartsWith("+OK"))
                {
                    _command = PASS_COMMAND + " " + _password + EOL;
                    data = Encoding.ASCII.GetBytes(_command);
                inp.Write(data, 0, data.Length);
                str = rdr.ReadLine();
                    if(str.StartsWith("+OK"))
                    {
                       _login=true;
                    }
                    else
                    {
                        _login=false;
                    }
                }
                else
                {
                    _login=false;
                }
              
            }
            return _login;
        }
        public Boolean Login()
        {
            if (_username.Length == 0 || _password.Length == 0)
            {
                throw new NullReferenceException("User Credentials Invalid or Missing");
            }
            else
            {
                return Login(_username, _password);
            }
        }

        private long GetMessageLength(string UID)
        {
            string _command = LIST_COMMAND + " " + UID + EOL;
           byte[] data = Encoding.ASCII.GetBytes(_command);
            inp.Write(data, 0, data.Length);
            string str = rdr.ReadLine();
            long length=0L;
            if (str.StartsWith("+OK"))
            {
                string[] _sp = str.Split(' ');
                length = long.Parse(_sp[2]);
            }
            else
            {
                //check if connection is broken here
            }

            return length;
        }

        public Boolean CheckConnection()
        {
            Boolean flag = true;

            return flag;
        }
        public EmailMessage GetEmail( string UID)
        {
            abort = false;
            string s_tag = this.Tag;

            EmailMessage e = null;
            string atch_str = "";
            long length = 0;

            length = GetMessageLength(UID);
            byte[] data = Encoding.ASCII.GetBytes(RETR_COMMAND + " " + UID + EOL);
            inp.Write(data, 0, data.Length);
            string str = rdr.ReadLine();
         
            long bytesread = 0L;
            int curbytelength = 0;
            if (str.StartsWith("-ERR"))
            {
                //no such message
            }
          
            else
            {

              
                //pg.Show();
                //pg.setlength(length);
                str = rdr.ReadLine();
                while (abort == false)
                {

                    //   MessageBox.Show(str);
                   
                    
                     curbytelength=Encoding.ASCII.GetByteCount(str);
                    bytesread += curbytelength;
                      
                         //   pg.setprogressbarvalue(curbytelength,length.ToString());
                        
                     
                        atch_str += str + EOL;
                    
                        if (bytesread == length || str.Trim().StartsWith("."))
                        {
                            break;
                        }
                        str = rdr.ReadLine();
                   
                }

            }
          //  pg.Close();

            e = new EmailMessage(atch_str);
            return e;
        }
        public void GetEmailAsync(object sender,DoWorkEventArgs ex)
        {
           
            BackgroundWorker bg = sender as BackgroundWorker;
            string UID = ex.Argument.ToString();
            abort = false;
            string s_tag = this.Tag;

            EmailMessage e = null;
            string atch_str = "";
            long length = 0;

            length = GetMessageLength(UID);
            byte[] data = Encoding.ASCII.GetBytes(RETR_COMMAND + " " + UID + EOL);
            inp.Write(data, 0, data.Length);
            string str = rdr.ReadLine();
         
            long bytesread = 0L;
            int curbytelength = 0;
            if (str.StartsWith("-ERR"))
            {
                //no such message
            }

            else
            {


              
                str = rdr.ReadLine();
                while (abort == false && bg.CancellationPending==false)
                {

                    //   MessageBox.Show(str);


                    curbytelength = Encoding.ASCII.GetByteCount(str);
                    bytesread += curbytelength;

               //     pg.setprogressbarvalue(curbytelength, length.ToString());
                    int i=Convert.ToInt32((bytesread*100)/length);
                    if(i<=100)
                    {
                        bg.ReportProgress(i);
                    }

                    atch_str += str + EOL;

                    if (bytesread == length || str.Trim().StartsWith("."))
                    {
                        break;
                    }
                    str = rdr.ReadLine();

                }

            }
           
            
            e = new EmailMessage(atch_str);
            ex.Result = e;
        }


        private void cleardata(string _local_tag)
        {
            string str = rdr.ReadLine();
            if (!str.StartsWith(_local_tag + " OK"))
            {
                str = rdr.ReadLine();
            }
        }
     
        public static string HmacMd5(string username, string password, string serverResponse)
        {
            //Convert the password into usable bytes
            byte[] passwordBytes = System.Text.ASCIIEncoding.Default.GetBytes(password);

            //Setup the Keyed vectors by XORing the password with the given vectors
            byte[] ipad = new byte[64];
            byte[] opad = new byte[64];
            for (int i = 0; i < 64; i++)
            {
                if (i < passwordBytes.Length)
                {
                    ipad[i] = (byte)(0x36 ^ passwordBytes[i]);
                    opad[i] = (byte)(0x5c ^ passwordBytes[i]);
                }
                else
                {
                    ipad[i] = 0x36 ^ 0x00;
                    opad[i] = 0x5C ^ 0x00;
                }
            }

            //Get rid of the "+ " at the beginning and then convert it into bytes
            string serverResponseT = serverResponse.TrimStart(new char[] { '+', ' ' });
            byte[] serverResponseBytes = System.Convert.FromBase64String(serverResponseT);

            //Setup the MD5 hash for the first round (ipad XOR Password + serverResponseT)
            MD5 md5 = new MD5CryptoServiceProvider();
            MemoryStream ms = new MemoryStream(1024);
            ms.Write(ipad, 0, ipad.Length);
            ms.Write(serverResponseBytes, 0, serverResponseBytes.Length);
            ms.Seek(0, SeekOrigin.Begin);
            byte[] resultIpad = md5.ComputeHash(ms);

            //Setup the MD5 hash for the second round (opad XOR password + first round response)
            ms = new MemoryStream(1024);
            ms.Write(opad, 0, opad.Length);
            ms.Write(resultIpad, 0, resultIpad.Length);
            ms.Seek(0, SeekOrigin.Begin);
            byte[] resultOpad = md5.ComputeHash(ms);

            //Append the username and the result and then convert it to base64
            string response = username + " " + ToHexString(resultOpad);
            response = Convert.ToBase64String(System.Text.ASCIIEncoding.Default.GetBytes(response));

            return response;
        }
        private static string ToHexString(byte[] bytes)
        {
            char[] hexDigits = {
                '0', '1', '2', '3', '4', '5', '6', '7',
                '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'
            };

            char[] chars = new char[bytes.Length * 2];

            for (int i = 0; i < bytes.Length; i++)
            {
                int b = bytes[i];
                chars[i * 2] = hexDigits[b >> 4];
                chars[i * 2 + 1] = hexDigits[b & 0xF];
            }
            return new string(chars);
        }
      
      
       
        public POP3Client(MailShared.MailServerHostEntry hst)
        {
            if (hst.HostName == string.Empty || hst.Port == 0)
            {
                throw new Exception("Host Entry Insufficient For Connection");
            }
            else
            {
                this.Host = hst;
            }
        }
        public POP3Client(string hostname, int port, string username, string password, Boolean useSSL)
        {

            MailShared.MailServerHostEntry hst = new MailShared.MailServerHostEntry(hostname, port);
            this.Host = hst;
            this.Password = password;
            this.UserName = username;
            this.UseSSL = useSSL;

        }
        public Dictionary<long, long> GetList(long _last_fetch=0L)
        {
            Dictionary<long, long> dict = new Dictionary<long, long>();
            long msgcount = 0l;
            string _command = LIST_COMMAND +  EOL;
            byte[] data = Encoding.ASCII.GetBytes(_command);
            inp.Write(data, 0, data.Length);
            string str = rdr.ReadLine();
            long msgno = 0l ;
            long length = 0l;
            if (str.StartsWith("+OK"))
            {
                string[] _sp = str.Split(' ');
                msgcount = long.Parse(_sp[1]);
                if (msgcount > 0)
                {
                   
                    while (!str.Trim().StartsWith("."))
                    {
                        if (!str.Trim().StartsWith("."))
                        {
                            _sp = str.Split(' ');
                            if (_sp.Length == 2)
                            {
                                msgno = long.Parse(_sp[0]);
                                length = long.Parse(_sp[1]);
                                long msg = long.Parse(_sp[0]);
                                if (_last_fetch > 0L)
                                {
                                    if (msg > _last_fetch)
                                    {
                                        dict.Add(msgno, length);
                                    }
                                }
                                else
                                {
                                    dict.Add(msgno, length);
                                }
                            }
                            str = rdr.ReadLine();
                        }
                    }
                }
            }
            else
            {
                
                //check if connection is broken here
            }

            return dict;
        }
        public Dictionary<long,long> GetStat()
        {
            Dictionary<long, long> dict = new Dictionary<long, long>();
            string _command = STAT_COMMAND + EOL;
            byte[] data = Encoding.ASCII.GetBytes(_command);
            inp.Write(data, 0, data.Length);
            string str = rdr.ReadLine();
            long msgcount = 0l;
            long msglength = 0l;
            if (str.StartsWith("+OK"))
            {
                string[] _sp = str.Split(' ');
                msgcount = long.Parse(_sp[1]);
                msglength = long.Parse(_sp[2]);
                dict.Add(msgcount, msglength);
              
            }
            else
            {

                //check if connection is broken here
            }

            return dict;
        }
    }
}
