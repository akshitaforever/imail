﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetworkLayer
{
    public static class StringUtils
    {
        public static string GetAbb(long length)
        {

            long l = length;
            int key = 0;
            while (l > 1024)
            {
                l = l / 1024;
                key++;
            }

            return l.ToString() + " " + GetValueByKey(key);
        }
        public static string GetValueByKey(int key)
        {
            string ret = "";
            switch (key)
            {
                case 0: ret = "Bytes";
                    break;
                case 1: ret = "KB";
                    break;
                case 2: ret = "MB";
                    break;
                case 3: ret = "GB";
                    break;
                case 4: ret = "TB";
                    break;
            }
            return ret;
        }
    }
   
}
