﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using System.Net.Security;
using System.ComponentModel;
using IMAIL.Models;

namespace NetworkLayer
{

    public class IMAPClient
    {
        public enum LoginType { PLAIN, MD5 }
        private Boolean abort = false;
        StreamReader rdr;
        private int _last_fetch = 1;
        private Boolean _download_completed = true;
        Stream inp;
        public static MailAccount MailConfig { get; set; }
        TcpClient sck = null;
        SslStream sslStream = null;

        private Boolean _last_action_cancelled = false;
        string curFolder = "";
        Dictionary<int, int> muid = new Dictionary<int, int>();
        Dictionary<int, List<EmailAttachment>> attch = new Dictionary<int, List<EmailAttachment>>();
        private string _username;
        private string _password;
        delegate Boolean MethodDelegate(string username, string password);
        private MailShared.MailServerHostEntry _hostinfo;
        private Boolean _SSL;
        private Boolean _login;
        private Boolean _connected;
        private LoginType _login_type;
        private const string FETCH_COMMAND = "FETCH";
        private const string UID_FETCH_COMMAND = "UID FETCH";
        private const string _tag = "xm00";
        private int _tag_no = 1;
        private const string SELECT_COMMAND = "SELECT";
        private const string LIST_COMMAND = "LIST";
        private const string EOL = "\r\n";
        private const string LOGIN_COMMAND = "LOGIN";
        private const string UID_STORE_COMMAND = "UID STORE";
        private const string UID_DELETE_COMMAND = "UID EXPUNGE";
        private const string UID_DELETE_AND_DESELECT_COMMAND = "UID CLOSE";
        private const string LOGOUT_COMMAND = "LOGOUT";
        private const string UID_SEARCH_COMMAND = "UID SEARCH";
        private const string MD5_LOGIN = "AUTHENTICATE CRAM-MD5";
        private const string FLAG_SEEN = "SEEN";
        private const string FLAG_UNSEEN = "UNSEEN";
        private const string FLAG_NEW = "NEW";
        private const string FLAG_RECENT = "RECENT";
        private const string FLAG_DELETED = "DELETED";
        public enum MessageFlag { SEEN, UNSEEN, NEW, RECENT, DELETED }
        private EmailAttachment email_at = null;
        private int _last_progress = 0;
        private string currentTag = "";
        public string UserName
        {
            get { return this._username; }
            set { this._username = value; }
        }
        public string Password
        {
            set { this._password = value; }
        }
        public MailShared.MailServerHostEntry Host
        {
            get { return this._hostinfo; }
            set { this._hostinfo = value; }
        }
        public Boolean UseSSL
        {
            get { return this._SSL; }
            set { this._SSL = value; }
        }
        public LoginType LoginMethod
        {
            get { return this._login_type; }
            set { this._login_type = value; }
        }
        public string Tag
        {
            get
            {
                currentTag = _tag + (_tag_no - 1).ToString();
                return _tag + _tag_no++.ToString();
            }
        }
        public Boolean IsLogin
        {
            get { return this._login; }
            set { this._login = value; }
        }
        public Boolean IsConnected
        {
            get { return this._connected; }
            set { this._connected = value; }
        }
        public Boolean Logout()
        {
            Boolean flag = false;
            byte[] data = Encoding.ASCII.GetBytes(this.Tag + " " + LOGOUT_COMMAND + EOL);
            if (MailConfig.SSL)
            {
                sslStream.Write(data, 0, data.Length);
            }
            else
            {
                inp.Write(data, 0, data.Length);
            }

            string str = ReadStreamText();
            if (str.StartsWith("* BYE"))
            {
                flag = true;
                _login = false;
            }
            return flag;
        }

        public void Dispose()
        {
            if (IsLogin == true)
            {
                Logout();
            }
            rdr.Close();
            inp.Close();
            sck.Close();
        }
        public void AbortDownload()
        {
            this.abort = true;
        }
        private string ReadStreamText(string terminatestring = "")
        {


            //byte[] buffer = new byte[2048];
            StringBuilder messageData = new StringBuilder();

            //while (!rdr.EndOfStream)
            //{
            if (rdr == null)
            {
                if (!MailConfig.SSL)
                {
                    rdr = new StreamReader(inp, Encoding.ASCII);
                }
                else
                {
                    rdr = new StreamReader(sslStream, Encoding.ASCII);
                }
            }
            //bytes = rdr.ReadLine()
            if (terminatestring.Length == 0)
                messageData.Append(rdr.ReadLine());
            else
            {
                string temp = rdr.ReadLine();
                if (temp.StartsWith(currentTag + " BAD"))
                {
                    return "BAD";
                }
                else
                {
                    messageData.Append(temp);
                    while (!temp.EndsWith(terminatestring))
                    {
                        temp = rdr.ReadLine();
                        messageData.Append(temp);
                    }
                }

            }

            //  }
            return messageData.ToString();

        }
        public Boolean Login(string username, string password, bool ssl = true)
        {
            this.UserName = username;
            this.Password = password;
            Boolean flag = false;

        //    MAILDOWN.frmprogress pg = new MAILDOWN.frmprogress();

            sck = new TcpClient(_hostinfo.HostName, _hostinfo.Port);
            if (!MailConfig.SSL)
            {
                inp = sck.GetStream();
            }
            else
            {
                sslStream = new SslStream(sck.GetStream(), true);
                sslStream.AuthenticateAsClient(MailConfig.Server.HostName);
            }

            //rdr = new StreamReader(sslStream, Encoding.ASCII);
            //byte[] buffer = new byte[2048];
            //StringBuilder messageData = new StringBuilder();
            //int bytes = -1;
            //while (!rdr.EndOfStream)
            //{
            //    //bytes = rdr.ReadLine()
            //    messageData.Append(rdr.ReadLine());

            //}

            //do
            //{

            //    bytes = sslStream.Read(buffer, 0, buffer.Length);

            //    Decoder decoder = Encoding.UTF8.GetDecoder();
            //    char[] chars = new char[decoder.GetCharCount(buffer, 0, bytes)];
            //    decoder.GetChars(buffer, 0, bytes, chars, 0);
            //    messageData.Append(chars);

            //    if (messageData.ToString().IndexOf("<EOF>") != -1)
            //    {
            //        break;
            //    }
            //} while (bytes != 0);
            string str = ReadStreamText();
            byte[] data;
            if (str.StartsWith("* OK"))
            {
                flag = true;
                _connected = true;
                string l_tag = this.Tag;
                string _command = "";
                switch (_login_type)
                {
                    case LoginType.PLAIN: _command = l_tag + " " + LOGIN_COMMAND + " " + _username + " " + _password + EOL;
                        break;
                    case LoginType.MD5:// _command = "AUTHENTICATE CRAM-MD5"+EOL;
                        data = Encoding.ASCII.GetBytes(_command);
                        if (MailConfig.SSL)
                        {
                            sslStream.Write(data, 0, data.Length);
                        }
                        else
                        {
                            inp.Write(data, 0, data.Length);
                        }

                        string _res = ReadStreamText();
                        _command = HmacMd5(username, password, _res);
                        break;
                }
                data = Encoding.ASCII.GetBytes(_command);
                sslStream.Write(data, 0, data.Length);
                str = ReadStreamText("(Success)");
                if (!str.StartsWith(l_tag + " NO"))
                {
                    _login = true;
                }
                //while (!str.StartsWith(l_tag))
                //{
                //    str = ReadStreamText();
                //    if (str.StartsWith(l_tag + " OK"))
                //    {
                //        _login = true;
                //    }
                //}
            }
            return _login;
        }

        public Boolean Login()
        {
            if (_username.Length == 0 || _password.Length == 0)
            {
                throw new NullReferenceException("User Credentials Invalid or Missing");
            }
            else
            {
                return Login(_username, _password);
            }
        }
        public List<MailHeader> GetAllMailHeaders(List<string> lst)
        {
            List<MailHeader> ls = new List<MailHeader>();
            foreach (string s in lst)
            {
                MailHeader h = GetMessageHeader(s);
                ls.Add(h);
            }
            return ls;
        }
        public SortedList<int, MIMEHeader> GetMailMIMEHeaders(string UID)
        {
            SortedList<int, MIMEHeader> lst = new SortedList<int, MIMEHeader>();
            int l = 1;
            MIMEHeader mh = GetMIMEMessageHeader(UID, l);
            if (mh.Content_Disposition.Equals("ATTACHMENT"))
            {
                lst.Add(l, mh);
            }
            while (mh != null)
            {
                ++l;
                mh = GetMIMEMessageHeader(UID, l);
                if (mh != null)
                {
                    if (mh.Content_Disposition.Equals("ATTACHMENT"))
                    {
                        lst.Add(l, mh);
                    }
                }
            }
            return lst;
        }
        public MIMEHeader GetMIMEMessageHeader(string UID, int _last_fetch)
        {
            MIMEHeader e = null;
            string s_tag = this.Tag;
            ++_last_fetch;
            byte[] data = Encoding.ASCII.GetBytes(s_tag + " " + UID_FETCH_COMMAND + " " + UID + " BODY[" + _last_fetch.ToString() + ".MIME]" + EOL);
            if (MailConfig.SSL)
            {
                sslStream.Write(data, 0, data.Length);
            }
            else
            {
                inp.Write(data, 0, data.Length);
            }

            string str = ReadStreamText();
            string header_string = "";
            long length = 0l;
            if (str.StartsWith(s_tag + " OK"))
            {
                //no message exists
            }
            else if (str.StartsWith(s_tag + " BAD"))
            {
                //no message exists
            }
            else
            {
                string[] _sp = str.Split('{');
                if (_sp.Length > 1)
                {
                    string len = _sp[_sp.Length - 1];
                    len = len.Replace("{", "");
                    len = len.Replace("}", "");
                    length = long.Parse(len);
                    while (true)
                    {

                        //   MessageBox.Show(str);
                        if (!str.StartsWith(s_tag + " OK"))
                        {
                            str = ReadStreamText();
                            if (!str.StartsWith(s_tag + " OK"))
                            {

                                if (str.Trim().Length > 1)
                                {
                                    header_string += Environment.NewLine + str;
                                }
                            }

                        }
                        else
                        {
                            break;
                        }
                    }
                    if (header_string.Trim().Length > 0)
                    {
                        e = new MIMEHeader(header_string, length);
                    }

                }
            }
            return e;
        }
        public MailHeader GetMessageHeader(string UID)
        {
            MailHeader e = null;
            string s_tag = this.Tag;
            byte[] data = Encoding.ASCII.GetBytes(s_tag + " " + UID_FETCH_COMMAND + " " + UID + " BODY[HEADER]" + EOL);
            if (MailConfig.SSL)
            {
                sslStream.Write(data, 0, data.Length);
            }
            else
            {
                inp.Write(data, 0, data.Length);
            }
            string str = ReadStreamText();
            string header_string = "";
            if (str.StartsWith(s_tag + " OK"))
            {
                //no message exists
            }
            else
            {
                while (true)
                {

                    //   MessageBox.Show(str);
                    if (!str.StartsWith(s_tag + " OK"))
                    {
                        str = ReadStreamText();
                        if (!str.StartsWith(s_tag + " OK"))
                        {

                            if (str.Trim().Length > 1)
                            {
                                header_string += Environment.NewLine + str;
                            }
                        }

                    }
                    else
                    {
                        break;
                    }
                }
                e = new MailHeader(header_string);

            }
            return e;
        }
        public List<EmailAttachment> GetMessageAttachments(string UID)
        {
            List<EmailAttachment> attch_list = new List<EmailAttachment>();

            int _lst = 1;
            EmailAttachment e = GetNextMessageAttachement(UID, _lst);
            while (e != null)
            {
                ++_lst;
                attch_list.Add(e);
                e = GetNextMessageAttachement(UID, _lst);
            }
            return attch_list;
        }
        /// <summary>
        /// Fetches email Attachments Asynchronously when called with BackgroundWorker
        /// </summary>
        /// <param name="sender">BackgroundWorker Object</param>
        /// <param name="de"></param>
        public void GetMessageAttachmentsAync(object sender, DoWorkEventArgs de)
        {

            _last_progress = 0;
            _last_action_cancelled = false;
            _last_fetch = 0;
            List<EmailAttachment> attch_list = new List<EmailAttachment>();
            string UID = de.Argument.ToString();

            SortedList<int, MIMEHeader> _mime = GetMailMIMEHeaders(UID);
            long total_bytes = 0L;
            IEnumerator<KeyValuePair<int, MIMEHeader>> iet = _mime.GetEnumerator();
            int _max_id = 0;

            while (iet.MoveNext())
            {
                total_bytes += iet.Current.Value.Size;
                if (_max_id < iet.Current.Key)
                {
                    _max_id = iet.Current.Key;
                }
            }
            _max_id += 1;
            iet.Dispose();
            if (_mime.Count > 0)
            {

                BackgroundWorker bg_sender = sender as BackgroundWorker;
                BackgroundWorker bge = new BackgroundWorker();
                bge.WorkerReportsProgress = true;
                bge.WorkerSupportsCancellation = true;
                bge.DoWork += new DoWorkEventHandler(GetNextMessageAttachementAsync);
                bge.ProgressChanged += new ProgressChangedEventHandler(AttchmentDownloading);
                bge.RunWorkerCompleted += new RunWorkerCompletedEventHandler(AttchmentDownloadCompleted);
                List<string> _arg = new List<string>();

                MailShared.MessageWrapper mw = null;

                //



                while (_last_fetch < _max_id)
                {



                    _arg = new List<string>();
                    _arg.Add(UID);
                    _arg.Add(_mime.Keys[_last_fetch].ToString());
                    mw = new MailShared.MessageWrapper(_arg);
                    _download_completed = false;
                    bge.RunWorkerAsync(mw);
                    while (bge.IsBusy)
                    {
                        System.Threading.Thread.Sleep(1000);


                    }
                    if (_last_action_cancelled == false)
                    {
                        if (email_at != null)
                        {
                            attch_list.Add(email_at);
                            ++_last_fetch;
                            int k = Convert.ToInt32((_mime[_mime.Keys[_last_fetch]].Size * 100) / total_bytes);
                            if (k <= 100)
                            {
                                bge.ReportProgress(k);
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                    else
                    {
                        if (bg_sender.CancellationPending == false)
                        {
                            continue;
                        }

                    }



                }


                //




                de.Result = attch_list;
            }
        }
        private void AttchmentDownloadCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled == false)
            {
                if (e.Result != null)
                {
                    email_at = e.Result as EmailAttachment;
                    _last_action_cancelled = false;
                    _download_completed = true;
                }
                else
                {
                    _last_action_cancelled = true;
                    email_at = null;
                    _download_completed = true;
                }
            }
            else
            {
                _last_action_cancelled = true;
            }
        }
        private void AttchmentDownloading(object sender, ProgressChangedEventArgs e)
        {
            _last_progress = e.ProgressPercentage;
        }
        public Boolean SetFlag(string UID, MessageFlag _flag)
        {
            string s_tag = this.Tag;
            string _command = s_tag + " " + UID_STORE_COMMAND + " " + UID + " flags " + GetFlag(_flag) + EOL;
            byte[] data = Encoding.ASCII.GetBytes(_command);
            if (MailConfig.SSL)
            {
                sslStream.Write(data, 0, data.Length);
            }
            else
            {
                inp.Write(data, 0, data.Length);
            }

            string str = ReadStreamText();
            Boolean flag = false;
            if (str.StartsWith(s_tag + " OK"))
            {
                flag = false;
            }
            else
            {
                if (str.Contains("FETCH (FLAGS (" + GetFlag(_flag) + ") UID " + UID))
                {
                    flag = true;
                    str = ReadStreamText();
                }
            }
            return flag;
        }
        private string GetFlag(MessageFlag fl)
        {
            string ret = "";
            switch (fl)
            {
                case MessageFlag.DELETED: ret = FLAG_DELETED;
                    break;
                case MessageFlag.NEW: ret = FLAG_NEW;
                    break;
                case MessageFlag.RECENT: ret = FLAG_RECENT;
                    break;
                case MessageFlag.SEEN: ret = FLAG_SEEN;
                    break;
                case MessageFlag.UNSEEN: ret = FLAG_UNSEEN;
                    break;
            }
            return ret;
        }
        private EmailAttachment GetNextMessageAttachement(string UID, int _last_fetch, MIMEHeader cd = null)
        {
            string s_tag = this.Tag;
            ++_last_fetch;
            EmailAttachment e = null;
            string atch_str = "";
            long length = 0L;
            if (cd == null)
            {
                cd = GetAttachmentContentType(UID, _last_fetch);
            }
            if (cd != null)
            {
                if (cd.Content_Disposition.Equals("ATTACHMENT"))
                {

                    byte[] data = Encoding.ASCII.GetBytes(s_tag + " " + UID_FETCH_COMMAND + " " + UID + " BODY[" + _last_fetch.ToString() + "]" + EOL);
                    if (MailConfig.SSL)
                    {
                        sslStream.Write(data, 0, data.Length);
                    }
                    else
                    {
                        inp.Write(data, 0, data.Length);
                    }

                    string str = ReadStreamText();

                    if (str.Contains(" FETCH (UID " + UID + " BODY[" + _last_fetch.ToString() + "] NIL)"))
                    {

                    }
                    else if (str.StartsWith(s_tag + " BAD"))
                    {

                    }
                    else if (str.StartsWith("* OK Reset Timeout"))
                    {
                        if (Login())
                        {
                            SelectInbox();

                        }
                    }
                    else
                    {


                        str = ReadStreamText();
                        while (true)
                        {

                            //   MessageBox.Show(str);
                            if (!str.StartsWith(s_tag + " OK"))
                            {
                                atch_str += str;
                                atch_str = atch_str.Replace("(", "");
                                atch_str = atch_str.Replace(")", "");
                                str = ReadStreamText();
                            }
                            else
                            {
                                break;
                            }
                        }
                        e = new EmailAttachment(cd, atch_str);
                    }
                }
            }
            return e;
        }
        private void GetNextMessageAttachementAsync(object sender, DoWorkEventArgs de)
        {
            BackgroundWorker bg = sender as BackgroundWorker;
            MailShared.MessageWrapper ms = de.Argument as MailShared.MessageWrapper;
            string UID = ms.Arguments[0];
            int __fetch = Int32.Parse(ms.Arguments[1]);
            string s_tag = this.Tag;
            ++__fetch;
            EmailAttachment e = null;
            string atch_str = "";
            long length = 0L;
            long curbytes = 0L;
            MIMEHeader cd = GetAttachmentContentType(UID, __fetch);

            if (cd != null)
            {
                if (cd.Content_Disposition.Equals("ATTACHMENT"))
                {
                    length = cd.Size;
                    byte[] data = Encoding.ASCII.GetBytes(s_tag + " " + UID_FETCH_COMMAND + " " + UID + " BODY[" + __fetch.ToString() + "]" + EOL);
                    if (MailConfig.SSL)
                    {
                        sslStream.Write(data, 0, data.Length);
                    }
                    else
                    {
                        inp.Write(data, 0, data.Length);
                    }

                    string str = ReadStreamText();

                    if (str.Contains(" FETCH (UID " + UID + " BODY[" + __fetch.ToString() + "] NIL)"))
                    {

                    }
                    else if (str.StartsWith(s_tag + " BAD"))
                    {

                    }
                    else if (str.StartsWith("* OK Reset Timeout"))
                    {
                        if (Login())
                        {
                            SelectInbox();
                            _last_action_cancelled = true;
                        }
                    }
                    else
                    {


                        str = ReadStreamText();
                        while (true)
                        {

                            //   MessageBox.Show(str);
                            if (!str.StartsWith(s_tag + " OK"))
                            {
                                curbytes += Encoding.ASCII.GetByteCount(str);
                                if (length > 0)
                                {
                                    int k = Convert.ToInt32((curbytes * 100) / length);
                                    if (k <= 100)
                                    {
                                        bg.ReportProgress(k);
                                    }
                                }
                                atch_str += str;
                                atch_str = atch_str.Replace("(", "");
                                atch_str = atch_str.Replace(")", "");
                                str = ReadStreamText();
                            }
                            else
                            {
                                break;
                            }
                        }
                        e = new EmailAttachment(cd, atch_str);
                    }
                }
            }
            de.Result = e;

        }
        public EmailMessage GetEmail(string UID)
        {
            EmailMessage e = null;
            if (curFolder.Length > 0)
            {
                abort = false;
                string s_tag = this.Tag;


                string atch_str = "";
                long length = 0;


                byte[] data = Encoding.ASCII.GetBytes(s_tag + " " + UID_FETCH_COMMAND + " " + UID + " BODY[TEXT]" + EOL);
                if (MailConfig.SSL)
                {
                    sslStream.Write(data, 0, data.Length);
                }
                else
                {
                    inp.Write(data, 0, data.Length);
                }

                string str = ReadStreamText();
               // MAILDOWN.frmprogress pg = new MAILDOWN.frmprogress();

                if (str.StartsWith(s_tag + " OK"))
                {
                    //no such message
                }
                else if (str.StartsWith(s_tag + " BAD"))
                {
                    //error
                }
                else
                {

                    string[] sp = str.Split('{');
                    string len = sp[1];
                    len = len.Replace("}", "");
                    length = long.Parse(len);
                //    pg.Show();
                 //   pg.setlength(length);
                    str = ReadStreamText();
                    while (abort == false)
                    {

                        //   MessageBox.Show(str);
                        if (!str.StartsWith(s_tag + " OK"))
                        {
                            atch_str += str + EOL;
                            atch_str = atch_str.Replace("(", "");
                            atch_str = atch_str.Replace(")", "");
                            str = ReadStreamText();
                            if (!str.StartsWith(s_tag + " OK"))
                            {
                             //   pg.setprogressbarvalue(Encoding.ASCII.GetByteCount(str), length.ToString());
                            }
                        }
                        else
                        {
                            break;
                        }
                    }

                }
            //    pg.Close();

                e = new EmailMessage(atch_str);
            }
            return e;
        }
        public void GetEmailAsync(object sender, DoWorkEventArgs de)
        {
            string str = "";
            try
            {
                BackgroundWorker bg = sender as BackgroundWorker;
                string UID = de.Argument.ToString();
                EmailMessage e = null;
                if (curFolder.Length > 0)
                {
                    abort = false;
                    string s_tag = this.Tag;


                    string atch_str = "";
                    long length = 0;


                    byte[] data = Encoding.ASCII.GetBytes(s_tag + " " + UID_FETCH_COMMAND + " " + UID + " BODY[TEXT]" + EOL);
                    if (MailConfig.SSL)
                    {
                        sslStream.Write(data, 0, data.Length);
                    }
                    else
                    {
                        inp.Write(data, 0, data.Length);
                    }

                    str = ReadStreamText();
                    //   MAILDOWN.frmprogress pg = new MAILDOWN.frmprogress();

                    if (str.StartsWith(s_tag + " OK"))
                    {
                        //no such message
                    }
                    if (str.StartsWith("* OK Reset"))
                    {

                        de.Cancel = true;
                        de.Result = "Network Timeout Attempting to Login";
                        if (Login())
                        {
                            SelectInbox();
                        }
                        else
                        {

                            de.Cancel = true;
                        }
                    }
                    else if (str.StartsWith(s_tag + " BAD"))
                    {
                        //error
                    }
                    else
                    {


                        string[] sp = str.Split('{');
                        if (sp.Length > 1)
                        {
                            string len = sp[1];
                            len = len.Replace("}", "");
                            length = long.Parse(len);
                            long bytesread = 0L;
                            int curbytelength = 0;
                            str = ReadStreamText();
                            while (abort == false)
                            {

                                //   MessageBox.Show(str);
                                if (!str.StartsWith(s_tag + " OK"))
                                {
                                    curbytelength = Encoding.ASCII.GetByteCount(str);
                                    bytesread += curbytelength;
                                    atch_str += str + EOL;
                                    atch_str = atch_str.Replace("(", "");
                                    atch_str = atch_str.Replace(")", "");
                                    str = ReadStreamText();
                                    int i = Convert.ToInt32((bytesread * 100) / length);
                                    if (i <= 100)
                                    {
                                        bg.ReportProgress(i);
                                    }
                                }
                                else
                                {
                                    break;
                                }
                            }


                        }
                        //  pg.Close();

                        e = new EmailMessage(atch_str);
                        de.Result = e;
                    }

                }
              //  System.Windows.Forms.MessageBox.Show(str);
            }

            catch (Exception e)
            {
             //   System.Windows.Forms.MessageBox.Show(e.Message + Environment.NewLine + e.StackTrace);
            }
        }

        private void cleardata(string _local_tag)
        {
            string str = ReadStreamText();
            if (!str.StartsWith(_local_tag + " OK"))
            {
                str = ReadStreamText();
            }
        }
        public long GetMessageSizes(string search)
        {
            long total = 0l;
            List<string> lst = Search(search);
            foreach (string s in lst)
            {
                total += GetMessageSize(s);
            }
            return total;
        }
        public long GetMessageSizes(List<string> lst)
        {
            long total = 0l;

            foreach (string s in lst)
            {
                total += GetMessageSize(s);
            }
            return total;
        }
        public long GetMessageSize(string UID)
        {
            long length = 0l;
            string s_tag = this.Tag;
            string _command = s_tag + " " + UID_FETCH_COMMAND + " " + UID + " RFC822.SIZE" + EOL;
            byte[] data = Encoding.ASCII.GetBytes(_command);
            if (MailConfig.SSL)
            {
                sslStream.Write(data, 0, data.Length);
            }
            else
            {
                inp.Write(data, 0, data.Length);
            }

            string str = ReadStreamText();
            Boolean flag = false;

            if (str.StartsWith(s_tag + " OK"))
            {
                //No message found
                flag = false;
            }
            else if (str.StartsWith(s_tag + " BAD"))
            {
                //No message found
                flag = false;
            }
            else
            {
                string[] _sp = str.Split(' ');
                string tmp = _sp[_sp.Length - 1];
                tmp = tmp.Replace(")", "");
                length = long.Parse(tmp);
                while (!str.StartsWith(s_tag + " OK"))
                {
                    str = ReadStreamText();
                }
            }
            return length;
        }
        private MIMEHeader GetAttachmentContentType(string UID, int _last_fetch)
        {

            string s_tag = this.Tag;
            long size = 0L;
            MIMEHeader cd = null;
            byte[] data = Encoding.ASCII.GetBytes(s_tag + " " + UID_FETCH_COMMAND + " " + UID + " BODY[" + _last_fetch.ToString() + ".MIME]" + EOL);
            if (MailConfig.SSL)
            {
                sslStream.Write(data, 0, data.Length);
            }
            else
            {
                inp.Write(data, 0, data.Length);
            }

            string str = ReadStreamText();
            //Add check here for nil
            string _content_type = "";
            if (str.StartsWith(s_tag + " OK"))
            {
                //no such message
            }
            else if (str.Contains(" FETCH (UID " + UID + " BODY[" + _last_fetch.ToString() + "] NIL)"))
            {

            }
            else
            {
                string _sz = "";
                if (str.IndexOf("{") >= 0)
                {
                    _sz = str.Substring(str.IndexOf("{"));
                    int i = _sz.IndexOf('}');
                    _sz = _sz.Substring(0, i);
                    _sz = _sz.Replace("{", "");
                    _sz = _sz.Replace("}", "");
                    if (!long.TryParse(_sz, out size) == true)
                    {
                        size = 0L;
                    }
                }
                str = ReadStreamText();
                while (true)
                {

                    //   MessageBox.Show(str);
                    if (!str.Equals(s_tag + " OK Success"))
                    {



                        //  if (!lst.Contains(str))
                        // {
                        // lst.Add(str);
                        //  }
                        _content_type += Environment.NewLine + str;

                        str = ReadStreamText();


                    }
                    else
                    {
                        break;
                    }

                }
                cd = new MIMEHeader(_content_type, size);
                inp.Flush();
            }
            return cd;

        }
        public static string HmacMd5(string username, string password, string serverResponse)
        {
            //Convert the password into usable bytes
            byte[] passwordBytes = System.Text.ASCIIEncoding.Default.GetBytes(password);

            //Setup the Keyed vectors by XORing the password with the given vectors
            byte[] ipad = new byte[64];
            byte[] opad = new byte[64];
            for (int i = 0; i < 64; i++)
            {
                if (i < passwordBytes.Length)
                {
                    ipad[i] = (byte)(0x36 ^ passwordBytes[i]);
                    opad[i] = (byte)(0x5c ^ passwordBytes[i]);
                }
                else
                {
                    ipad[i] = 0x36 ^ 0x00;
                    opad[i] = 0x5C ^ 0x00;
                }
            }

            //Get rid of the "+ " at the beginning and then convert it into bytes
            string serverResponseT = serverResponse.TrimStart(new char[] { '+', ' ' });
            byte[] serverResponseBytes = System.Convert.FromBase64String(serverResponseT);

            //Setup the MD5 hash for the first round (ipad XOR Password + serverResponseT)
            MD5 md5 = new MD5CryptoServiceProvider();
            MemoryStream ms = new MemoryStream(1024);
            ms.Write(ipad, 0, ipad.Length);
            ms.Write(serverResponseBytes, 0, serverResponseBytes.Length);
            ms.Seek(0, SeekOrigin.Begin);
            byte[] resultIpad = md5.ComputeHash(ms);

            //Setup the MD5 hash for the second round (opad XOR password + first round response)
            ms = new MemoryStream(1024);
            ms.Write(opad, 0, opad.Length);
            ms.Write(resultIpad, 0, resultIpad.Length);
            ms.Seek(0, SeekOrigin.Begin);
            byte[] resultOpad = md5.ComputeHash(ms);

            //Append the username and the result and then convert it to base64
            string response = username + " " + ToHexString(resultOpad);
            response = Convert.ToBase64String(System.Text.ASCIIEncoding.Default.GetBytes(response));

            return response;
        }
        private static string ToHexString(byte[] bytes)
        {
            char[] hexDigits = {
                '0', '1', '2', '3', '4', '5', '6', '7',
                '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'
            };

            char[] chars = new char[bytes.Length * 2];

            for (int i = 0; i < bytes.Length; i++)
            {
                int b = bytes[i];
                chars[i * 2] = hexDigits[b >> 4];
                chars[i * 2 + 1] = hexDigits[b & 0xF];
            }
            return new string(chars);
        }
        public Boolean SelectFolder(string FolderName)
        {

            Boolean flag = true;
            string s_tag = this.Tag;
            byte[] data = Encoding.ASCII.GetBytes(s_tag + " " + SELECT_COMMAND + " " + FolderName + EOL);
            if (MailConfig.SSL)
            {
                sslStream.Write(data, 0, data.Length);
            }
            else
            {
                inp.Write(data, 0, data.Length);
            }

            string str = ReadStreamText("(Success)");
            //  string flags = ReadStreamText();
            //  str = ReadStreamText();
            //    while (!str.StartsWith(s_tag))
            //   {
            //str = ReadStreamText();
            if (!str.StartsWith(s_tag + " NO"))
            {
                flag = true;
                curFolder = FolderName;
            }
            else
            {
                //       Match mc = Regex.Match(str, "([0-9]) EXISTS",RegexOptions.IgnoreCase);
                //        if(mc.Success)
                //        {
                //            string num = mc.Groups[1].Value;
                //        }
                //         mc = Regex.Match(str, "([0-9]) RECENT", RegexOptions.IgnoreCase);
                //        if (mc.Success)
                //        {
                //            string num = mc.Groups[1].Value;
                //        }    
            }
            // }

            //      data = ASCIIEncoding.ASCII.GetBytes("xm003 FETCH 1 BODY[2]\r\n");
            if (MailConfig.SSL)
            {
                sslStream.Flush();
            }
            else
            {
                inp.Flush();
            }

            return flag;
        }
        public List<string> Search(string search)
        {

            List<string> msglist = new List<string>();
            if (curFolder.Length == 0)
            {
                throw new Exception("Select a Folder");
            }
            else
            {
                string s_tag = this.Tag;
                byte[] data = Encoding.ASCII.GetBytes(s_tag + " " + UID_SEARCH_COMMAND + " " + search + EOL);
                if (MailConfig.SSL)
                {
                    sslStream.Write(data, 0, data.Length);
                }
                else
                {
                    inp.Write(data, 0, data.Length);
                }

                string str = ReadStreamText("OK SEARCH completed (Success)");
                if (str.StartsWith(s_tag + " BAD"))
                {
                    //error in command
                }
                else
                {
                    if (str.StartsWith("* FLAGS"))
                    {
                        str = ReadStreamText(); //str.Replace("* SEARCH", "").Trim();
                    }
                    str = str.Replace("* SEARCH", "");
                    str = str.Substring(0, str.IndexOf(s_tag));
                    string[] msg = str.Split(' ');
                    foreach (string s in msg)
                    {
                        if (s.Trim().Length > 0)
                        {
                            msglist.Add(s);
                        }
                    }

                    //if (str != null)
                    //{
                    //    while (!str.StartsWith(s_tag))
                    //    {
                    //        str = ReadStreamText();

                    //    }
                    //}




                    inp.Flush();
                }
            }
            return msglist;
        }

        public Boolean SelectInbox()
        {
            var lst = GetFolders();
            string foldername = "INBOX";
            Boolean flag = true;
            flag = SelectFolder(foldername);
            return flag;
        }
        public List<string> GetFolders()
        {
            Boolean flag = true;
            string s_tag = this.Tag;
            List<string> lst = new List<string>();
            string _com = s_tag + " " + LIST_COMMAND + " " + @"""" + "" + @"""" + " " + @"""" + "*" + @"""" + EOL;
            byte[] data = Encoding.ASCII.GetBytes(_com);
            if (MailConfig.SSL)
            {
                sslStream.Write(data, 0, data.Length);
            }
            else
            {
                inp.Write(data, 0, data.Length);
            }

            string str = ReadStreamText("OK Success");
            str = str.Replace(s_tag + " OK Success", "");
            string[] segments = str.Split(new string[] { "* LIST" }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string item in segments)
            {
                
                string fname = item.Trim().Split(' ')[2];
                fname = fname.Replace(@"""", "");
                lst.Add(fname);
            }
            //      data = ASCIIEncoding.ASCII.GetBytes("xm003 FETCH 1 BODY[2]\r\n");
            if (MailConfig.SSL)
            {
                sslStream.Flush();
            }
            else
            {
                inp.Flush();
            }
            return lst;

        }
        public IMAPClient(MailShared.MailServerHostEntry hst)
        {
            if (hst.HostName == string.Empty || hst.Port < 1 || hst.Port > 65535)
            {
                throw new Exception("Host Entry Insufficient For Connection");
            }
            else
            {
                this.Host = hst;
            }
        }
        public IMAPClient(string hostname, int port, string username, string password, Boolean useSSL)
        {

            MailShared.MailServerHostEntry hst = new MailShared.MailServerHostEntry(hostname, port);
            this.Host = hst;
            this.Password = password;
            this.UserName = username;
            this.UseSSL = useSSL;

        }
    }
}
