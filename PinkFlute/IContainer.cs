﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PinkFlute
{
    public interface IContainer
    {

        ContainerEntry<Type, Type> Register<T, TImpl>();
        ContainerEntry<Type, Type> RegisterWithPredicate<T,TImpl>(Func<TImpl> predicate);
        ContainerEntry<Type, Type> RegisterSingleton<T, TImpl>();
        ContainerEntry<Type, Type> RegisterStatic<T, TImpl>(TImpl @object);

    }
}
