﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PinkFlute
{
    public class ContainerEntry<TSource, TArget>
    {
        private Func<TSource, TArget, Object> _ConstructorAction;
        private Object _stateObject;
        private SortedList<int, Object> _Arguments;
        public SortedList<int, object> GetArguments()
        {
            return _Arguments;
        }
        public ContainerEntry()
        {
            this._Arguments = new SortedList<int, object>();
        }
        public ContainerEntry<TSource, TArget> ConstructUsing(Func<TSource, TArget, Object> instancefunction, Object ExtraObject)
        {
            this._ConstructorAction = instancefunction;
            this._stateObject = ExtraObject;
            return this;
        }
        public ContainerEntry<TSource, TArget> WithParameters(params Object[] args)
        {
            if (_Arguments.Count > 0)
            {
                throw new ArgumentException("With Parameters cannot be called Multiple Times");
            }
            foreach (var item in args)
            {
                _Arguments.Add(_Arguments.Count + 1, item);
            }
            return this;
        }
        public ContainerEntry<TSource, TArget> WithParameters<T>(T obj)
        {
            if (_Arguments.Count > 0)
            {
                throw new ArgumentException("With Parameters cannot be called Multiple Times");
            }

            _Arguments.Add(1, obj);

            return this;
        }
        public ContainerEntry<TSource, TArget> WithParameters<T>(params T[] obj)
        {
            if (_Arguments.Count > 0)
            {
                throw new ArgumentException("With Parameters cannot be called Multiple Times");
            }
            foreach (var item in obj)
            {
                _Arguments.Add(_Arguments.Count + 1, item);
            }
            return this;
        }
        public ContainerEntry<TSource, TArget> WithParameters<T1, T2>(T1 obj, T2 obj2)
        {
            if (_Arguments.Count > 0)
            {
                throw new ArgumentException("With Parameters cannot be called Multiple Times");
            }
            _Arguments.Add(1, obj);
            _Arguments.Add(2, obj2);
            return this;
        }
        public ContainerEntry<TSource, TArget> WithParameters<T1, T2, T3>(T1 obj1, T2 obj2, T3 obj3)
        {
            if (_Arguments.Count > 0)
            {
                throw new ArgumentException("With Parameters cannot be called Multiple Times");
            }
            _Arguments.Add(1, obj1);
            _Arguments.Add(2, obj2);
            _Arguments.Add(2, obj3);
            return this;
        }
    }
}
