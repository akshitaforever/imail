﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PinkFlute
{
    public class TypeMeta<TService, TImpl> where TImpl : class, TService
    {
        private Type TInterface { get; set; }
        private Type TConcrete { get; set; }
        private Func<TImpl> predicate { get; set; }
        private ContainerEntry<TService, TImpl> _entry { get; set; }
        private Object StateObject { get; set; }
        private Object _inst { get; set; }
        public Enums.InstanceMode Mode { get; set; }
        private bool _hascustom;
        public TypeMeta(Type TInterface, Type TConcrete, Enums.InstanceMode Mode, Func<TImpl> predicate = null, Object StateObject = null, object instance = null)
        {
            this.TInterface = TInterface;
            this.TConcrete = TConcrete;
            this.Mode = Mode;
            this.predicate = predicate;
            this.StateObject = StateObject;
            this._entry = new ContainerEntry<TService, TImpl>();
            this._inst = instance;

        }
        public TypeMeta(TImpl obj)
        {
            _inst = obj;
        }
        public ContainerEntry<TService, TImpl> GetContainerEntry()
        {
            return this._entry;
        }
        public bool HasEntry(Type TService)
        {
            return TService == TInterface;
        }
        public Object GetObject()
        {
            switch (Mode)
            {
                case Enums.InstanceMode.Singleton: return GetSingleton();
                    break;
                case Enums.InstanceMode.Predicate: return GetPredicateOutput();
                    break;
                case Enums.InstanceMode.PerInstance: return GetObjectByDefault();
                    break;
                case Enums.InstanceMode.Static: return GetObjectStatic();
                    break;
                default:
                    break;
            }
            return null;

        }
        private TImpl GetObjectWithConstructor()
        {
            if (_entry.GetArguments().Count > 0)
                return Activator.CreateInstance(TConcrete, _entry.GetArguments().ToArray()) as TImpl;
            else
            {
                var k = typeof(TImpl);
                return Activator.CreateInstance(TConcrete) as TImpl;
            }
        }
        private TImpl GetObjectStatic()
        {
            return _inst as TImpl;
        }
        private TImpl GetSingleton()
        {
            return _inst != null ? _inst as TImpl : GetObjectWithConstructor();
        }
        private TImpl GetPredicateOutput()
        {
            if (predicate == null)
                throw new ArgumentNullException("predicate is Undefined");
            return predicate();
        }
        private TImpl GetObjectByDefault()
        {
            return GetObjectWithConstructor();
        }

    }

}
