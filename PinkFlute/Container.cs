﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace PinkFlute
{
    public class Container : IContainer, IDependencyResolver
    {
        List<TypeMeta<Type, Type>> _reg;

        public Container()
        {
            _reg = new List<TypeMeta<Type, Type>>();
        }

        public ContainerEntry<Type, Type> Register<T, TImpl>()
        {
            return AddContainerEntry<T, TImpl>(Enums.InstanceMode.PerInstance);
        }
        public ContainerEntry<Type, Type> Register(Type BaseType, Type InstanceType)
        {
            return AddContainerEntry(BaseType, InstanceType, Enums.InstanceMode.PerInstance);
        }
        public ContainerEntry<Type, Type> RegisterWithPredicate<T, TImpl>(Func<TImpl> predicate)
        {
            return AddContainerEntry<T, TImpl>(Enums.InstanceMode.Predicate, predicate, null);
        }
        public ContainerEntry<Type, Type> RegisterStatic<T, TImpl>(TImpl @object)
        {
            return AddContainerEntry<T, TImpl>(Enums.InstanceMode.Static, @object);
        }
        public ContainerEntry<Type, Type> RegisterSingleton<T, TImpl>()
        {
            return AddContainerEntry<T, TImpl>(Enums.InstanceMode.Singleton);
        }

        private ContainerEntry<Type, Type> AddContainerEntry<T, TImpl>(Enums.InstanceMode mode, params Object[] args)
        {
            return AddContainerEntry(typeof(T), typeof(TImpl), mode, args);
        }
        private ContainerEntry<Type, Type> AddContainerEntry(Type TService, Type TImplementation, PinkFlute.Enums.InstanceMode mode, params Object[] args)
        {
            ContainerEntry<Type, Type> _entry = new ContainerEntry<Type, Type>();
            switch (mode)
            {
                case PinkFlute.Enums.InstanceMode.Singleton:
                    _reg.Add(new TypeMeta<Type, Type>(TService, TImplementation, mode, null, null));
                    break;
                case PinkFlute.Enums.InstanceMode.PerInstance:
                    _reg.Add(new TypeMeta<Type, Type>(TService, TImplementation, mode, null, null));
                    break;
                case PinkFlute.Enums.InstanceMode.Predicate:
                    _reg.Add(new TypeMeta<Type, Type>(TService, TImplementation, mode, args[0] as Func<Type>, args[1]));
                    break;
                case PinkFlute.Enums.InstanceMode.Static:
                    _reg.Add(new TypeMeta<Type, Type>(TService, TImplementation, mode, null, null, args[0]));
                    break;
                default:
                    break;
            }
            return _reg.Single(p => p.HasEntry(TService)).GetContainerEntry();
        }


        public TService GetInstance<TService>(TService obj = null)
            where TService : class
        {

            if (_reg.Any(p => p.HasEntry(typeof(TService))))
            {

                return _reg.Single(p => p.HasEntry(typeof(TService))).GetObject() as TService;

            }
            return null;
        }

        public void RegisterGenericWithBase<TAssembly, TBase, Timpl>(TAssembly targetassembly, TBase basetype, Timpl BindType) where TAssembly : Assembly
        {
            var _classes = targetassembly.ExportedTypes.Where(p => p.BaseType != null && p.BaseType == typeof(TBase));
            if (!typeof(Timpl).IsConstructedGenericType)
            {
                throw new ArgumentException("Bind Type is not Generic");
            }
            foreach (var item in _classes)
            {
                //  this.Register(item.GetType(),
            }

        }

        public object GetService(Type serviceType)
        {
            return GetInstance(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return new List<Object>() { GetInstance(serviceType) };
        }
    }

}
