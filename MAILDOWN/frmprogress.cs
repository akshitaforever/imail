﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using NetworkLayer;
namespace IMAIL
{
    public partial class frmprogress : Form
    {
        public frmprogress()
        {
            InitializeComponent();
        }
        private IMAPClient t;
        private long sz;
        int donebytes = 0;
        long i = 0;
      
        private void frmprogress_Load(object sender, EventArgs e)
        {

        }
        public void SetProgressbar(Boolean flag)
        {
            pg1.Visible = flag;
        }
        private void btn_cancel_Click(object sender, EventArgs e)
        {
            t.AbortDownload();
            this.Close();
        }
        public void setprogressbarvalue(long temp,string status)
        {
            this.i += temp;
            if (this.i > 0)
            {
               
               
                int ttl = Convert.ToInt32(((i+1)*100) / sz );
                if (ttl <= 100)
                {
                    pg1.Value = ttl;
                }

                this.lbl_status.Text =GetAbb(this.i) + " of "+ GetAbb(sz);
                
                this.Refresh();
                this.Visible = true;
            }
        }
        public void setlength(long  length)
        {

            sz = length;
            this.lbl_status.Text = GetAbb(sz);

        }
        public string GetAbb(long length)
        {
          
            long l = length;
            int key = 0;
            while (l > 1024)
            {
                l = l / 1024;
                key++;
            }

            return l.ToString()+" "+GetValueByKey(key);
        }
        public string GetValueByKey(int key)
        {
            string ret = "";
            switch (key)
            {
                case 0: ret = "Bytes";
                    break;
                case 1: ret = "KB";
                    break;
                case 2: ret = "MB";
                    break;
                case 3: ret = "GB";
                    break;
                case 4: ret = "TB";
                    break;
            }
            return ret;
        }
    }
}
