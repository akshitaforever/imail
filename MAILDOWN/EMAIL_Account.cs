﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net.Mail;
using System.IO;
using System.Net;
using IMAIL.Models;
using IMAIL.AccountSetup;
namespace IMAIL
{
    public partial class EMAIL_Account : Form
    {
        public EMAIL_Account()
        {
            InitializeComponent();
        }
        private enum WriteMode { Add, Edit, Delete }
        WriteMode mode;


        Dictionary<string, Boolean> in_use = new Dictionary<string, bool>();
        private const string APP_FILE_NAME = "app.ini";
        private void EMAIL_Account_Load(object sender, EventArgs e)
        {
            load_account_data();

            cmb_type.SelectedIndex = 0;
            cmbview.SelectedIndex = 0;
        }



        private void addlistviewitem(MailAccount m)
        {
            ListViewItem li = new ListViewItem(m.Name);
            li.Name = m.Name;
            li.Text = m.Name;
            li.SubItems.Add(m.Server.HostName);
            li.SubItems.Add(m.Server.Port.ToString());
            li.SubItems.Add(m.Type.ToString());
            li.SubItems.Add(in_use[m.Name] == true ? "Yes" : "No");
            li.SubItems.Add(m.SSL ? "Yes" : "No");
            if (in_use[m.Name] == true)
            {
                li.ForeColor = Color.Black;
            }
            else
            {
                li.ForeColor = Color.Gray;
            }
            li.Group = lv_account.Groups[m.Type.ToString()];
            li.ImageIndex = 0;

            lv_account.Items.Add(li);
        }

        private void load_account_data()
        {
            lv_account.Items.Clear();
            in_use.Clear();
            string _host = "";
            int _port = 143;
            string _name = "";
            string _type = "";
            bool _ssl = false;
            Boolean _inuse = false;
            if (!File.Exists(System.Windows.Forms.Application.StartupPath + "\\" + APP_FILE_NAME))
            {
                MessageBox.Show("Application Configuration File Could Not Be Found");
            }
            else
            {
                StreamReader rdr = new StreamReader(System.Windows.Forms.Application.StartupPath + "\\" + APP_FILE_NAME);
                string str = "";
                Boolean isbegin = false;
                while (!rdr.EndOfStream)
                {
                    str = rdr.ReadLine();
                    if (str.StartsWith("#BEGIN"))
                    {
                        isbegin = true;

                    }
                    else if (str.StartsWith("#END"))
                    {
                        isbegin = false;
                        string a_type = "";

                        MailShared.MailServerHostEntry m = new MailShared.MailServerHostEntry(_host, _port);
                        MailAccount ml = new MailAccount(_name, m, _inuse, _type == "IMAP" ? MailAccountType.IMAP : MailAccountType.POP3, _ssl);
                        in_use.Add(_name, _inuse);
                        if (_type.Equals("IMAP"))
                        {
                            a_type = "IMAP";

                        }
                        else
                        {
                            a_type = "POP3";
                        }
                        addlistviewitem(ml);


                    }
                    else
                    {
                        if (isbegin == true)
                        {

                            if (str.IndexOf(':') >= 0)
                            {
                                string[] _sp = str.Split(':');
                                if (str.StartsWith("ACC_NAME"))
                                {
                                    _name = _sp[1];
                                }
                                if (str.StartsWith("ACC_SERVER"))
                                {
                                    _host = _sp[1];
                                }
                                if (str.StartsWith("ACC_PORT"))
                                {
                                    _port = Int32.Parse(_sp[1].Trim());
                                }
                                if (str.StartsWith("ACC_TYPE"))
                                {
                                    _type = _sp[1];
                                }
                                if (str.StartsWith("IN_USE"))
                                {
                                    _inuse = _sp[1].Equals("1") ? true : false;
                                }
                                if (str.StartsWith("SSL"))
                                {
                                    _ssl = _sp[1].Equals("1") ? true : false;
                                }
                            }
                        }
                    }
                }
                rdr.Close();
            }
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            if (txt_acc_name.Text.Length == 0 || txt_port.Text.Length == 0 || txt_server.Text.Length == 0 || cmb_type.Text.Length == 0)
            {
                MessageBox.Show("Please Fill All Required Fields");
            }
            else
            {
                if (in_use.ContainsKey(txt_acc_name.Text) && mode == WriteMode.Add)
                {
                    MessageBox.Show("Name Already In Use Please enter another Name");
                }
                else
                {
                    if (mode == WriteMode.Add)
                    {
                        StreamWriter wr = new StreamWriter(System.Windows.Forms.Application.StartupPath + "\\" + APP_FILE_NAME, true);
                        AddItem(wr, txt_acc_name.Text, txt_server.Text, txt_port.Text, cmb_type.Text, ch_in_use.Checked, ch_ssl.Checked);
                        wr.Close();
                        InitializeSync();
                        load_account_data();

                    }
                    else
                    {
                        if (lv_account.SelectedItems.Count > 0)
                        {
                            lv_account.SelectedItems[0].SubItems[1].Text = txt_server.Text;
                            lv_account.SelectedItems[0].SubItems[2].Text = txt_port.Text;
                            lv_account.SelectedItems[0].SubItems[3].Text = cmb_type.Text;
                            lv_account.SelectedItems[0].SubItems[4].Text = ch_in_use.Checked == true ? "Yes" : "No";
                            lv_account.SelectedItems[0].SubItems[5].Text = ch_ssl.Checked == true ? "Yes" : "No";
                            edititem(false);
                            setupaccount();
                            load_account_data();
                        }
                    }
                    p_add.Visible = false;
                    this.Width = lv_account.Left + lv_account.Width + 10;
                }
            }
            txt_acc_name.Text = "";
            txt_port.Text = "";
            txt_server.Text = "";
            cmb_type.SelectedIndex = 0;
            ch_ssl.Checked = false;
            ch_in_use.Checked = false;
        }

        private bool setupaccount()
        {
            Models.MailShared.MailServerHostEntry server = new MailShared.MailServerHostEntry(txt_server.Text, Int32.Parse(txt_port.Text));
            MailAccountType type = cmb_type.Text == "IMAP" ? MailAccountType.IMAP : MailAccountType.POP3;
            MailAccount macc = new MailAccount(txt_acc_name.Text, server, ch_in_use.Checked, type, ch_ssl.Checked);
            PasswordDialog pdialog = new PasswordDialog("", txt_acc_name.Text, false);
            if (pdialog.ShowDialog() == DialogResult.OK)
            {
                NetworkCredential nc = new NetworkCredential(pdialog.Username, pdialog.Password);
                AccountSync.InitializeSync(macc, nc);
            }
            return false;
        }

        private void InitializeSync()
        {

        }

        private void AddItem(StreamWriter wr, string _name, string _host, string _port, string _type, Boolean _inuse, bool ssl)
        {

            wr.WriteLine("#BEGIN");
            wr.WriteLine("ACC_NAME:" + _name);
            wr.WriteLine("ACC_SERVER:" + _host);
            wr.WriteLine("ACC_PORT:" + _port);
            wr.WriteLine("ACC_TYPE:" + _type);
            wr.WriteLine("IN_USE:" + (_inuse == true ? "1" : "0"));
            wr.WriteLine("SSL:" + (ssl == true ? "1" : "0"));
            wr.WriteLine("#END");

        }
        private void Appenditem()
        {
            StreamWriter wr = new StreamWriter(System.Windows.Forms.Application.StartupPath + "\\" + APP_FILE_NAME, true);
            AddItem(wr, txt_acc_name.Text, txt_server.Text, txt_port.Text, cmb_type.Text, ch_in_use.Checked, ch_ssl.Checked);
            wr.Close();
        }
        private void edititem(Boolean delete, string item = "")
        {
            if (File.Exists(System.Windows.Forms.Application.StartupPath + "\\" + APP_FILE_NAME))
            {
                File.Delete(System.Windows.Forms.Application.StartupPath + "\\" + APP_FILE_NAME);
            }
            using (StreamWriter wr = new StreamWriter(System.Windows.Forms.Application.StartupPath + "\\" + APP_FILE_NAME, false))
            {
                foreach (ListViewItem li in lv_account.Items)
                {
                    if (delete == true)
                    {
                        if (!li.Name.Equals(item))
                        {
                            AddItem(wr, li.Text, li.SubItems[1].Text, li.SubItems[2].Text, li.SubItems[3].Text, li.SubItems[4].Text.Equals("Yes"), li.SubItems[5].Text.Equals("Yes"));
                        }
                    }
                    else
                    {
                        AddItem(wr, li.Text, li.SubItems[1].Text, li.SubItems[2].Text, li.SubItems[3].Text, li.SubItems[4].Text.Equals("Yes"), li.SubItems[5].Text.Equals("Yes"));
                    }
                }
                wr.Close();
            }
        }
        private void btn_add_Click(object sender, EventArgs e)
        {
            mode = WriteMode.Add;
            enable_edit();
        }

        private void enable_edit()
        {
            this.Width = lv_account.Left + lv_account.Width + 10 + p_add.Width + 10;
            p_add.Visible = true;
        }

        private void btn_edit_Click(object sender, EventArgs e)
        {
            if (lv_account.Items.Count > 0)
            {
                if (lv_account.SelectedItems.Count > 0)
                {
                    ListViewItem li = lv_account.SelectedItems[0];
                    mode = WriteMode.Edit;
                    enable_edit();
                    txt_acc_name.Text = li.Text;
                    txt_server.Text = li.SubItems[1].Text;
                    txt_port.Text = li.SubItems[2].Text;
                    ch_ssl.Checked = li.SubItems[5].Text.Equals("Yes");
                    cmb_type.SelectedIndex = cmb_type.FindString(li.SubItems[3].Text);
                    ch_in_use.Checked = li.SubItems[4].Text.Equals("Yes") ? true : false;

                }

            }
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            if (lv_account.SelectedItems.Count > 0)
            {
                if (MessageBox.Show("Are You Sure You Want To Remove item " + lv_account.SelectedItems[0].Text + " ?", "Confirm", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    edititem(true, lv_account.SelectedItems[0].Text);
                    lv_account.Items.Remove(lv_account.SelectedItems[0]);
                }
            }
        }

        private void btnview_Click(object sender, EventArgs e)
        {

        }

        private void cmbview_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (cmbview.SelectedIndex)
            {
                case 0: lv_account.View = View.LargeIcon;
                    break;
                case 1: lv_account.View = View.List;
                    break;
                case 2: lv_account.View = View.Details;
                    break;
                case 3: lv_account.View = View.Tile;
                    break;
            }
        }

        private void EMAIL_Account_FormClosing(object sender, FormClosingEventArgs e)
        {
            frmmain frm = (frmmain)this.Tag;
            if (frm != null)
            {
                frm.load_account_data();
            }
        }
    }
}
