﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DB;
using IMAIL.Models;
namespace IMAIL.Startup
{
    public class InitializeApplication
    {
        public InitializeApplication()
        {
            PinkFlute.Container container = new PinkFlute.Container();
            container.Register<DB.IContext<MailProfile>, SQLiteDB.SQLiteRepository<MailProfile>>();
            Properties.Settings.Default.DIContainer = container;
        }
        
        
    }
}
