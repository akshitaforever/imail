﻿namespace IMAIL
{
    partial class EMAIL_Account
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.ListViewGroup listViewGroup1 = new System.Windows.Forms.ListViewGroup("IMAP Accounts", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup2 = new System.Windows.Forms.ListViewGroup("POP3 Accounts", System.Windows.Forms.HorizontalAlignment.Left);
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EMAIL_Account));
            this.lv_account = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.btn_add = new System.Windows.Forms.Button();
            this.btn_edit = new System.Windows.Forms.Button();
            this.btn_delete = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cmb_type = new System.Windows.Forms.ComboBox();
            this.txt_acc_name = new System.Windows.Forms.TextBox();
            this.txt_server = new System.Windows.Forms.TextBox();
            this.txt_port = new System.Windows.Forms.TextBox();
            this.ch_in_use = new System.Windows.Forms.CheckBox();
            this.btn_ok = new System.Windows.Forms.Button();
            this.p_add = new System.Windows.Forms.Panel();
            this.ctxview = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.listToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.detailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbview = new System.Windows.Forms.ComboBox();
            this.ch_ssl = new System.Windows.Forms.CheckBox();
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.p_add.SuspendLayout();
            this.ctxview.SuspendLayout();
            this.SuspendLayout();
            // 
            // lv_account
            // 
            this.lv_account.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6});
            listViewGroup1.Header = "IMAP Accounts";
            listViewGroup1.Name = "IMAP";
            listViewGroup2.Header = "POP3 Accounts";
            listViewGroup2.Name = "POP3";
            this.lv_account.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup1,
            listViewGroup2});
            this.lv_account.LargeImageList = this.imageList1;
            this.lv_account.Location = new System.Drawing.Point(12, 25);
            this.lv_account.Name = "lv_account";
            this.lv_account.Size = new System.Drawing.Size(385, 239);
            this.lv_account.TabIndex = 0;
            this.lv_account.UseCompatibleStateImageBehavior = false;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Account Name";
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Server";
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Port";
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Type";
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "In Use";
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Icon1.ico");
            // 
            // btn_add
            // 
            this.btn_add.Location = new System.Drawing.Point(100, 278);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(95, 22);
            this.btn_add.TabIndex = 1;
            this.btn_add.Text = "&Add";
            this.btn_add.UseVisualStyleBackColor = true;
            this.btn_add.Click += new System.EventHandler(this.btn_add_Click);
            // 
            // btn_edit
            // 
            this.btn_edit.Location = new System.Drawing.Point(201, 278);
            this.btn_edit.Name = "btn_edit";
            this.btn_edit.Size = new System.Drawing.Size(95, 22);
            this.btn_edit.TabIndex = 2;
            this.btn_edit.Text = "&Edit";
            this.btn_edit.UseVisualStyleBackColor = true;
            this.btn_edit.Click += new System.EventHandler(this.btn_edit_Click);
            // 
            // btn_delete
            // 
            this.btn_delete.Location = new System.Drawing.Point(302, 278);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Size = new System.Drawing.Size(95, 22);
            this.btn_delete.TabIndex = 3;
            this.btn_delete.Text = "&Remove";
            this.btn_delete.UseVisualStyleBackColor = true;
            this.btn_delete.Click += new System.EventHandler(this.btn_delete_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "E-mail Accounts";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Account Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Incoming Server";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 92);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(26, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Port";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 131);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Type";
            // 
            // cmb_type
            // 
            this.cmb_type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_type.FormattingEnabled = true;
            this.cmb_type.Items.AddRange(new object[] {
            "IMAP",
            "POP3"});
            this.cmb_type.Location = new System.Drawing.Point(7, 147);
            this.cmb_type.Name = "cmb_type";
            this.cmb_type.Size = new System.Drawing.Size(124, 21);
            this.cmb_type.TabIndex = 10;
            // 
            // txt_acc_name
            // 
            this.txt_acc_name.Location = new System.Drawing.Point(8, 28);
            this.txt_acc_name.Name = "txt_acc_name";
            this.txt_acc_name.Size = new System.Drawing.Size(123, 20);
            this.txt_acc_name.TabIndex = 11;
            // 
            // txt_server
            // 
            this.txt_server.Location = new System.Drawing.Point(8, 69);
            this.txt_server.Name = "txt_server";
            this.txt_server.Size = new System.Drawing.Size(123, 20);
            this.txt_server.TabIndex = 12;
            // 
            // txt_port
            // 
            this.txt_port.Location = new System.Drawing.Point(8, 108);
            this.txt_port.Name = "txt_port";
            this.txt_port.Size = new System.Drawing.Size(123, 20);
            this.txt_port.TabIndex = 13;
            // 
            // ch_in_use
            // 
            this.ch_in_use.AutoSize = true;
            this.ch_in_use.Location = new System.Drawing.Point(7, 174);
            this.ch_in_use.Name = "ch_in_use";
            this.ch_in_use.Size = new System.Drawing.Size(57, 17);
            this.ch_in_use.TabIndex = 14;
            this.ch_in_use.Text = "In Use";
            this.ch_in_use.UseVisualStyleBackColor = true;
            // 
            // btn_ok
            // 
            this.btn_ok.Location = new System.Drawing.Point(7, 197);
            this.btn_ok.Name = "btn_ok";
            this.btn_ok.Size = new System.Drawing.Size(95, 22);
            this.btn_ok.TabIndex = 15;
            this.btn_ok.Text = "&OK";
            this.btn_ok.UseVisualStyleBackColor = true;
            this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
            // 
            // p_add
            // 
            this.p_add.Controls.Add(this.ch_ssl);
            this.p_add.Controls.Add(this.label2);
            this.p_add.Controls.Add(this.btn_ok);
            this.p_add.Controls.Add(this.label3);
            this.p_add.Controls.Add(this.ch_in_use);
            this.p_add.Controls.Add(this.label4);
            this.p_add.Controls.Add(this.txt_port);
            this.p_add.Controls.Add(this.label5);
            this.p_add.Controls.Add(this.txt_server);
            this.p_add.Controls.Add(this.cmb_type);
            this.p_add.Controls.Add(this.txt_acc_name);
            this.p_add.Location = new System.Drawing.Point(403, 25);
            this.p_add.Name = "p_add";
            this.p_add.Size = new System.Drawing.Size(143, 239);
            this.p_add.TabIndex = 16;
            this.p_add.Visible = false;
            // 
            // ctxview
            // 
            this.ctxview.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listToolStripMenuItem,
            this.tilesToolStripMenuItem,
            this.detailsToolStripMenuItem});
            this.ctxview.Name = "ctxview";
            this.ctxview.Size = new System.Drawing.Size(110, 70);
            // 
            // listToolStripMenuItem
            // 
            this.listToolStripMenuItem.Name = "listToolStripMenuItem";
            this.listToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.listToolStripMenuItem.Text = "List";
            // 
            // tilesToolStripMenuItem
            // 
            this.tilesToolStripMenuItem.Name = "tilesToolStripMenuItem";
            this.tilesToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.tilesToolStripMenuItem.Text = "Tiles";
            // 
            // detailsToolStripMenuItem
            // 
            this.detailsToolStripMenuItem.Name = "detailsToolStripMenuItem";
            this.detailsToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.detailsToolStripMenuItem.Text = "Details";
            // 
            // cmbview
            // 
            this.cmbview.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbview.FormattingEnabled = true;
            this.cmbview.Items.AddRange(new object[] {
            "LargeItme",
            "List",
            "Details",
            "Tiles"});
            this.cmbview.Location = new System.Drawing.Point(15, 279);
            this.cmbview.Name = "cmbview";
            this.cmbview.Size = new System.Drawing.Size(82, 21);
            this.cmbview.TabIndex = 19;
            this.cmbview.SelectedIndexChanged += new System.EventHandler(this.cmbview_SelectedIndexChanged);
            // 
            // ch_ssl
            // 
            this.ch_ssl.AutoSize = true;
            this.ch_ssl.Location = new System.Drawing.Point(70, 174);
            this.ch_ssl.Name = "ch_ssl";
            this.ch_ssl.Size = new System.Drawing.Size(46, 17);
            this.ch_ssl.TabIndex = 16;
            this.ch_ssl.Text = "SSL";
            this.ch_ssl.UseVisualStyleBackColor = true;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Use SSL";
            // 
            // EMAIL_Account
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(578, 312);
            this.Controls.Add(this.cmbview);
            this.Controls.Add(this.p_add);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_delete);
            this.Controls.Add(this.btn_edit);
            this.Controls.Add(this.btn_add);
            this.Controls.Add(this.lv_account);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EMAIL_Account";
            this.Text = "Email Accounts";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.EMAIL_Account_FormClosing);
            this.Load += new System.EventHandler(this.EMAIL_Account_Load);
            this.p_add.ResumeLayout(false);
            this.p_add.PerformLayout();
            this.ctxview.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lv_account;
        private System.Windows.Forms.Button btn_add;
        private System.Windows.Forms.Button btn_edit;
        private System.Windows.Forms.Button btn_delete;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmb_type;
        private System.Windows.Forms.TextBox txt_acc_name;
        private System.Windows.Forms.TextBox txt_server;
        private System.Windows.Forms.TextBox txt_port;
        private System.Windows.Forms.CheckBox ch_in_use;
        private System.Windows.Forms.Button btn_ok;
        private System.Windows.Forms.Panel p_add;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ContextMenuStrip ctxview;
        private System.Windows.Forms.ToolStripMenuItem listToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tilesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem detailsToolStripMenuItem;
        private System.Windows.Forms.ComboBox cmbview;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.CheckBox ch_ssl;
        private System.Windows.Forms.ColumnHeader columnHeader6;
    }
}