﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IMAIL
{
    public partial class SyncForm : Form
    {
        public SyncForm()
        {
            InitializeComponent();
        }
        public void AddMessage(string message)
        {
            if(lv_status.Items.Count>0)
            {
                lv_status.Items[lv_status.Items.Count - 1].ImageKey = "done";
            }
            lv_status.Items.Add(message, "process");
        }
        public void SetError()
        {
            lv_status.Items[lv_status.Items.Count - 1].ImageKey = "error";
        }
    }
}
