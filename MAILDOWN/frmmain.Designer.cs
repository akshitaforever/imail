﻿namespace IMAIL
{
    partial class frmmain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmmain));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.btn_down_all = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.stl_connection = new System.Windows.Forms.ToolStripStatusLabel();
            this.tl_pg = new System.Windows.Forms.ToolStripProgressBar();
            this.lv_info = new System.Windows.Forms.ListView();
            this.Action = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.pg_download_comp = new System.Windows.Forms.ProgressBar();
            this.btn_cancel = new System.Windows.Forms.Button();
            this.tb_download = new System.Windows.Forms.TabControl();
            this.tb_search = new System.Windows.Forms.TabPage();
            this.pnl_search = new System.Windows.Forms.Panel();
            this.btn_get = new System.Windows.Forms.Button();
            this.btn_search = new System.Windows.Forms.Button();
            this.gb_criteria = new System.Windows.Forms.GroupBox();
            this.txt_text = new System.Windows.Forms.TextBox();
            this.ch_text = new System.Windows.Forms.CheckBox();
            this.txt_body_search = new System.Windows.Forms.TextBox();
            this.cmb_date_search = new System.Windows.Forms.ComboBox();
            this.dt_date = new System.Windows.Forms.DateTimePicker();
            this.txt_subject_search = new System.Windows.Forms.TextBox();
            this.txt_from_search = new System.Windows.Forms.TextBox();
            this.ch_body_search = new System.Windows.Forms.CheckBox();
            this.ch_date_search = new System.Windows.Forms.CheckBox();
            this.ch_subject = new System.Windows.Forms.CheckBox();
            this.ch_from = new System.Windows.Forms.CheckBox();
            this.gb_flags = new System.Windows.Forms.GroupBox();
            this.op_seen = new System.Windows.Forms.RadioButton();
            this.op_all = new System.Windows.Forms.RadioButton();
            this.op_new = new System.Windows.Forms.RadioButton();
            this.op_recent = new System.Windows.Forms.RadioButton();
            this.op_unseen = new System.Windows.Forms.RadioButton();
            this.tb_downloadoption = new System.Windows.Forms.TabPage();
            this.btn_save_config = new System.Windows.Forms.Button();
            this.cmb_file_action = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btn_browse = new System.Windows.Forms.Button();
            this.txt_path = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rd_all = new System.Windows.Forms.RadioButton();
            this.rd_no = new System.Windows.Forms.RadioButton();
            this.txt_nom = new System.Windows.Forms.TextBox();
            this.tb_search_results = new System.Windows.Forms.TabPage();
            this.btn_deselect = new System.Windows.Forms.Button();
            this.btn_select_all = new System.Windows.Forms.Button();
            this.lv_results = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tb_pop_options = new System.Windows.Forms.TabPage();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.btn_set_pop_user = new System.Windows.Forms.Button();
            this.btn_set_last = new System.Windows.Forms.Button();
            this.txt_mail_no = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.fbd1 = new System.Windows.Forms.FolderBrowserDialog();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cmb_account = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_password = new System.Windows.Forms.TextBox();
            this.txt_username = new System.Windows.Forms.TextBox();
            this.btn_login = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.pg_download_overall = new System.Windows.Forms.ProgressBar();
            this.nf = new System.Windows.Forms.NotifyIcon(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.statusStrip1.SuspendLayout();
            this.tb_download.SuspendLayout();
            this.tb_search.SuspendLayout();
            this.pnl_search.SuspendLayout();
            this.gb_criteria.SuspendLayout();
            this.gb_flags.SuspendLayout();
            this.tb_downloadoption.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tb_search_results.SuspendLayout();
            this.tb_pop_options.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "images.jpeg");
            this.imageList1.Images.SetKeyName(1, "file.jpeg");
            // 
            // btn_down_all
            // 
            this.btn_down_all.Enabled = false;
            this.btn_down_all.Location = new System.Drawing.Point(576, 284);
            this.btn_down_all.Name = "btn_down_all";
            this.btn_down_all.Size = new System.Drawing.Size(217, 24);
            this.btn_down_all.TabIndex = 11;
            this.btn_down_all.Text = "Download";
            this.btn_down_all.UseVisualStyleBackColor = true;
            this.btn_down_all.Click += new System.EventHandler(this.btn_down_all_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stl_connection,
            this.tl_pg});
            this.statusStrip1.Location = new System.Drawing.Point(0, 504);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(845, 22);
            this.statusStrip1.TabIndex = 12;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // stl_connection
            // 
            this.stl_connection.Name = "stl_connection";
            this.stl_connection.Size = new System.Drawing.Size(79, 17);
            this.stl_connection.Text = "Disconnected";
            // 
            // tl_pg
            // 
            this.tl_pg.Name = "tl_pg";
            this.tl_pg.Size = new System.Drawing.Size(100, 16);
            this.tl_pg.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            // 
            // lv_info
            // 
            this.lv_info.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Action});
            this.lv_info.FullRowSelect = true;
            this.lv_info.Location = new System.Drawing.Point(12, 310);
            this.lv_info.Name = "lv_info";
            this.lv_info.Size = new System.Drawing.Size(554, 156);
            this.lv_info.TabIndex = 14;
            this.lv_info.UseCompatibleStateImageBehavior = false;
            this.lv_info.View = System.Windows.Forms.View.Details;
            // 
            // Action
            // 
            this.Action.Text = "Action";
            this.Action.Width = 512;
            // 
            // pg_download_comp
            // 
            this.pg_download_comp.Location = new System.Drawing.Point(591, 350);
            this.pg_download_comp.Name = "pg_download_comp";
            this.pg_download_comp.Size = new System.Drawing.Size(188, 19);
            this.pg_download_comp.TabIndex = 15;
            // 
            // btn_cancel
            // 
            this.btn_cancel.Enabled = false;
            this.btn_cancel.Location = new System.Drawing.Point(609, 417);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(142, 23);
            this.btn_cancel.TabIndex = 16;
            this.btn_cancel.Text = "Skip Current Item Downloading";
            this.btn_cancel.UseVisualStyleBackColor = true;
            this.btn_cancel.Visible = false;
            this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
            // 
            // tb_download
            // 
            this.tb_download.Controls.Add(this.tb_search);
            this.tb_download.Controls.Add(this.tb_downloadoption);
            this.tb_download.Controls.Add(this.tb_search_results);
            this.tb_download.Controls.Add(this.tb_pop_options);
            this.tb_download.Location = new System.Drawing.Point(12, 42);
            this.tb_download.Name = "tb_download";
            this.tb_download.SelectedIndex = 0;
            this.tb_download.Size = new System.Drawing.Size(558, 266);
            this.tb_download.TabIndex = 23;
            // 
            // tb_search
            // 
            this.tb_search.Controls.Add(this.pnl_search);
            this.tb_search.Location = new System.Drawing.Point(4, 22);
            this.tb_search.Name = "tb_search";
            this.tb_search.Padding = new System.Windows.Forms.Padding(3);
            this.tb_search.Size = new System.Drawing.Size(550, 240);
            this.tb_search.TabIndex = 0;
            this.tb_search.Text = "SearchOptions";
            this.tb_search.UseVisualStyleBackColor = true;
            // 
            // pnl_search
            // 
            this.pnl_search.Controls.Add(this.btn_get);
            this.pnl_search.Controls.Add(this.btn_search);
            this.pnl_search.Controls.Add(this.gb_criteria);
            this.pnl_search.Controls.Add(this.gb_flags);
            this.pnl_search.Location = new System.Drawing.Point(6, 6);
            this.pnl_search.Name = "pnl_search";
            this.pnl_search.Size = new System.Drawing.Size(538, 228);
            this.pnl_search.TabIndex = 25;
            // 
            // btn_get
            // 
            this.btn_get.Enabled = false;
            this.btn_get.Location = new System.Drawing.Point(210, 197);
            this.btn_get.Name = "btn_get";
            this.btn_get.Size = new System.Drawing.Size(140, 27);
            this.btn_get.TabIndex = 24;
            this.btn_get.Text = "&Get Details";
            this.btn_get.UseVisualStyleBackColor = true;
            this.btn_get.Click += new System.EventHandler(this.btn_get_Click_1);
            // 
            // btn_search
            // 
            this.btn_search.Enabled = false;
            this.btn_search.Location = new System.Drawing.Point(356, 197);
            this.btn_search.Name = "btn_search";
            this.btn_search.Size = new System.Drawing.Size(140, 27);
            this.btn_search.TabIndex = 23;
            this.btn_search.Text = "Search";
            this.btn_search.UseVisualStyleBackColor = true;
            this.btn_search.Click += new System.EventHandler(this.btn_search_Click);
            // 
            // gb_criteria
            // 
            this.gb_criteria.Controls.Add(this.txt_text);
            this.gb_criteria.Controls.Add(this.ch_text);
            this.gb_criteria.Controls.Add(this.txt_body_search);
            this.gb_criteria.Controls.Add(this.cmb_date_search);
            this.gb_criteria.Controls.Add(this.dt_date);
            this.gb_criteria.Controls.Add(this.txt_subject_search);
            this.gb_criteria.Controls.Add(this.txt_from_search);
            this.gb_criteria.Controls.Add(this.ch_body_search);
            this.gb_criteria.Controls.Add(this.ch_date_search);
            this.gb_criteria.Controls.Add(this.ch_subject);
            this.gb_criteria.Controls.Add(this.ch_from);
            this.gb_criteria.Location = new System.Drawing.Point(12, 52);
            this.gb_criteria.Name = "gb_criteria";
            this.gb_criteria.Size = new System.Drawing.Size(484, 139);
            this.gb_criteria.TabIndex = 22;
            this.gb_criteria.TabStop = false;
            this.gb_criteria.Text = "Criteria";
            // 
            // txt_text
            // 
            this.txt_text.Enabled = false;
            this.txt_text.Location = new System.Drawing.Point(175, 119);
            this.txt_text.Name = "txt_text";
            this.txt_text.Size = new System.Drawing.Size(293, 20);
            this.txt_text.TabIndex = 10;
            // 
            // ch_text
            // 
            this.ch_text.AutoSize = true;
            this.ch_text.Location = new System.Drawing.Point(32, 123);
            this.ch_text.Name = "ch_text";
            this.ch_text.Size = new System.Drawing.Size(54, 17);
            this.ch_text.TabIndex = 9;
            this.ch_text.Text = "TEXT";
            this.ch_text.UseVisualStyleBackColor = true;
            this.ch_text.CheckedChanged += new System.EventHandler(this.ch_text_CheckedChanged);
            // 
            // txt_body_search
            // 
            this.txt_body_search.Enabled = false;
            this.txt_body_search.Location = new System.Drawing.Point(175, 95);
            this.txt_body_search.Name = "txt_body_search";
            this.txt_body_search.Size = new System.Drawing.Size(293, 20);
            this.txt_body_search.TabIndex = 8;
            // 
            // cmb_date_search
            // 
            this.cmb_date_search.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_date_search.Enabled = false;
            this.cmb_date_search.FormattingEnabled = true;
            this.cmb_date_search.Items.AddRange(new object[] {
            "SINCE",
            "SENTON",
            "SENTSINCE",
            "SENTBEFORE"});
            this.cmb_date_search.Location = new System.Drawing.Point(175, 68);
            this.cmb_date_search.Name = "cmb_date_search";
            this.cmb_date_search.Size = new System.Drawing.Size(148, 21);
            this.cmb_date_search.TabIndex = 7;
            // 
            // dt_date
            // 
            this.dt_date.CustomFormat = "dd-MMM-yyyy";
            this.dt_date.Enabled = false;
            this.dt_date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dt_date.Location = new System.Drawing.Point(329, 67);
            this.dt_date.Name = "dt_date";
            this.dt_date.Size = new System.Drawing.Size(139, 20);
            this.dt_date.TabIndex = 6;
            // 
            // txt_subject_search
            // 
            this.txt_subject_search.Enabled = false;
            this.txt_subject_search.Location = new System.Drawing.Point(175, 42);
            this.txt_subject_search.Name = "txt_subject_search";
            this.txt_subject_search.Size = new System.Drawing.Size(294, 20);
            this.txt_subject_search.TabIndex = 5;
            // 
            // txt_from_search
            // 
            this.txt_from_search.Enabled = false;
            this.txt_from_search.Location = new System.Drawing.Point(175, 18);
            this.txt_from_search.Name = "txt_from_search";
            this.txt_from_search.Size = new System.Drawing.Size(294, 20);
            this.txt_from_search.TabIndex = 4;
            // 
            // ch_body_search
            // 
            this.ch_body_search.AutoSize = true;
            this.ch_body_search.Location = new System.Drawing.Point(32, 99);
            this.ch_body_search.Name = "ch_body_search";
            this.ch_body_search.Size = new System.Drawing.Size(56, 17);
            this.ch_body_search.TabIndex = 3;
            this.ch_body_search.Text = "BODY";
            this.ch_body_search.UseVisualStyleBackColor = true;
            this.ch_body_search.CheckedChanged += new System.EventHandler(this.ch_body_search_CheckedChanged);
            // 
            // ch_date_search
            // 
            this.ch_date_search.AutoSize = true;
            this.ch_date_search.Location = new System.Drawing.Point(32, 73);
            this.ch_date_search.Name = "ch_date_search";
            this.ch_date_search.Size = new System.Drawing.Size(49, 17);
            this.ch_date_search.TabIndex = 2;
            this.ch_date_search.Text = "Date";
            this.ch_date_search.UseVisualStyleBackColor = true;
            this.ch_date_search.CheckedChanged += new System.EventHandler(this.ch_date_search_CheckedChanged);
            // 
            // ch_subject
            // 
            this.ch_subject.AutoSize = true;
            this.ch_subject.Location = new System.Drawing.Point(32, 46);
            this.ch_subject.Name = "ch_subject";
            this.ch_subject.Size = new System.Drawing.Size(62, 17);
            this.ch_subject.TabIndex = 1;
            this.ch_subject.Text = "Subject";
            this.ch_subject.UseVisualStyleBackColor = true;
            this.ch_subject.CheckedChanged += new System.EventHandler(this.ch_subject_CheckedChanged);
            // 
            // ch_from
            // 
            this.ch_from.AutoSize = true;
            this.ch_from.Location = new System.Drawing.Point(33, 21);
            this.ch_from.Name = "ch_from";
            this.ch_from.Size = new System.Drawing.Size(49, 17);
            this.ch_from.TabIndex = 0;
            this.ch_from.Text = "From";
            this.ch_from.UseVisualStyleBackColor = true;
            this.ch_from.CheckedChanged += new System.EventHandler(this.ch_from_CheckedChanged_1);
            // 
            // gb_flags
            // 
            this.gb_flags.Controls.Add(this.op_seen);
            this.gb_flags.Controls.Add(this.op_all);
            this.gb_flags.Controls.Add(this.op_new);
            this.gb_flags.Controls.Add(this.op_recent);
            this.gb_flags.Controls.Add(this.op_unseen);
            this.gb_flags.Location = new System.Drawing.Point(12, 3);
            this.gb_flags.Name = "gb_flags";
            this.gb_flags.Size = new System.Drawing.Size(523, 43);
            this.gb_flags.TabIndex = 21;
            this.gb_flags.TabStop = false;
            this.gb_flags.Text = "Search in";
            // 
            // op_seen
            // 
            this.op_seen.AutoSize = true;
            this.op_seen.Location = new System.Drawing.Point(399, 19);
            this.op_seen.Name = "op_seen";
            this.op_seen.Size = new System.Drawing.Size(54, 17);
            this.op_seen.TabIndex = 20;
            this.op_seen.Text = "SEEN";
            this.op_seen.UseVisualStyleBackColor = true;
            // 
            // op_all
            // 
            this.op_all.AutoSize = true;
            this.op_all.Location = new System.Drawing.Point(215, 19);
            this.op_all.Name = "op_all";
            this.op_all.Size = new System.Drawing.Size(44, 17);
            this.op_all.TabIndex = 17;
            this.op_all.Text = "ALL";
            this.op_all.UseVisualStyleBackColor = true;
            // 
            // op_new
            // 
            this.op_new.AutoSize = true;
            this.op_new.Checked = true;
            this.op_new.Location = new System.Drawing.Point(125, 19);
            this.op_new.Name = "op_new";
            this.op_new.Size = new System.Drawing.Size(51, 17);
            this.op_new.TabIndex = 19;
            this.op_new.TabStop = true;
            this.op_new.Text = "NEW";
            this.op_new.UseVisualStyleBackColor = true;
            // 
            // op_recent
            // 
            this.op_recent.AutoSize = true;
            this.op_recent.Location = new System.Drawing.Point(299, 19);
            this.op_recent.Name = "op_recent";
            this.op_recent.Size = new System.Drawing.Size(69, 17);
            this.op_recent.TabIndex = 18;
            this.op_recent.Text = "RECENT";
            this.op_recent.UseVisualStyleBackColor = true;
            // 
            // op_unseen
            // 
            this.op_unseen.AutoSize = true;
            this.op_unseen.Location = new System.Drawing.Point(32, 19);
            this.op_unseen.Name = "op_unseen";
            this.op_unseen.Size = new System.Drawing.Size(70, 17);
            this.op_unseen.TabIndex = 14;
            this.op_unseen.Text = "UNSEEN";
            this.op_unseen.UseVisualStyleBackColor = true;
            // 
            // tb_downloadoption
            // 
            this.tb_downloadoption.Controls.Add(this.btn_save_config);
            this.tb_downloadoption.Controls.Add(this.cmb_file_action);
            this.tb_downloadoption.Controls.Add(this.label5);
            this.tb_downloadoption.Controls.Add(this.btn_browse);
            this.tb_downloadoption.Controls.Add(this.txt_path);
            this.tb_downloadoption.Controls.Add(this.label4);
            this.tb_downloadoption.Controls.Add(this.groupBox1);
            this.tb_downloadoption.Location = new System.Drawing.Point(4, 22);
            this.tb_downloadoption.Name = "tb_downloadoption";
            this.tb_downloadoption.Padding = new System.Windows.Forms.Padding(3);
            this.tb_downloadoption.Size = new System.Drawing.Size(550, 240);
            this.tb_downloadoption.TabIndex = 1;
            this.tb_downloadoption.Text = "Download Options";
            this.tb_downloadoption.UseVisualStyleBackColor = true;
            // 
            // btn_save_config
            // 
            this.btn_save_config.Location = new System.Drawing.Point(181, 203);
            this.btn_save_config.Name = "btn_save_config";
            this.btn_save_config.Size = new System.Drawing.Size(199, 31);
            this.btn_save_config.TabIndex = 29;
            this.btn_save_config.Text = "Save Config";
            this.btn_save_config.UseVisualStyleBackColor = true;
            this.btn_save_config.Click += new System.EventHandler(this.btn_save_config_Click);
            // 
            // cmb_file_action
            // 
            this.cmb_file_action.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_file_action.FormattingEnabled = true;
            this.cmb_file_action.Items.AddRange(new object[] {
            "Rename",
            "Replace"});
            this.cmb_file_action.Location = new System.Drawing.Point(96, 170);
            this.cmb_file_action.Name = "cmb_file_action";
            this.cmb_file_action.Size = new System.Drawing.Size(143, 21);
            this.cmb_file_action.TabIndex = 28;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(18, 173);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 13);
            this.label5.TabIndex = 27;
            this.label5.Text = "If File Exists";
            // 
            // btn_browse
            // 
            this.btn_browse.Location = new System.Drawing.Point(447, 146);
            this.btn_browse.Name = "btn_browse";
            this.btn_browse.Size = new System.Drawing.Size(87, 24);
            this.btn_browse.TabIndex = 26;
            this.btn_browse.Text = "&Browse";
            this.btn_browse.UseVisualStyleBackColor = true;
            this.btn_browse.Click += new System.EventHandler(this.btn_browse_Click);
            // 
            // txt_path
            // 
            this.txt_path.Location = new System.Drawing.Point(17, 117);
            this.txt_path.Name = "txt_path";
            this.txt_path.ReadOnly = true;
            this.txt_path.Size = new System.Drawing.Size(518, 20);
            this.txt_path.TabIndex = 25;
            this.txt_path.Text = "C:\\Downloads";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 96);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 13);
            this.label4.TabIndex = 24;
            this.label4.Text = "Download Location";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rd_all);
            this.groupBox1.Controls.Add(this.rd_no);
            this.groupBox1.Controls.Add(this.txt_nom);
            this.groupBox1.Location = new System.Drawing.Point(18, 19);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(178, 68);
            this.groupBox1.TabIndex = 23;
            this.groupBox1.TabStop = false;
            // 
            // rd_all
            // 
            this.rd_all.AutoSize = true;
            this.rd_all.Checked = true;
            this.rd_all.Location = new System.Drawing.Point(6, 11);
            this.rd_all.Name = "rd_all";
            this.rd_all.Size = new System.Drawing.Size(36, 17);
            this.rd_all.TabIndex = 21;
            this.rd_all.TabStop = true;
            this.rd_all.Text = "All";
            this.rd_all.UseVisualStyleBackColor = true;
            this.rd_all.CheckedChanged += new System.EventHandler(this.rd_all_CheckedChanged);
            // 
            // rd_no
            // 
            this.rd_no.AutoSize = true;
            this.rd_no.Location = new System.Drawing.Point(6, 38);
            this.rd_no.Name = "rd_no";
            this.rd_no.Size = new System.Drawing.Size(74, 17);
            this.rd_no.TabIndex = 20;
            this.rd_no.Text = "No of Msg";
            this.rd_no.UseVisualStyleBackColor = true;
            this.rd_no.CheckedChanged += new System.EventHandler(this.rd_no_CheckedChanged);
            // 
            // txt_nom
            // 
            this.txt_nom.Location = new System.Drawing.Point(95, 37);
            this.txt_nom.Name = "txt_nom";
            this.txt_nom.ReadOnly = true;
            this.txt_nom.Size = new System.Drawing.Size(37, 20);
            this.txt_nom.TabIndex = 13;
            this.txt_nom.Text = "10";
            // 
            // tb_search_results
            // 
            this.tb_search_results.Controls.Add(this.btn_deselect);
            this.tb_search_results.Controls.Add(this.btn_select_all);
            this.tb_search_results.Controls.Add(this.lv_results);
            this.tb_search_results.Location = new System.Drawing.Point(4, 22);
            this.tb_search_results.Name = "tb_search_results";
            this.tb_search_results.Size = new System.Drawing.Size(550, 240);
            this.tb_search_results.TabIndex = 2;
            this.tb_search_results.Text = "Search Results";
            this.tb_search_results.UseVisualStyleBackColor = true;
            // 
            // btn_deselect
            // 
            this.btn_deselect.Location = new System.Drawing.Point(305, 184);
            this.btn_deselect.Name = "btn_deselect";
            this.btn_deselect.Size = new System.Drawing.Size(112, 27);
            this.btn_deselect.TabIndex = 2;
            this.btn_deselect.Text = "DeSelect All";
            this.btn_deselect.UseVisualStyleBackColor = true;
            this.btn_deselect.Click += new System.EventHandler(this.btn_deselect_Click);
            // 
            // btn_select_all
            // 
            this.btn_select_all.Location = new System.Drawing.Point(187, 184);
            this.btn_select_all.Name = "btn_select_all";
            this.btn_select_all.Size = new System.Drawing.Size(112, 27);
            this.btn_select_all.TabIndex = 1;
            this.btn_select_all.Text = "Select All";
            this.btn_select_all.UseVisualStyleBackColor = true;
            this.btn_select_all.Click += new System.EventHandler(this.btn_select_all_Click);
            // 
            // lv_results
            // 
            this.lv_results.CheckBoxes = true;
            this.lv_results.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.lv_results.FullRowSelect = true;
            this.lv_results.Location = new System.Drawing.Point(14, 14);
            this.lv_results.Name = "lv_results";
            this.lv_results.Size = new System.Drawing.Size(522, 155);
            this.lv_results.TabIndex = 0;
            this.lv_results.UseCompatibleStateImageBehavior = false;
            this.lv_results.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Date";
            this.columnHeader1.Width = 150;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Subject";
            this.columnHeader2.Width = 86;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "From";
            this.columnHeader3.Width = 368;
            // 
            // tb_pop_options
            // 
            this.tb_pop_options.Controls.Add(this.label11);
            this.tb_pop_options.Controls.Add(this.label10);
            this.tb_pop_options.Controls.Add(this.label9);
            this.tb_pop_options.Controls.Add(this.btn_set_pop_user);
            this.tb_pop_options.Controls.Add(this.btn_set_last);
            this.tb_pop_options.Controls.Add(this.txt_mail_no);
            this.tb_pop_options.Controls.Add(this.label8);
            this.tb_pop_options.Location = new System.Drawing.Point(4, 22);
            this.tb_pop_options.Name = "tb_pop_options";
            this.tb_pop_options.Size = new System.Drawing.Size(550, 240);
            this.tb_pop_options.TabIndex = 3;
            this.tb_pop_options.Text = "POP3 Mail Options";
            this.tb_pop_options.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(3, 96);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(544, 134);
            this.label11.TabIndex = 6;
            this.label11.Text = resources.GetString("label11.Text");
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(357, 53);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(133, 13);
            this.label10.TabIndex = 5;
            this.label10.Text = "Sets the Explicitily Mail No,";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(357, 23);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(149, 13);
            this.label9.TabIndex = 4;
            this.label9.Text = "Sets the Last mail no recieved";
            // 
            // btn_set_pop_user
            // 
            this.btn_set_pop_user.Location = new System.Drawing.Point(212, 47);
            this.btn_set_pop_user.Name = "btn_set_pop_user";
            this.btn_set_pop_user.Size = new System.Drawing.Size(135, 25);
            this.btn_set_pop_user.TabIndex = 3;
            this.btn_set_pop_user.Text = "Set User";
            this.btn_set_pop_user.UseVisualStyleBackColor = true;
            this.btn_set_pop_user.Click += new System.EventHandler(this.btn_set_pop_user_Click);
            // 
            // btn_set_last
            // 
            this.btn_set_last.Enabled = false;
            this.btn_set_last.Location = new System.Drawing.Point(212, 16);
            this.btn_set_last.Name = "btn_set_last";
            this.btn_set_last.Size = new System.Drawing.Size(135, 25);
            this.btn_set_last.TabIndex = 2;
            this.btn_set_last.Text = "Set Last";
            this.btn_set_last.UseVisualStyleBackColor = true;
            this.btn_set_last.Click += new System.EventHandler(this.btn_set_last_Click);
            // 
            // txt_mail_no
            // 
            this.txt_mail_no.Location = new System.Drawing.Point(132, 19);
            this.txt_mail_no.Name = "txt_mail_no";
            this.txt_mail_no.Size = new System.Drawing.Size(46, 20);
            this.txt_mail_no.TabIndex = 1;
            this.txt_mail_no.Text = "0";
            this.txt_mail_no.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.validatedigit);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(18, 22);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(108, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Last Fetched Mail No";
            // 
            // fbd1
            // 
            this.fbd1.Description = "Choose Location where Downloaded Files will be saved";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cmb_account);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.txt_password);
            this.groupBox2.Controls.Add(this.txt_username);
            this.groupBox2.Controls.Add(this.btn_login);
            this.groupBox2.Location = new System.Drawing.Point(572, 57);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(221, 218);
            this.groupBox2.TabIndex = 24;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Login";
            // 
            // cmb_account
            // 
            this.cmb_account.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_account.FormattingEnabled = true;
            this.cmb_account.Location = new System.Drawing.Point(33, 165);
            this.cmb_account.Name = "cmb_account";
            this.cmb_account.Size = new System.Drawing.Size(146, 21);
            this.cmb_account.TabIndex = 14;
            this.cmb_account.SelectedIndexChanged += new System.EventHandler(this.cmb_account_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(30, 149);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "IMAP Account";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(30, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Password";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(30, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "UserName";
            // 
            // txt_password
            // 
            this.txt_password.Location = new System.Drawing.Point(33, 90);
            this.txt_password.Name = "txt_password";
            this.txt_password.PasswordChar = '*';
            this.txt_password.Size = new System.Drawing.Size(146, 20);
            this.txt_password.TabIndex = 10;
            this.txt_password.Text = "abhinav@123";
            // 
            // txt_username
            // 
            this.txt_username.Location = new System.Drawing.Point(33, 51);
            this.txt_username.Name = "txt_username";
            this.txt_username.Size = new System.Drawing.Size(146, 20);
            this.txt_username.TabIndex = 9;
            this.txt_username.Text = "abhinav.s@droisys.com";
            // 
            // btn_login
            // 
            this.btn_login.Location = new System.Drawing.Point(33, 116);
            this.btn_login.Name = "btn_login";
            this.btn_login.Size = new System.Drawing.Size(84, 22);
            this.btn_login.TabIndex = 8;
            this.btn_login.Text = "Login";
            this.btn_login.UseVisualStyleBackColor = true;
            this.btn_login.Click += new System.EventHandler(this.btn_login_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(588, 334);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(105, 13);
            this.label6.TabIndex = 25;
            this.label6.Text = "Component Progress";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(588, 376);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(84, 13);
            this.label7.TabIndex = 27;
            this.label7.Text = "Overall Progress";
            // 
            // pg_download_overall
            // 
            this.pg_download_overall.Location = new System.Drawing.Point(591, 392);
            this.pg_download_overall.Name = "pg_download_overall";
            this.pg_download_overall.Size = new System.Drawing.Size(188, 19);
            this.pg_download_overall.TabIndex = 26;
            // 
            // nf
            // 
            this.nf.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.nf.BalloonTipText = "All Attachements  Finished Downloading";
            this.nf.Icon = ((System.Drawing.Icon)(resources.GetObject("nf.Icon")));
            this.nf.Text = "Mail Downloader";
            this.nf.Visible = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(474, 15);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(102, 27);
            this.button1.TabIndex = 28;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // frmmain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(845, 526);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.pg_download_overall);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.tb_download);
            this.Controls.Add(this.btn_cancel);
            this.Controls.Add(this.pg_download_comp);
            this.Controls.Add(this.lv_info);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.btn_down_all);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frmmain";
            this.Text = "MAIL DownLoader";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmmain_FormClosing);
            this.Load += new System.EventHandler(this.frmmain_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tb_download.ResumeLayout(false);
            this.tb_search.ResumeLayout(false);
            this.pnl_search.ResumeLayout(false);
            this.gb_criteria.ResumeLayout(false);
            this.gb_criteria.PerformLayout();
            this.gb_flags.ResumeLayout(false);
            this.gb_flags.PerformLayout();
            this.tb_downloadoption.ResumeLayout(false);
            this.tb_downloadoption.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tb_search_results.ResumeLayout(false);
            this.tb_pop_options.ResumeLayout(false);
            this.tb_pop_options.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_down_all;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel stl_connection;
        private System.Windows.Forms.ToolStripProgressBar tl_pg;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ListView lv_info;
        private System.Windows.Forms.ColumnHeader Action;
        private System.Windows.Forms.ProgressBar pg_download_comp;
        private System.Windows.Forms.Button btn_cancel;
        private System.Windows.Forms.TabControl tb_download;
        private System.Windows.Forms.TabPage tb_search;
        private System.Windows.Forms.TabPage tb_downloadoption;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rd_all;
        private System.Windows.Forms.RadioButton rd_no;
        private System.Windows.Forms.TextBox txt_nom;
        private System.Windows.Forms.ComboBox cmb_file_action;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btn_browse;
        private System.Windows.Forms.TextBox txt_path;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btn_save_config;
        private System.Windows.Forms.FolderBrowserDialog fbd1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox cmb_account;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_password;
        private System.Windows.Forms.TextBox txt_username;
        private System.Windows.Forms.Button btn_login;
        private System.Windows.Forms.Panel pnl_search;
        private System.Windows.Forms.GroupBox gb_criteria;
        private System.Windows.Forms.TextBox txt_body_search;
        private System.Windows.Forms.ComboBox cmb_date_search;
        private System.Windows.Forms.DateTimePicker dt_date;
        private System.Windows.Forms.TextBox txt_subject_search;
        private System.Windows.Forms.TextBox txt_from_search;
        private System.Windows.Forms.CheckBox ch_body_search;
        private System.Windows.Forms.CheckBox ch_date_search;
        private System.Windows.Forms.CheckBox ch_subject;
        private System.Windows.Forms.CheckBox ch_from;
        private System.Windows.Forms.GroupBox gb_flags;
        private System.Windows.Forms.RadioButton op_all;
        private System.Windows.Forms.RadioButton op_new;
        private System.Windows.Forms.RadioButton op_recent;
        private System.Windows.Forms.RadioButton op_unseen;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ProgressBar pg_download_overall;
        private System.Windows.Forms.Button btn_search;
        private System.Windows.Forms.RadioButton op_seen;
        private System.Windows.Forms.TabPage tb_search_results;
        private System.Windows.Forms.ListView lv_results;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Button btn_get;
        private System.Windows.Forms.Button btn_deselect;
        private System.Windows.Forms.Button btn_select_all;
        private System.Windows.Forms.TabPage tb_pop_options;
        private System.Windows.Forms.Button btn_set_pop_user;
        private System.Windows.Forms.Button btn_set_last;
        private System.Windows.Forms.TextBox txt_mail_no;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.NotifyIcon nf;
        private System.Windows.Forms.TextBox txt_text;
        private System.Windows.Forms.CheckBox ch_text;
        private System.Windows.Forms.Button button1;
    }
}