﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using DB;
using NetworkLayer;
using Pipeline.ActionDependency;
using Pipeline.BoxTypes;
using IMAIL.Models;
using IMAIL.Configuration;
namespace IMAIL.AccountSetup
{
    public static class AccountSync
    {

        public static void InitializeSync(MailAccount acc, NetworkCredential nc)
        {


            var dbcontext = Properties.Settings.Default.DIContainer.GetInstance<IContext<Models.MailProfile>>();
            if (dbcontext.CreateDB(AppConfiguration.GetSetting(AppConfiguration.SettingType.ConnectionString)))
            {
                MailProfile mprofile = new MailProfile()
                {
                    AccountID = acc.Name
                };
                //get list of folders
                SyncForm frm = new SyncForm();
                frm.AddMessage("Logging in to " + acc.Server);
                IMAPClient client = new IMAPClient(acc.Server);
                ObjectRegistry registry = new ObjectRegistry();
                registry.AddObject(client, ObjectEntry.ObjectLockType.Instance);
                ValueDependency<BoxedBoolean> localdep = new ValueDependency<BoxedBoolean>();
                localdep.PushValueDependency(() =>
                {
                    return new BoxedBoolean(true);
                }, "true", Enums.ActionPredicate.CompareValue);


            }
        }
    }

}
