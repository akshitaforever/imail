﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IMAIL
{
    public partial class PasswordDialog : Form
    {
        public string Password { get; set; }
        public string Username { get; set; }
        public string Account { get; set; }
        public bool Remember { get; set; }
        public PasswordDialog()
        {
            InitializeComponent();
        }
        public PasswordDialog(string username,string accname,bool @default=true):this()
        {
            this.Account = accname;
            this.Username = username;
            txt_account.ReadOnly = txt_username.ReadOnly = @default;
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            this.Password = txt_password.Text;
            Remember = chk_remember.Checked;
            this.DialogResult = DialogResult.OK;
        }
    }
}
