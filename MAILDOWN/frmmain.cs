﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net.Mail;
using NetworkLayer;
using System.Threading;
using System.IO;
using IMAIL.Models;
namespace IMAIL
{
    public partial class frmmain : Form
    {

        int _downloaded_items = 0;
        Boolean _isbroken = false;
        Dictionary<string, MailAccount> i_acc = new Dictionary<string, MailAccount>();
        string hostname = "";
        int portno = 143;
        Boolean _last_operation_completed = true;
        private string ACC_FILE_PATH = System.Windows.Forms.Application.StartupPath + "\\app.ini";
        private string COMMONS_FILE_PATH = System.Windows.Forms.Application.StartupPath + "\\conf.ini";
        long msgcount;
        long msglength;
        List<string> searchlist = new List<string>();
        Boolean loginmode = false;
        Boolean isconnected = false;
        Boolean islogin = false;
        Thread _connect = null;
        MailAccountType mailmode;
        private Boolean _state = false;//not connecting
        IMAPClient ic = null;
        POP3Client pc = null;
        string _login_prev_string = "Login";
        int _last_id = 0;
        public frmmain()
        {
            InitializeComponent();
            load_account_data();
        }

        private void btn_login_Click(object sender, EventArgs e)
        {
            if (txt_username.Text.Length == 0 || txt_password.Text.Length == 0)
            {
                MessageBox.Show("UserName/Or Password is empty");
            }
            else
            {
                handlelogin();
            }
        }

        private void handlelogin()
        {
            update_login_button_string();
            MailAccount _mail_account = i_acc[cmb_account.Text];
            if (_state == false)
            {
                if (islogin == false)
                {

                    loginmode = true;
                    tl_pg.Visible = true;
                    btn_login.Text = "Cancel";
                    stl_connection.Text = "Connecting";
                    if (_mail_account.Type == MailAccountType.IMAP)
                    {
                        ic = new IMAPClient(_mail_account.Server.HostName, _mail_account.Server.Port, txt_username.Text, txt_password.Text, false);
                        _connect = new Thread(IMAPLogin);
                        _state = true;
                        _connect.IsBackground = true;
                        _connect.Start();
                    }
                    else
                    {
                        pc = new POP3Client(_mail_account.Server.HostName, _mail_account.Server.Port, txt_username.Text, txt_password.Text, false);
                        _connect = new Thread(POPLogin);
                        _state = true;
                        _connect.IsBackground = true;
                        _connect.Start();
                    }
                }
                else
                {
                    tl_pg.Visible = true;
                    loginmode = false;
                    stl_connection.Text = "Disconnecting";
                    if (_mail_account.Type == MailAccountType.IMAP)
                    {
                        _connect.IsBackground = true;
                        _connect = new Thread(IMAPLogout);
                        _state = true;
                        _connect.Start();
                    }
                    else
                    {
                        _connect.IsBackground = true;
                        _connect = new Thread(POPLogout);
                        _state = true;
                        _connect.Start();
                    }
                }
            }
            else
            {
                
                tl_pg.Visible = false;
                if (loginmode == true)
                {
                    _connect.Abort();

                    _state = false;
                    update_login_button_string();
                    btn_login.Text = "Login";
                    stl_connection.Text = "Last Operation Cancelled";
                    //abort running process
                }
                else
                {

                }
            }
        }

        private void update_login_button_string()
        {
            _login_prev_string = btn_login.Text;
        }

        private void IMAPLogin()
        {
            Boolean flag=ic.Login();
            if (flag == true)
            {
                awake();
            }
            else
            {
                error("Login Failed");
            }
        }
        private void POPLogin()
        {
            Boolean flag = pc.Login();
            if (flag == true)
            {
                awake();
            }
            else
            {
                error("Login Failed");
            }
        }
        private void IMAPLogout()
        {
            Boolean flag = ic.Logout();
            if (flag == true)
            {
                awake(true);
            }
            else
            {
                error("Logout error");
            }
        }
        private void POPLogout()
        {
            Boolean flag = pc.Logout();
            if (flag == true)
            {
                awake(true);
            }
            else
            {
                error("Logout error");
            }
        }

      
        private void frmmain_Load(object sender, EventArgs e)
        {

            load_account_data();
            cmb_account.SelectedIndex = 0;
            cmb_date_search.SelectedIndex = 0;
            cmb_file_action.SelectedIndex = 0;
           
            loadconfiguration();
            tl_pg.Visible = false;
            dt_date.Value = DateTime.Now;
            
        }

        private void loadconfiguration()
        {
            MailConfiguration mc = new MailConfiguration(COMMONS_FILE_PATH);
            ConfigurationSettings cf = mc.ReadConfiguration();
            if (cf != null)
            {
                txt_path.Text = cf.DownloadLocation;
                if (cf.MessageCount == -1)
                {
                    rd_all.Checked = true;
                    txt_nom.Text = "1";
                }
                else
                {
                    rd_no.Checked = true;
                    txt_nom.Text = cf.MessageCount.ToString();
                }
                if (cf.FileExistAction.ToString().Equals("Replace"))
                {
                    cmb_file_action.SelectedIndex = 1;
                }
                else
                {
                    
                        cmb_file_action.SelectedIndex = 0;
                    
                }
            }
        }
        private void validatedigit(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }
        private void temp()
        {
            this.Invoke((MethodInvoker)delegate
            {
                string text = "";
                foreach (string s in searchlist)
                {
                    text += s + Environment.NewLine;
                }
                Clipboard.SetText(text);
            });
        }
        private void awake(Boolean logout=false)
        {
            this.Invoke((MethodInvoker)delegate
           {

               tl_pg.Visible = false ;
               if (logout == false)
               {
                   islogin = true;
                   _state = false;
                   this.Enabled = true;
                   tl_pg.Visible = false;
                   btn_login.Text = "Logout";
                   _state = false;//no thread operation pending
                   stl_connection.Text = "Connected To :" + i_acc[cmb_account.Text].Server.HostName;
                   btn_down_all.Enabled = false;
                   lv_info.Items.Insert(0, "Connected To :" + i_acc[cmb_account.Text].Server.HostName);
                   txt_username.ReadOnly = true;
                   txt_password.ReadOnly = true;
                   cmb_account.Enabled = false;
                   btn_search.Enabled = true;
                   btn_set_last.Enabled = true;
                   _downloaded_items = 0;
                   Thread t = new Thread(getstatus);
                   stl_connection.Text = "Retrieving Message Information";
                   t.Start();
               }
               else
               {
                   islogin = false;
                   _state = false;
                   this.Enabled = true;
                   tl_pg.Visible = false;
                   btn_login.Text = "Login";
                   _state = false;//no thread operation pending
                   stl_connection.Text = "Disconnected";
                   txt_username.ReadOnly = false;
                   txt_password.ReadOnly = false;
                   btn_down_all.Enabled = false;
                   cmb_account.Enabled = true;
                   _last_id = 0;
                   _isbroken = false;
                   btn_set_last.Enabled = false;
                   lv_info.Items.Clear();
                   btn_search.Enabled = false;
                   pg_download_comp.Value = 0;
                   pg_download_overall.Value = 0;
               }
           });
        }

        private void getstatus()
        {
            
            this.Invoke((MethodInvoker)delegate{
                btn_search.Enabled = false;
            });
            List<string> lst = new List<string>();
            Dictionary<long, long> dict = new Dictionary<long, long>();
            if (mailmode == MailAccountType.IMAP)
            {
                ic.SelectInbox();
                //lst = ic.Search("HEADER Content-Type multipart/mixed UNSEEN SENTON " + DateTime.Now.ToString("dd-MMM-yyyy"));
                lst = ic.Search("HEADER Content-Type multipart/mixed UNSEEN");
            }
            else
            {
                dict = pc.GetList(long.Parse(txt_mail_no.Text));
            }
             this.Invoke((MethodInvoker)delegate
           {
            msglength = 0L;
            msgcount = 0L;
            stl_connection.Text = "Connected To :" + i_acc[cmb_account.Text].Server.HostName;
            lv_info.Items.Insert(0,"Retrieving Message Metadata");
            if (mailmode == MailAccountType.IMAP)
            {
               
                msgcount = lst.Count;
                msglength = ic.GetMessageSizes(lst);
                lv_info.Items.Insert(0,lst.Count + " Messages Exists (" + StringUtils.GetAbb(msglength) + " Total)");
                if (msgcount > 0)
                {
                    btn_down_all.Enabled = true;
                    searchlist = lst;
                }
            }
            else
            {
                //POP
              
              
                searchlist.Clear();
                Dictionary<long, long>.Enumerator ie = dict.GetEnumerator();
                while (ie.MoveNext())
                {
                    msglength += ie.Current.Value;
                  
                    searchlist.Add(ie.Current.Key.ToString());
                }
                msgcount = dict.Count;
                lv_info.Items.Insert(0,msgcount.ToString()+" Messages Exists ("+StringUtils.GetAbb(msglength)+" Total)");
                if (msgcount > 0)
                {
                    btn_down_all.Enabled = true;
                    
                }
            }
           
           
                stl_connection.Text = "Connected To :" + i_acc[cmb_account.Text].Server.HostName;
                lv_info.Items.Insert(0,"Message Metadata Retrieval Completed Successfully");
                btn_search.Enabled = true;
            });
           
        }
        private void getmessagelist()
        {
            if (mailmode == MailAccountType.POP3)
            {
                Dictionary<long, long> dict = pc.GetList(long.Parse(txt_mail_no.Text));
                msgcount = dict.Count;
                this.Invoke((MethodInvoker)delegate
                {
                    searchlist.Clear();
                    Dictionary<long, long>.Enumerator ie = dict.GetEnumerator();
                    while (ie.MoveNext())
                    {
                        msglength += ie.Current.Value;
                        searchlist.Add(ie.Current.Key.ToString());
                    }
                    lv_info.Items.Insert(0, "Preparing List of messages to be downloaded (" + StringUtils.GetAbb(msglength) + " Total)");
                    if (msgcount > 0)
                    {
                        btn_down_all.Enabled = true;

                    }
                });
            }
        }
        private void error(string errorText)
        {
            this.Invoke((MethodInvoker)delegate
            {
                tl_pg.Visible = false;
                islogin = false;
                btn_login.Text = "Login";
                txt_username.ReadOnly = false;
                txt_password.ReadOnly = false;
                tl_pg.Visible = false;
                btn_down_all.Enabled = false;
                _last_id = 0;
                _isbroken = false;
                _state = false;
                stl_connection.Text = errorText+" "+  i_acc[cmb_account.Text].Server.HostName;
            });
        }

        private void ch_from_CheckedChanged(object sender, EventArgs e)
        {
           
        }
        public  void load_account_data()
        {
            cmb_account.Items.Clear();
            i_acc.Clear();
            MailConfiguration mc = new MailConfiguration(System.Windows.Forms.Application.StartupPath + "\\app.ini");
            List<MailAccount> m = mc.ReadFile();
            foreach (MailAccount ma in m)
            {
                i_acc.Add(ma.Name, ma);
                cmb_account.Items.Add(ma);
            }
            cmb_account.Items.Add("<Edit...>");
            if (cmb_account.Items.Count > 1)
            {
                cmb_account.SelectedIndex = 0;
            }
           
        }
        private void ch_date_CheckedChanged(object sender, EventArgs e)
        {
          
        }

        private void cmb_account_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmb_account.Text.Equals("<Edit...>"))
            {
                EMAIL_Account ea = new EMAIL_Account();
                ea.Tag = this;
                ea.ShowDialog();
            }
            else
            {
                IMAPClient.MailConfig = (MailAccount)cmb_account.SelectedItem;
                hostname = i_acc[cmb_account.Text].Server.HostName;
                portno = i_acc[cmb_account.Text].Server.Port;
                if (i_acc[cmb_account.Text].Type == MailAccountType.IMAP)
                {
                    if (!tb_download.TabPages.Contains(tb_search))
                    {
                        tb_download.TabPages.Insert(0, tb_search);
                        tb_download.SelectedIndex = 0;
                        mailmode = MailAccountType.IMAP;
                    }
                    if (!tb_download.TabPages.Contains(tb_search_results))
                    {
                        tb_download.TabPages.Insert(2, tb_search_results);
                    }
                  
                        tb_download.TabPages.Remove(tb_pop_options);
                    
                }
                else
                {
                    tb_download.TabPages.Remove(tb_search);
                    tb_download.TabPages.Remove(tb_search_results);
                    if (!tb_download.TabPages.Contains(tb_pop_options))
                    {
                        tb_download.TabPages.Insert(1, tb_pop_options);
                    }
                    mailmode = MailAccountType.POP3;
                    loadpopdata();
                }

            }
        }

        private void loadpopdata()
        {
            MailConfiguration mc = new MailConfiguration(COMMONS_FILE_PATH);
            POP3Configuration pc = mc.ReadPOPConfiguration(System.Windows.Forms.Application.StartupPath + "\\" + cmb_account.Text);
            if (pc != null)
            {
                txt_mail_no.Text = pc.MessageID.ToString();
            }
            else
            {
                txt_mail_no.Text = "0";
            }
        }
     
        private void btn_get_Click(object sender, EventArgs e)
        {
           
        }

        private void getmails(string filter)
        {
           
         
       
        }

        private void op_recent_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void op_from_CheckedChanged(object sender, EventArgs e)
        {
           
        }

        private void op_sent_CheckedChanged(object sender, EventArgs e)
        {
          
       
        }

        private void rd_all_CheckedChanged(object sender, EventArgs e)
        {
            if (rd_all.Checked == true)
            {
                txt_nom.ReadOnly = true;
            }
        }

        private void rd_no_CheckedChanged(object sender, EventArgs e)
        {
            if (rd_no.Checked == true)
            {
                txt_nom.ReadOnly = false;
            }
        }

        private void btn_browse_Click(object sender, EventArgs e)
        {
            fbd1.ShowDialog(this);
            if (fbd1.SelectedPath.Length > 0)
            {
                txt_path.Text = fbd1.SelectedPath;
            }
        }

        private void ch_from_CheckedChanged_1(object sender, EventArgs e)
        {
            txt_from_search.Enabled = ch_from.Checked;
          
        }

        private void ch_subject_CheckedChanged(object sender, EventArgs e)
        {
            txt_subject_search.Enabled = ch_subject.Checked;
          
        }

        private void ch_date_search_CheckedChanged(object sender, EventArgs e)
        {
            dt_date.Enabled = ch_date_search.Checked;
            cmb_date_search.Enabled = dt_date.Enabled;
         
        }

        private void ch_body_search_CheckedChanged(object sender, EventArgs e)
        {
            txt_body_search.Enabled = ch_body_search.Checked;
         
        }
        private string getFilterstring()
        {
            string filterstring = "";
            this.Invoke((MethodInvoker)delegate
            {
             
                if (op_all.Checked == true)
                {
                    filterstring = "ALL";
                }
                if (op_new.Checked == true)
                {
                    filterstring = "NEW";
                }
                if (op_recent.Checked == true)
                {
                    filterstring = "RECENT";
                }
                if (op_unseen.Checked == true)
                {
                    filterstring = "UNSEEN";
                }
                if (op_seen.Checked == true)
                {
                    filterstring = "SEEN";
                }
                if (ch_from.Checked == true)
                {
                    filterstring += " FROM " + txt_from_search.Text;
                }
                if (ch_subject.Checked == true)
                {
                    filterstring += " SUBJECT " + txt_subject_search.Text;
                }
                if (ch_body_search.Checked == true)
                {
                    filterstring += " BODY " + txt_body_search.Text;
                }
                if (ch_text.Checked == true)
                {
                    filterstring += " TEXT " + txt_text.Text;
                }
                if (ch_date_search.Checked == true)
                {
                    filterstring += " " + cmb_date_search.Text + " " + dt_date.Value.ToString("dd-MMM-yyyy");
                }
                filterstring += " HEADER Content-Type multipart/mixed";
            });
            return filterstring;
        }

        private void btn_search_Click(object sender, EventArgs e)
        {
            
                string filter = getFilterstring();
                if (mailmode == MailAccountType.IMAP)
                {
                    if (_state == false)
                    {
                        _connect = new Thread(search);
                       lv_info.Items.Insert(0,"Searching for mails having "+filter);
                        
                   //  li.EnsureVisible();
                        btn_down_all.Enabled = false;
                        _state = true;
                        btn_search.Text = "Cancel";
                        _connect.Start();
                    }
                    else
                    {
                        _connect.Abort();
                        _state = false;
                        btn_search.Text = "Search";
                        btn_down_all.Enabled = true;
                        lv_info.Items.Insert(0,"Operation Cancelled by User");
                     //   lv_info.Items[lv_info.Items.Count - 1].EnsureVisible();
                     
                    }

                }
           
           
        }
        private void search()
        {
            List<string> lst = ic.Search(getFilterstring());
            this.Invoke((MethodInvoker) delegate{
            searchlist.Clear();

            searchlist = lst;
            lv_info.Items.Insert(0,searchlist.Count.ToString() + " Items found in search");
            _state = false;
            if (searchlist.Count > 0)
            {
                btn_down_all.Enabled = true;
                btn_get.Enabled = true;
            }
            btn_search.Text = "Search";
            _downloaded_items = 0;
            });
        }

        private void btn_down_all_Click(object sender, EventArgs e)
        {
            if (_state == false)
            {
                _state = true;
                btn_down_all.Text = "Cancel Download";
                _connect = new Thread(DownloadItems1);
                _connect.Start();
            }
            else
            {
                _state = false;
                _connect.Abort();
                btn_down_all.Text = "Download";
            }
        }

        private void DownloadItems()
        {
            if (searchlist.Count > 0)
            {
               // temp();
              
                try
                {
                    BackgroundWorker bg = new BackgroundWorker();
                    if (mailmode == MailAccountType.IMAP)
                    {
                        bg.DoWork -= new DoWorkEventHandler(ic.GetMessageAttachmentsAync);
                        bg.DoWork += new DoWorkEventHandler(ic.GetMessageAttachmentsAync);
                    }
                    else
                    {
                        bg.DoWork -= new DoWorkEventHandler(pc.GetEmailAsync);
                        bg.DoWork += new DoWorkEventHandler(pc.GetEmailAsync);
                    }
                    this.Invoke((MethodInvoker)delegate
                    {
                        _last_id = 0;
                        string _str = "";
                        foreach (string s in searchlist)
                        {
                            _str += s + " ";
                        }
                        lv_info.Items.Insert(0, _str);
                    });
                    bg.ProgressChanged += new ProgressChangedEventHandler(DownloadProgress);
                    bg.RunWorkerCompleted += new RunWorkerCompletedEventHandler(DownloadCompleted);
                    List<string> l = new List<string>();
                    bg.WorkerReportsProgress = true;
                    bg.WorkerSupportsCancellation = true;
                  //  for(int i=_last_id;i<searchlist.Count;)
                  while(_last_id<searchlist.Count)
                    {

                        this.Invoke((MethodInvoker)delegate
                        {
                           
                            lv_info.Items.Insert(0, "Attempting Download for message@ "+_last_id.ToString()+""+searchlist[_last_id]);
                        });
                        if (rd_all.Checked == false)
                        {
                            if (_downloaded_items == Int32.Parse(txt_nom.Text))
                            {
                                break;
                            }
                            else
                            {
                                _last_operation_completed = false;
                                bg.RunWorkerAsync(searchlist[_last_id]);
                                while (bg.IsBusy)
                                {
                                    Thread.Sleep(1000);
                                    if (_last_operation_completed == true)
                                    {
                                        break;
                                    }
                                }
                                if (_isbroken == true)
                                {
                                    
                                    continue;
                                  //  DownloadItems();
                                   
                                    
                                }
                                else
                                {
                                    ++_last_id;
                                    continue;
                                }
                            }
                        }
                        else
                        {
                            //  MessageBox.Show(s);
                            _last_operation_completed = false;
                            bg.RunWorkerAsync(searchlist[_last_id]);
                            while (bg.IsBusy)
                            {
                                Thread.Sleep(1000);
                                if (_last_operation_completed == true)
                                {
                                    break;
                                }
                            }
                            if (_isbroken == true)
                            {
                                
                                continue;
                                //  DownloadItems();


                            }
                            else
                            {
                                ++_last_id;
                                continue;
                                // _last_id = i;
                            }
                        }
                    }
                    //  bg.RunWorkerAsync("11931");
                    this.Invoke((MethodInvoker)delegate
                    {
                        pg_download_comp.Value = 0;
                        pg_download_overall.Value = 0;
                        btn_down_all.Enabled = false;
                        btn_cancel.Enabled = false;
                        _downloaded_items = 0;
                        _last_id = 0;
                        _isbroken = false;
                        nf.ShowBalloonTip(1000, "Mail", "All Attachments Finished Downloading", ToolTipIcon.Info);
                        lv_info.Items.Insert(0, "All Attachments Finished Downloading");
                    });
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message + Environment.NewLine + e.StackTrace);
                }
            }
      
        }
        private void DownloadItems1()
        {
            if (searchlist.Count > 0)
            {
                // temp();

                try
                {
                    BackgroundWorker bg = new BackgroundWorker();
                    if (mailmode == MailAccountType.IMAP)
                    {
                        bg.DoWork -= new DoWorkEventHandler(ic.GetMessageAttachmentsAync);
                        
                        bg.DoWork += new DoWorkEventHandler(ic.GetMessageAttachmentsAync);
                        bg.RunWorkerCompleted -= new RunWorkerCompletedEventHandler(IMAPDownloadCompleted);
                        bg.RunWorkerCompleted += new RunWorkerCompletedEventHandler(IMAPDownloadCompleted);
                        
                    }
                    else
                    {
                        bg.DoWork -= new DoWorkEventHandler(pc.GetEmailAsync);
                        bg.DoWork += new DoWorkEventHandler(pc.GetEmailAsync);
                        bg.RunWorkerCompleted -= new RunWorkerCompletedEventHandler(DownloadCompleted);
                        bg.RunWorkerCompleted += new RunWorkerCompletedEventHandler(DownloadCompleted);
                        
                    }
                    this.Invoke((MethodInvoker)delegate
                    {
                        _last_id = 0;
                        string _str = "";
                        foreach (string s in searchlist)
                        {
                            _str += s + " ";
                        }
                        lv_info.Items.Insert(0, _str);
                    });
                    bg.ProgressChanged += new ProgressChangedEventHandler(DownloadProgress);
                  
                    List<string> l = new List<string>();
                    bg.WorkerReportsProgress = true;
                    bg.WorkerSupportsCancellation = true;
                    //  for(int i=_last_id;i<searchlist.Count;)
                    while (_last_id < searchlist.Count)
                    {

                        this.Invoke((MethodInvoker)delegate
                        {

                            lv_info.Items.Insert(0, "Attempting Download for message@ " + _last_id.ToString() + "" + searchlist[_last_id]);
                        });
                        if (rd_all.Checked == false)
                        {
                            if (_downloaded_items == Int32.Parse(txt_nom.Text))
                            {
                                break;
                            }
                            else
                            {
                                _last_operation_completed = false;
                                bg.RunWorkerAsync(searchlist[_last_id]);
                                while (bg.IsBusy)
                                {
                                    Thread.Sleep(1000);
                                    if (_last_operation_completed == true)
                                    {
                                        break;
                                    }
                                }
                                if (_isbroken == true)
                                {

                                    continue;
                                    //  DownloadItems();


                                }
                                else
                                {
                                    ++_last_id;
                                    continue;
                                }
                            }
                        }
                        else
                        {
                            //  MessageBox.Show(s);
                            _last_operation_completed = false;
                            bg.RunWorkerAsync(searchlist[_last_id]);
                            while (bg.IsBusy)
                            {
                                Thread.Sleep(1000);
                                if (_last_operation_completed == true)
                                {
                                    break;
                                }
                            }
                            if (_isbroken == true)
                            {

                                continue;
                                //  DownloadItems();


                            }
                            else
                            {
                                ++_last_id;
                                continue;
                                // _last_id = i;
                            }
                        }
                    }
                    //  bg.RunWorkerAsync("11931");
                    this.Invoke((MethodInvoker)delegate
                    {
                        pg_download_comp.Value = 0;
                        pg_download_overall.Value = 0;
                        btn_down_all.Enabled = false;
                        btn_cancel.Enabled = false;
                        _downloaded_items = 0;
                        _last_id = 0;
                        _isbroken = false;
                        nf.ShowBalloonTip(1000, "Mail", "All Attachments Finished Downloading", ToolTipIcon.Info);
                        lv_info.Items.Insert(0, "All Attachments Finished Downloading");
                    });
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message + Environment.NewLine + e.StackTrace);
                }
            }

        }
        private void DownloadProgress(object sender,ProgressChangedEventArgs e)
        {
            this.Invoke((MethodInvoker)delegate
            {   
                pg_download_comp.Value = e.ProgressPercentage;
            });
        }
        private void DownloadCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled == false)
            {
                List<EmailAttachment> eac = null;
                
                EmailMessage em = e.Result as EmailMessage;
                eac = em.Attachments.ToList();
                if (em != null)
                {
                    _isbroken = false;
                    foreach (EmailAttachment eml in eac)
                    {
                        string fname = eml.FileName;
                        fname = fname.Replace("\\", "");
                        eml.SaveFile(txt_path.Text + "\\" + eml.FileName);
                    }
                    this.Invoke((MethodInvoker)delegate
                    {
                        if (_last_id < searchlist.Count)
                        {
                            lv_info.Items.Insert(0, searchlist[_last_id] + " Finished Downloading");
                            ++_downloaded_items;
                            int k = (_downloaded_items * 100) / searchlist.Count;
                            if (k <= 100)
                            {
                                pg_download_overall.Value = k;
                            }
                        }
                      //  ++_last_id;
                    });
                }
            }
            else
            {
                if (e.Cancelled == true)
                {
                    this.Invoke((MethodInvoker)delegate
                    {
                        lv_info.Items.Insert(0, "Network Error: Retrying Download");
                    });
                 
                    
                    _isbroken = true;
                }
            }
            _last_operation_completed = true;
        }
        private void IMAPDownloadCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled == false)
            {
                List<EmailAttachment> eac = null;

                eac = e.Result as List<EmailAttachment>;
           
                if (eac != null)
                {
                    _isbroken = false;
                    foreach (EmailAttachment eml in eac)
                    {
                        string fname = eml.FileName;
                        fname = fname.Replace("\\", "");
                        eml.SaveFile(txt_path.Text + "\\" + eml.FileName);
                    }
                    this.Invoke((MethodInvoker)delegate
                    {
                        if (_last_id < searchlist.Count)
                        {
                            lv_info.Items.Insert(0, searchlist[_last_id] + " Finished Downloading");
                            ++_downloaded_items;
                            int k = (_downloaded_items * 100) / searchlist.Count;
                            if (k <= 100)
                            {
                                pg_download_overall.Value = k;
                            }
                        }
                        //  ++_last_id;
                    });
                }
            }
            else
            {
                if (e.Cancelled == true)
                {
                    this.Invoke((MethodInvoker)delegate
                    {
                        lv_info.Items.Insert(0, "Network Error: Retrying Download");
                    });


                    _isbroken = true;
                }
            }
            _last_operation_completed = true;
        }
        private void frmmain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_state == false)
            {
                if (islogin == true)
                {
                    if (mailmode == MailAccountType.IMAP)
                    {
                        ic.Logout();
                        ic.Dispose();
                    }
                    else
                    {
                        pc.Logout();
                        pc.Dispose();
                    }
                }
            }
            else
            {

                if (MessageBox.Show("A Network Operation is in Progress.Click No to Quit any Operation (Login,Logout,Search,Download etc)  and Retry or click Yes Abort current Operation and Quit", "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes)
                {
                    if (_connect != null)
                    {
                        _connect.Abort();
                    }
                }
                else
                {
                    e.Cancel = true;
                }
            }
            
        }

        private void btn_get_Click_1(object sender, EventArgs e)
        {
            if (_state == false)
            {
                _connect = new Thread(GetDetails);
                lv_info.Items.Insert(0,"Retrieving Mail info from Server");
                btn_get.Enabled = false;
                _connect.Start();
                _state = true;
            }
            else
            {
                _connect.Abort();
                btn_get.Enabled = false;
                _state = false;
            }
        }
        private void GetDetails()
        {
            List<MailHeader> lst = ic.GetAllMailHeaders(searchlist);
            this.Invoke((MethodInvoker)delegate
            {
                if (searchlist != null)
                {

                    lv_results.Items.Clear();
                    int i = 0;
                    foreach (MailHeader mh in lst)
                    {
                        ListViewItem li = new ListViewItem(mh.Date);
                        li.Tag = searchlist[i++];
                        li.SubItems.Add(mh.Subject);
                        if (mh.From.Count > 0)
                        {
                            li.SubItems.Add(mh.To[0].Email);
                        }
                        lv_results.Items.Add(li);
                        _state = false;

                    }
                    lv_info.Items.Insert(0, "Operation Completed Successfully Please Navigate to Search Results Pane");
                }
            });
        }

        private void btn_select_all_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem li in lv_results.Items)
            {
                li.Checked = true;
            }
        }

        private void btn_deselect_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem li in lv_results.Items)
            {
                li.Checked = false;
            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }

        private void btn_set_last_Click(object sender, EventArgs e)
        {
            if (mailmode == MailAccountType.POP3)
            {
                _connect = new Thread(getlast);
                txt_mail_no.Enabled = false;
                _connect.Start();

            }
        }
        private void getlast()
        {
            long l = 0l;
            Dictionary<long, long> ls = pc.GetStat();
            Dictionary<long, long>.Enumerator ie = ls.GetEnumerator();
            while (ie.MoveNext())
            {
                l = ie.Current.Key;
            }

            this.Invoke((MethodInvoker)delegate {
                txt_mail_no.Text = l.ToString();
                txt_mail_no.Enabled = true;
            });
        }

        private void btn_save_config_Click(object sender, EventArgs e)
        {
            MailConfiguration MC = new MailConfiguration(COMMONS_FILE_PATH);
            ConfigurationSettings cf = new ConfigurationSettings(txt_path.Text, (rd_all.Checked==true?-1: Int32.Parse(txt_nom.Text)), cmb_file_action.SelectedIndex == 0 ? ConfigurationSettings.FileAction.Rename : ConfigurationSettings.FileAction.Replace);
            MC.WriteConfiguration(cf);
            

        }

        private void btn_set_pop_user_Click(object sender, EventArgs e)
        {
            MailConfiguration mc = new MailConfiguration(cmb_account.Text+".POP");
            POP3Configuration pc = new POP3Configuration(cmb_account.Text,long.Parse(txt_mail_no.Text));
            mc.WritePOPConfiguration(cmb_account.Text + ".POP", pc);
        }

        private void ch_text_CheckedChanged(object sender, EventArgs e)
        {
            txt_text.Enabled = ch_text.Checked;
        }

        private void button1_Click(object sender, EventArgs e)
        {
          
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {

        }
        
    }
}

