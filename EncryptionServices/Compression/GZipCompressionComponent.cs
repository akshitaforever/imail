﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpCompress;
using System.IO;
using System.IO.Compression;
namespace EncryptionServices.Compression
{
    public class GZipCompressionComponent<T> : ICompressionComponent<T> where T : class
    {
        public byte[] Compress(T obj)
        {
            byte[] objectdata = StreamUtilities.ObjectToByteArray<T>(obj);
            using (MemoryStream memory = new MemoryStream())
            {
                using (GZipStream gzip = new GZipStream(memory, CompressionMode.Compress, false))
                {
                    gzip.Write(objectdata, 0, objectdata.Length);
                }
                return memory.ToArray();
            }

        }

        public T DeCompress(byte[] rawdata)
        {
            using (MemoryStream memory = new MemoryStream())
            {
                using (GZipStream gzip = new GZipStream(memory, CompressionMode.Decompress, false))
                {
                    gzip.Write(rawdata, 0, rawdata.Length);
                }
                return StreamUtilities.GetObject<T>(memory.ToArray());
            }
        }
    }
}
