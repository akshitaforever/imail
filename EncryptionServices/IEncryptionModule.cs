﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace EncryptionServices
{
    public interface IEncryptionModule<T> where T : class
    {
        Byte[] Encrypt(Byte[] data);

        Byte[] Encrypt(T data);
        T Decrypt(Byte[] data);
       
    }
}
