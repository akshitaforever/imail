﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace EncryptionServices
{
    public class AesEncryptionComponent<T> : AbstractEncryptionComponent<T> where T : class
    {
        private Aes _instance;
        private const int BLOCK_SIZE = 16;//16 Bytes
        public AesEncryptionComponent(byte[] key, byte[] IV)
        {
            _instance = Aes.Create();
            _instance.IV = IV;
            _instance.Key = key;
        }
        public AesEncryptionComponent(string algorithmName, byte[] key, byte[] IV)
        {
            _instance = Aes.Create(algorithmName);
            _instance.IV = IV;
            _instance.Key = key;
            
        }


        public override ICryptoTransform GetEncryptor()
        {
            return _instance.CreateEncryptor();
            
        }

        public override ICryptoTransform GetDecryptor()
        {
            return _instance.CreateDecryptor();
        }



        public override byte[] GenerateKey()
        {
             _instance.GenerateKey();
             return _instance.Key;
        }

        public override byte[] GenerateIV()
        {
            _instance.GenerateIV();
            return _instance.IV;
        }
    }
}
