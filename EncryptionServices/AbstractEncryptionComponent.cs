﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace EncryptionServices
{
    public abstract class AbstractEncryptionComponent<T> : IEncryptionModule<T> where T : class
    {
        public Encoding @Encoding { get; set; }
        protected SortedList<int, Object> _param = new SortedList<int, object>();
        public void AddArguments(params Object[] param)
        {
            foreach (var item in param)
            {
                _param.Add(_param.Count + 1, item);
            }
        }
        public virtual byte[] Encrypt(byte[] data)
        {
            ICryptoTransform encryptor = GetEncryptor();

            // Create the streams used for encryption. 
            using (MemoryStream msEncrypt = new MemoryStream())
            {
                using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                {
                    using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                    {

                        //Write all data to the stream.
                        swEncrypt.Write(data);
                    }
                    return msEncrypt.ToArray();
                }
            }

        }
        public virtual byte[] Encrypt(T data)
        {
            ICryptoTransform encryptor = GetEncryptor();

            // Create the streams used for encryption. 
            using (MemoryStream msEncrypt = new MemoryStream())
            {
                using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                {
                    using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                    {

                        //Write all data to the stream.
                        swEncrypt.Write(data);
                    }
                    return msEncrypt.ToArray();
                }
            }
        }

        public virtual T Decrypt(byte[] data)
        {
            try
            {
                ICryptoTransform decryptor = GetDecryptor();

                bool error = false;

                using (MemoryStream msDecrypt = new MemoryStream(data))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt
    , decryptor, CryptoStreamMode.Read))
                    {
                        using (BinaryReader srDecrypt = new BinaryReader(csDecrypt))
                        {


                            //  List<Byte> dest = new List<Byte>();
                            //  long bytestoRead = BLOCK_SIZE;
                            //  int index
                            Byte[] buffer = new Byte[srDecrypt.BaseStream.Length];
                            //while (bytestoRead>0)
                            //{
                            //    if (Math.Abs(srDecrypt.BaseStream.Length - bytestoRead) <= 16)
                            //    {
                            //        bytestoRead = Math.Abs(srDecrypt.BaseStream.Length - bytestoRead);
                            //    }
                            srDecrypt.Read(buffer, 0, (int)srDecrypt.BaseStream.Length);

                            return StreamUtilities.GetObject<T>(buffer);
                            // }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                return null;
            }
            return null;
        }
        public virtual byte[] DecryptRaw(byte[] data)
        {
            try
            {
                ICryptoTransform decryptor = GetDecryptor();
                bool error = false;

                using (MemoryStream msDecrypt = new MemoryStream(data))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt
    , decryptor, CryptoStreamMode.Read))
                    {
                        using (BinaryReader srDecrypt = new BinaryReader(csDecrypt))
                        {


                            //  List<Byte> dest = new List<Byte>();
                            //  long bytestoRead = BLOCK_SIZE;
                            //  int index
                            Byte[] buffer = new Byte[srDecrypt.BaseStream.Length];
                            //while (bytestoRead>0)
                            //{
                            //    if (Math.Abs(srDecrypt.BaseStream.Length - bytestoRead) <= 16)
                            //    {
                            //        bytestoRead = Math.Abs(srDecrypt.BaseStream.Length - bytestoRead);
                            //    }
                            srDecrypt.Read(buffer, 0, (int)srDecrypt.BaseStream.Length);

                            return buffer;
                            // }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                return null;
            }
            return null;
        }
        public abstract ICryptoTransform GetEncryptor();
        public abstract ICryptoTransform GetDecryptor();
        public abstract byte[] GenerateKey();
        public abstract byte[] GenerateIV();
    }
}
