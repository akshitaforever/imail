﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DB
{
   
    public interface IContext<T> where T : BaseEntity
    {
        bool CreateDB(string connstring);
        T Add(T item);
        T Update(T item);
        bool Delete(T item);
        T GetByID(int ID);
        IEnumerable<T> Get(Expression<Func<T, bool>> expression, Expression<Func<T, string>> orderby = null);
        IEnumerable<T> GetPaged(Expression<Func<T, string>> orderby, Expression<Func<T, bool>> expression = null, int page = 1, int itemcount = 10);
        T GetSingle(Expression<Func<T, bool>> expression);
    }
}
