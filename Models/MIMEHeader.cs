﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMAIL.Models
{
    public class MIMEHeader:BaseEntity
    {
        
       
        //private Encoding _enc;
        public string FileName
        {
            get;
           private set;
        }
        public long Size
        {
            get;
            private set;
        }
        //public Encoding Charset
        //{
        //    get;
        //    private set;
        //}
        public string ContentType
        {
            get;
            private set;
        }
        public string Encoding
        {
            get;
            private set;
        }

        public string Content_Disposition
        {
            get;
            private set;
        }
        public string AttachmentID
        {
            get;
            private set;
        }
        public MIMEHeader(string content,long length=0L)
        {
            Size = length;
            string[] str = content.Split(new string[]{"\r\n"},StringSplitOptions.RemoveEmptyEntries);
            List<string> lstitems = new List<string>();
            for (int i = 0; i < str.Length; i++)
            {
                string substring = "";
                string _str = "";
                if (str[i].StartsWith("Content-Disposition"))
                {
                    
                    int index = str[i].IndexOf(';');
                    if (index > 0)
                    {
                        substring = str[i].Substring(index + 1);
                        _str = str[i].Substring(0, index);
                    }
                }
                else
                {
                    _str = str[i];
                }
                if (_str.Trim().Length > 1)
                {
                    lstitems.Add(_str.Trim());
                 
                }
                if (substring.Trim().Length > 0)
                {
                   
                    lstitems.Add(substring.Trim());
                }
               
               
            }
            foreach (string s in lstitems)
            {
                if (s.StartsWith("Content-Type"))
                {
                    
                    string[] _sp = s.Split(':');
                    ContentType = s.Split(':')[1].Split(';')[0];
                    string len = _sp[1].Split(';')[1];
                    if (len.Length > 1)
                    {
                        
                        if (len.Trim().ToUpper().StartsWith("CHARSET"))
                        {
                            _sp=len.Split('=');
                            //_enc = System.Text.Encoding.GetEncoding(_sp[1].Trim());
                            Encoding =_sp[1].Trim();
                        }
                    }
                }
               
                if (s.StartsWith("filename="))
                {
                    
                    FileName = s.Split('=')[1].Replace('"', ' ').Trim();
                    int index = s.IndexOf('"');
                    int p_index = 0;
                    if (index >= 0)
                    {
                        p_index = index;
                        index = s.IndexOf('"',p_index+1);
                        if (index >= 0)
                        {
                            if (index > p_index)
                            {
                                FileName = s.Substring(p_index, index - p_index + 1);
                                FileName = FileName.Replace("\\", "");
                                FileName = FileName.Replace("/", "");
                                FileName = FileName.Replace("*", "");
                                FileName = FileName.Replace(":", "");
                                FileName = FileName.Replace('"', ' ');
                                FileName = FileName.Replace("?", "");
                                FileName = FileName.Replace(">", "");
                                FileName = FileName.Replace("<", "");
                                FileName = FileName.Trim();
                            }
                        }
                    }
                }
                if (s.StartsWith("Content-Transfer-Encoding"))
                {
                    Encoding = s.Split(':')[1].Trim().ToUpper();
                }
                if (s.StartsWith("X-Attachment-Id"))
                {
                    AttachmentID = s.Split(':')[1].Trim(); ;
                }
                if (s.StartsWith("Content-Disposition"))
                {
                    Content_Disposition = s.Split(':')[1].Trim().ToUpper();
                }
            }
        }
       
    }
}
