﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using System.Net.Mail;
using System.ComponentModel.DataAnnotations;
using Models;
namespace IMAIL.Models
{
    public class EmailMessage : BaseEntity
    {

        public virtual ICollection<EmailAttachment> Attachments { get; set; }

        public MailHeader Header { get; private set; }


        private string EOL = "\r\n";


        public bool IsRead { get; set; }
        public EmailMessage(MailHeader _hd, List<EmailAttachment> e, string bodycontent, string htmlcontent)
        {
            Attachments = e;
            this.Header = _hd;
            this.Body = bodycontent;
            this.HtmlBody = htmlcontent;
        }

        public string Body
        {
            get;
            private set;
        }
        public string HtmlBody
        {
            get;
            private set;
        }
        public string Date
        {
            get;
            private set;
        }
        public string Content
        {
            get;
            private set;
        }
        public List<EMailAddress> From
        {
            get;
            private set;
        }
        public List<EMailAddress> To
        {
            get;
            private set;
        }

        /*public EmailMessage(string content)
         {
             this._content = content;
             string[] lines = content.Split(new string[] { EOL }, StringSplitOptions.None);
             Boolean _isattachment = false;
             string ct = "";
             Boolean b = false;
             string _attchmentstring = "";
             string prev = "";
             List<EmailAttachment> lst = new List<EmailAttachment>();
             EmailMessage eml = null;
             List<MailAddress> _from = new List<MailAddress>();
             List<MailAddress> _to = new List<MailAddress>();
           
             string from="";
             Boolean htmlstart = false;
             Mime.MIMEHeader mc = null;
             for (int i = 0; i < lines.Length; i++)
             {

                 string line = lines[i];
                 line = line.Replace("\r\n", "");
                 if (_isattachment == false)
                 {

                     if (line.StartsWith("Content-Disposition:"))
                     {
                         string[] _sp = line.Split(':');
                         if (_sp[1].ToUpper().Trim().StartsWith("ATTACHMENT"))
                         {
                             ct = "";
                             b = true;

                             ct += line + EOL;

                             if (prev.StartsWith("Content-Type:"))
                             {
                                 ct += prev + EOL;
                             }
                         }
                       
                     }
                  
                     if (line.StartsWith("X-Attachment-Id:"))
                     {
                         if (b == true)
                         {
                             ct += line + EOL;
                         }
                     }
                     if (line.StartsWith("Content-Transfer-Encoding:"))
                     {
                        
                             ct += line + EOL;
                        

                     }
                     if (line.StartsWith("From"))
                     {
                         from += line+EOL;

                     }
                     if (line.StartsWith("To"))
                     {
                         from += line+EOL;

                     }
                     if (line.StartsWith("Date"))
                     {
                         from += line+EOL;

                     }
                     if (line.StartsWith("Subject"))
                     {
                         from += line+EOL;

                     }
                     if (line.StartsWith("Message-ID"))
                     {
                         from += line+EOL;

                     }

                     if (b == true && line.Length == 0)
                     {
                         _isattachment = true;
                         mc = new Mime.MIMEHeader(ct);
                     }
                 
                 }

                 else 
                 {
                     if (line.Trim().StartsWith("--"))
                     {

                         _isattachment = false;
                         _attchmentstring = _attchmentstring.Replace("(", "");
                         _attchmentstring = _attchmentstring.Replace(")", "");
                         EmailAttachment ec = new EmailAttachment(mc, _attchmentstring);
                         lst.Add(ec);
                         _attchmentstring = "";
                         b = false;




                     }
                     else
                     {

                         _attchmentstring += line;
                     }
                 }
               
                 prev = line.Trim();
             }
             this._hdr = new MailHeader(from);
             this._attch = lst;
          
           

           
         }
         * */
        public EmailMessage(string content)
        {
            try
            {
                this.Content = content;
                string[] lines = content.Split('\r');
                Boolean _isattachment = false;
                string ct = "";
                Boolean b = false;
                string _attchmentstring = "";
                string prev = "";
                List<EmailAttachment> lst = new List<EmailAttachment>();
                EmailMessage eml = null;
                List<MailAddress> _from = new List<MailAddress>();
                List<MailAddress> _to = new List<MailAddress>();

                string from = "";
                Boolean htmlstart = false;
                MIMEHeader mc = null;
                for (int i = 0; i < lines.Length; i++)
                {

                    string line = lines[i];
                    //  line = line.Replace("\r\n", "");

                    if (_isattachment == false)
                    {
                        line = line.Replace("\n", "");

                        if (line.StartsWith("Content-Disposition:"))
                        {
                            string[] _sp = line.Split(':');
                            if (_sp[1].ToUpper().Trim().StartsWith("ATTACHMENT"))
                            {
                                //  ct = "";
                                b = true;

                                ct += line + EOL;


                            }

                        }
                        if (line.StartsWith("Content-Type:"))
                        {
                            ct += line + EOL;
                        }
                        if (line.StartsWith("X-Attachment-Id:"))
                        {
                            if (b == true)
                            {
                                ct += line + EOL;
                            }
                        }
                        if (line.StartsWith("Content-Transfer-Encoding:"))
                        {

                            ct += line + EOL;


                        }
                        if (line.Trim().StartsWith("filename"))
                        {
                            if (b == true)
                            {
                                ct += line + EOL;
                            }
                        }
                        if (line.StartsWith("From"))
                        {
                            from += line + EOL;

                        }
                        if (line.StartsWith("To"))
                        {
                            from += line + EOL;

                        }
                        if (line.StartsWith("Date"))
                        {
                            from += line + EOL;

                        }
                        if (line.StartsWith("Subject"))
                        {
                            from += line + EOL;

                        }
                        if (line.StartsWith("Message-ID"))
                        {
                            from += line + EOL;

                        }

                        if (b == true && line.Length == 0)
                        {
                            _isattachment = true;
                            mc = new MIMEHeader(ct);
                        }

                    }

                    else
                    {
                        if (line.Trim().StartsWith("--"))
                        {

                            _isattachment = false;
                            _attchmentstring = _attchmentstring.Replace("(", "");
                            _attchmentstring = _attchmentstring.Replace(")", "");
                            EmailAttachment ec = new EmailAttachment(mc, _attchmentstring);
                            if (ec != null)
                            {
                                lst.Add(ec);
                            }
                            _attchmentstring = "";
                            b = false;
                            ct = "";



                        }
                        else
                        {
                            line = line.Replace("\n", "");
                            _attchmentstring += line;
                        }
                        //if (line.Length == 0)
                        //{
                        //    _isattachment = false;
                        //    _attchmentstring = _attchmentstring.Replace("(", "");
                        //    _attchmentstring = _attchmentstring.Replace(")", "");
                        //    EmailAttachment ec = new EmailAttachment(mc, _attchmentstring);
                        //    if (ec != null)
                        //    {
                        //        lst.Add(ec);
                        //    }
                        //    _attchmentstring = "";
                        //    b = false;
                        //    ct = "";
                        //}
                    }
                    if (line.StartsWith("--"))
                    {
                        ct = "";
                    }

                    prev = line.Trim();
                }
                this.Header = new MailHeader(from);
                this.Attachments = lst;



            }
            catch (Exception e)
            {
                // System.Windows.Forms.MessageBox.Show(e.Source + Environment.NewLine + e.StackTrace);
            }
        }
        private string DecodeQuotedPrintables(string input)
        {
            var occurences = new Regex(@"=[0-9A-Z]{2}", RegexOptions.Multiline);
            var matches = occurences.Matches(input);
            foreach (Match match in matches)
            {
                char hexChar = (char)Convert.ToInt32(match.Groups[0].Value.Substring(1), 16);
                input = input.Replace(match.Groups[0].Value, hexChar.ToString());
            }
            return input.Replace("=\r\n", "");
        }
    }
}
