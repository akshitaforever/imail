﻿using Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace IMAIL.Models
{
    public class SQLiteMailProfileHandler : IMailProfileHandler
    {
        public async Task<MailProfile> GetProfile(string profileid)
        {
            MailProfile mp = new MailProfile();

            return mp;
        }
    }
    public class BinaryProfileHandler : IMailProfileHandler
    {
        Task<MailProfile> IMailProfileHandler.GetProfile(string profileid)
        {
            throw new NotImplementedException();
        }
    }
    public interface IMailProfileHandler
    {
        Task<MailProfile> GetProfile(string profileid);

    }
    public class Folder:BaseEntity
    {
        public string DisplayName { get; set; }
        public long MailCount { get; set; }
        public ICollection<EmailMessage> Messages { get; set; }

    }
    public class MailProfile : BaseEntity
    {
       
        public string AccountID { get; set; }
        public virtual ICollection<Folder> Folders { get; set; }

    }


}
