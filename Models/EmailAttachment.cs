﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Models;
namespace IMAIL.Models
{
    public class EmailAttachment : BaseEntity
    {


        public string FileName { get;private set; }
        public string Encoding { get; private set; }
        public long Size { get; private set; }
        public MIMEHeader ContentType { get; private set; }
        public byte[] RawData { get; private set; }
        public string Text { get; private set; }


        private byte[] DecodeBase64StringToBinary(string content)
        {

            byte[] b = Convert.FromBase64String(content);
            return b;
        }
        public void SaveFile(String dest)
        {
            if (RawData.Length == 0)
            {
                throw new NullReferenceException("Attachment Data is Empty");
            }
            else
            {

                FileStream fs = new FileStream(dest, FileMode.Create);

                fs.Write(RawData, 0, RawData.Length);


                fs.Close();
            }
        }
        public EmailAttachment(MIMEHeader _content, string content)
        {
            this.Encoding = _content.Encoding;
            this.FileName = _content.FileName;
            this.ContentType = _content;
            if (this.Encoding.Equals("BASE64"))
            {
                this.RawData = DecodeBase64StringToBinary(content);
            }
            if (Encoding.ToUpper().Equals("8BIT"))
            {
                content = content.Replace("\n", "\r\n");
                RawData = System.Text.Encoding.UTF8.GetBytes(content);
            }
            Size = RawData.LongLength;
        }

    }
}
