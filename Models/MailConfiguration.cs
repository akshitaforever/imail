﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net.Mail;
using System.ComponentModel.DataAnnotations;
namespace IMAIL.Models
{
    public enum MailAccountType { IMAP, POP3 }
    public struct MailAccount
    {
        private string _accname;
        private MailShared.MailServerHostEntry _mail_serv;
        private Boolean _inuse;
        private MailAccountType _mailtype;
        private bool _ssl;
        public MailAccount(string name, MailShared.MailServerHostEntry mail, Boolean inuse, MailAccountType type, bool ssl = false)
        {
            this._accname = name;
            this._inuse = inuse;
            this._mail_serv = mail;
            this._mailtype = type;
            this._ssl = ssl;
        }
       
        public string Name
        {
            get { return _accname; }
        }
        public MailShared.MailServerHostEntry Server
        {
            get { return _mail_serv; }
        }
        public Boolean IsActive
        {
            get { return _inuse; }
        }
        public MailAccountType Type
        {
            get { return _mailtype; }
        }
        public bool SSL { get { return this._ssl; } }
        public override string ToString()
        {
            return _accname;
        }
    }
    public class MailConfiguration
    {
        private string _inputfile;
        public string Filename
        {
            get { return _inputfile; }
        }
        public MailConfiguration(string filename)
        {
            this._inputfile = filename;
        }
        /// <summary>
        /// Reads Configuration File 
        /// </summary>
        /// <param name="_filename">File Name to Read</param>
        /// <param name="_listActive">Specifies wheather to return only active items</param>
        /// <returns></returns>
        public List<MailAccount> ReadFile(Boolean _listActive = false)
        {
            string _host = "";
            int _port = 143;
            string _name = "";
            string _type = "";
            bool _ssl = false;
            List<MailAccount> lst_account = new List<MailAccount>();
            Boolean _inuse = false;
            MailAccount mc;
            if (!File.Exists(_inputfile))
            {
                //throw new FileNotFoundException("File Not Found");
            }
            else
            {
                StreamReader rdr = new StreamReader(_inputfile);
                string str = "";
                Boolean isbegin = false;
                while (!rdr.EndOfStream)
                {
                    str = rdr.ReadLine();
                    if (str.StartsWith("#BEGIN"))
                    {
                        isbegin = true;

                    }
                    else if (str.StartsWith("#END"))
                    {
                        isbegin = false;
                        MailAccountType a_type;
                        MailShared.MailServerHostEntry m = new MailShared.MailServerHostEntry(_host, _port);
                        MailAccount ml = new MailAccount(_name, m, _inuse, _type == "IMAP" ? MailAccountType.IMAP : MailAccountType.POP3, _ssl);

                        if (_type.Equals("IMAP"))
                        {
                            a_type = MailAccountType.IMAP;
                        }
                        else
                        {
                            a_type = MailAccountType.POP3;
                        }
                        if (_listActive == true)
                        {
                            if (_inuse == true)
                            {
                                mc = new MailAccount(_name, m, _inuse, a_type, _ssl);
                                lst_account.Add(mc);
                            }
                        }
                        else
                        {
                            mc = new MailAccount(_name, m, _inuse, a_type, _ssl);
                            lst_account.Add(mc);
                        }


                    }
                    else
                    {
                        if (isbegin == true)
                        {

                            if (str.IndexOf(':') >= 0)
                            {
                                string[] _sp = str.Split(':');
                                if (str.StartsWith("ACC_NAME"))
                                {
                                    _name = _sp[1];
                                }
                                if (str.StartsWith("ACC_SERVER"))
                                {
                                    _host = _sp[1];
                                }
                                if (str.StartsWith("ACC_PORT"))
                                {
                                    _port = Int32.Parse(_sp[1].Trim());
                                }
                                if (str.StartsWith("ACC_TYPE"))
                                {
                                    _type = _sp[1];
                                }
                                if (str.StartsWith("IN_USE"))
                                {
                                    _inuse = _sp[1].Equals("1") ? true : false;
                                }
                                if (str.StartsWith("SSL"))
                                {
                                    _ssl = _sp[1].Equals("1") ? true : false;
                                }

                            }
                        }
                    }
                }
                rdr.Close();
            }

            return lst_account;
        }
        public void WriteConfiguration(ConfigurationSettings cnf)
        {
            if (File.Exists(_inputfile))
            {
                File.Delete(_inputfile);
            }

            StreamWriter sw = new StreamWriter(_inputfile);
            sw.WriteLine("#Begin Mail Downloader Configuration File");
            {
                sw.WriteLine("APPLICATION_B967:2d9be1a4-a3d2-4c11-9c2d-584aedcc9ece");
                sw.WriteLine(ConfigurationSettings.LOCATION_GUID + ":" + cnf.DownloadLocation);
                sw.WriteLine(ConfigurationSettings.MSG_GUID + ":" + cnf.MessageCount.ToString());
                sw.WriteLine(ConfigurationSettings.FILE_ACT_GUID + ":" + (cnf.FileExistAction.ToString().Equals("Rename") ? "PS120" : "LS120"));
                sw.WriteLine("#End Mail Downloader Configuration File");
                sw.Close();
            }
        }
        public ConfigurationSettings ReadConfiguration()
        {


            StreamReader sr = new StreamReader(_inputfile);
            string str = "";
            string _loc = "";
            int MSG_COUNT = 0;
            ConfigurationSettings CF = null;
            ConfigurationSettings.FileAction _file_action = ConfigurationSettings.FileAction.Rename;
            if (File.Exists(_inputfile))
            {
                while (!sr.EndOfStream)
                {
                    str = sr.ReadLine();
                    if (!str.StartsWith("#"))
                    {
                        if (str.StartsWith("APPLICATION_B967:2d9be1a4-a3d2-4c11-9c2d-584aedcc9ece"))
                        {

                        }
                        if (str.StartsWith(ConfigurationSettings.LOCATION_GUID))
                        {
                            int index = str.IndexOf(':');
                            if (index >= 0)
                            {
                                _loc = str.Substring(index + 1);
                            }
                        }
                        if (str.StartsWith(ConfigurationSettings.MSG_GUID))
                        {
                            MSG_COUNT = Int32.Parse(str.Split(':')[1].Trim());
                        }
                        if (str.StartsWith(ConfigurationSettings.FILE_ACT_GUID))
                        {
                            _file_action = str.Split(':')[1].Trim().Equals("LS120") ? ConfigurationSettings.FileAction.Replace : ConfigurationSettings.FileAction.Rename;
                        }
                    }

                }
                sr.Close();
            }
            CF = new ConfigurationSettings(_loc, MSG_COUNT, _file_action);
            return CF;

        }
        public void WritePOPConfiguration(string _filename, POP3Configuration cnf)
        {
            if (File.Exists(_filename))
            {
                File.Delete(_filename);
            }

            StreamWriter sw = new StreamWriter(_filename);

            sw.WriteLine("#Begin Mail Downloader POP Configuration File");
            {
                sw.WriteLine("APPLICATION_B967:2d9be1a4-a3d2-4c11-9c2d-584aedcc9ece");
                sw.WriteLine("ACC_NO:" + cnf.Account);
                sw.WriteLine("LAST_MSG_ID:" + cnf.MessageID.ToString());

                sw.WriteLine("#End Mail Downloade rPOP Configuration File");
                sw.Close();
            }
        }
        public POP3Configuration ReadPOPConfiguration(string _filename)
        {
            POP3Configuration pc = null;
            string str = "";
            string _acc_name = "";
            long _last_fetch = 0l;
            if (File.Exists(_filename))
            {

                StreamReader sr = new StreamReader(_filename);


                while (!sr.EndOfStream)
                {
                    str = sr.ReadLine();
                    if (!str.StartsWith("#"))
                    {
                        if (str.StartsWith("ACC_NO"))
                        {
                            _acc_name = str.Split(':')[1].Trim();
                        }
                        if (str.StartsWith("LAST_MSG_ID"))
                        {
                            _last_fetch = long.Parse(str.Split(':')[1].Trim());
                        }
                    }
                }
                sr.Close();
            }
            pc = new POP3Configuration(_acc_name, _last_fetch);
            return pc;
        }

    }
    public class ConfigurationSettings
    {
        public const string LOCATION_GUID = "39454C80C8964AF7AC1B9B136F40D679";
        public const string FILE_ACT_GUID = "F121BEFACCB241E2B3EBF209F98EC231";
        public const string MSG_GUID = "84316D7CAE904B6E8A6CCF6093F5C23F";
        public const string HASH = "5AFC8537AAB4D32A0FA9CC71A1D7BA";
        public const string HKEY = "TBWS6KGNP67y4h";
        public enum FileAction { Replace, Rename }
        private int _no_of_messages;
        private string _location;
        private FileAction _fileaction;
        public string DownloadLocation
        {
            get { return _location; }
        }
        public long MessageCount
        {
            get { return _no_of_messages; }
        }
        public FileAction FileExistAction
        {
            get { return _fileaction; }
        }
        public ConfigurationSettings(string _location, int message_count, FileAction _action)
        {
            this._fileaction = _action;
            this._location = _location;
            this._no_of_messages = message_count;
        }
    }
    public class POP3Configuration
    {

        private long _last_fetch;
        private string _acc_name;

        public string Account
        {
            get { return _acc_name; }
        }
        public long MessageID
        {
            get { return _last_fetch; }
        }

        public POP3Configuration(string _acc_name, long _last_fetch)
        {

            this._acc_name = _acc_name;
            this._last_fetch = _last_fetch;
        }
    }
}
