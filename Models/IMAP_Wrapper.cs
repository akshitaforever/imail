﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMAIL.Models
{
    public class MailShared
    {
       
        public struct MailServerHostEntry
        {
            
            private string _hostname;
            private int _portno;
            public string HostName
            {
                get { return _hostname; }
             
            }
            public int Port
            {
                get { return _portno; }
             
            }
            public MailServerHostEntry(string hostname, int port)
            {
                this._hostname = hostname;
                this._portno = port;
            }
        }
        public enum MIMEEncoding {Base64,QuotedPrintable }
        public class MessageWrapper
        {
            private List<string> _arg=new List<string>() ;
           
            public MessageWrapper(List<string> _ARG)
            {
                _arg = _ARG;
              
            }
            public List<string> Arguments
            {
                get { return this._arg; }
            }
         
        }
    }
}
