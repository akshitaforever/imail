﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;

namespace IMAIL.Models
{
    public class MailHeader : BaseEntity
    {
       


        public List<EMailAddress> From
        {
            get;
            private set;
        }
        public List<EMailAddress> To
        {
            get;
            private set;
        }
        public string Date
        {
            get;
            private set;
        }
        public string Subject
        {
            get;
            private set;
        }
        public string MessageID { get;private set; }
        public MailHeader(List<MailAddress> from_address, List<MailAddress> to_address, string _date, string subject, string message_id)
        {
            this.Date = _date;
            this.From = from_address.Select(p => new EMailAddress() { Email = p.Address, DisplayName = p.DisplayName }).ToList();
            this.To = to_address.Select(p => new EMailAddress() { Email = p.Address, DisplayName = p.DisplayName }).ToList();
            this.Subject = subject;
            this.MessageID = message_id;
        }
        public MailHeader(string headerstring)
        {
            string[] str = headerstring.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            List<string> lstitems = new List<string>();
            for (int i = 0; i < str.Length; i++)
            {
                string substring = "";
                string _str = "";
                int l_index = -1;
                str[i] = str[i].Trim();
                if (str[i].StartsWith("To") || str[i].StartsWith("From") || str[i].StartsWith("Date") || str[i].StartsWith("Subject") || str[i].StartsWith("Message-ID"))
                {
                    List<EMailAddress> _addresses = new List<EMailAddress>();
                    if (str[i].StartsWith("To"))
                    {

                        l_index = parseaddresses(str, i, l_index, ref _addresses);
                        this.To = _addresses;
                    }
                    else if (str[i].StartsWith("From"))
                    {
                        _addresses.Clear();
                        l_index = parseaddresses(str, i, l_index, ref _addresses);
                        this.From = _addresses;
                    }
                    else
                    {
                        _str = str[i];
                        if (_str.Trim().Length > 1)
                        {
                            lstitems.Add(_str.Trim());

                        }
                        if (substring.Trim().Length > 0)
                        {

                            lstitems.Add(substring.Trim());
                        }

                    }
                }
            }
            foreach (string s in lstitems)
            {
                string _temp = "";
                if (s.StartsWith("Date"))
                {
                    int index = s.IndexOf(':');

                    Date = s.Substring(index + 1);
                }

                if (s.StartsWith("Subject"))
                {
                    int index = s.IndexOf(':');

                    Subject = s.Substring(index + 1);
                }
                if (s.StartsWith("Message-ID"))
                {
                    int index = s.IndexOf(':');

                    MessageID = s.Substring(index + 1);
                }

            }
        }

        private int parseaddresses(string[] str, int i, int l_index, ref List<EMailAddress> lst)
        {
            string _display_name = "";
            string _address = "";
            string _temp2;
            int index = str[i].IndexOf(':');
            if (index >= 0)
            {
                string _temp = str[i].Substring(index + 1);
                index = -1;
                while (true)
                {
                    index = _temp.IndexOf(',', index + 1);
                    if (index == -1)
                    {
                        if (l_index > 0)
                        {
                            _temp = _temp.Substring(l_index + 1);
                            _parse_address(ref _display_name, ref _address, l_index, _temp, ref lst);
                        }
                        break;
                    }
                    else
                    {
                        if (index > 0)
                        {
                            _temp2 = _temp.Substring(0, index);
                            _parse_address(ref _display_name, ref _address, index, _temp2, ref lst);
                        }
                    }
                    l_index = index;

                }
            }
            return l_index;
        }

        private void _parse_address(ref string _display_name, ref string _address, int index, string _temp, ref List<EMailAddress> lst)
        {
            string _temp1 = _temp;


            string _temp2 = "";
            int s_index = getFirstSpace(_temp1.Trim());
            if (s_index >= 0)
            {
                _display_name = _temp1.Substring(0, s_index + 1);
                _address = _temp1.Substring(s_index + 1).Trim();
            }
            else
            {
                _address = _temp1.Trim();
                _display_name = "";

            }
            _address = _address.Replace("<", "");
            _address = _address.Replace(">", "");
            EMailAddress m = null;
            if (_address != null && _display_name != null)
            {
                if (_address.Length > 0 && _display_name.Length > 0)
                {
                    try
                    {
                        m = new EMailAddress() { Email = _address.Trim(), DisplayName = _display_name };
                        lst.Add(m);
                    }
                    catch
                    {
                    }
                }
            }
            m = null;
        }

        private int getFirstSpace(string str)
        {
            int index = -1;
            Boolean isdoublequote = false;
            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] == '"')
                {
                    if (isdoublequote == true)
                    {
                        isdoublequote = false;
                    }
                    else
                    {
                        isdoublequote = true;

                    }

                }
                if (str[i] == ' ')
                {
                    if (isdoublequote == false)
                    {
                        index = i;
                        break;
                    }
                }
            }
            return index;
        }

    }
}
