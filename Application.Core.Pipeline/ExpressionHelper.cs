﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Pipeline
{
    public static class ExpressionHelper
    {
        public static IEnumerable<PropertyInfo> GetProperties<T, TReturn>(Expression<Func<T, TReturn>> propertyExpression)
        {
            return GetProperties(propertyExpression.Body);
        }

        private static IEnumerable<PropertyInfo> GetProperties(Expression expression)
        {
            var memberExpression = expression as MemberExpression;
            if (memberExpression == null) yield break;

            var property = memberExpression.Member as PropertyInfo;
            if (property == null)
            {
                throw new Exception("Expression is not a property accessor");
            }
            foreach (var propertyInfo in GetProperties(memberExpression.Expression))
            {
                yield return propertyInfo;
            }
            yield return property;
        }
        public static IEnumerable<T> Intersperse<T>(this IEnumerable<T> items, T separator)
        {
            var first = true;
            foreach (var item in items)
            {
                if (first) first = false;
                else
                {
                    yield return separator;
                }
                yield return item;
            }
        }
        public static string Concat(this IEnumerable<string> items)
        {
            return items.Aggregate("", (agg, item) => agg + item);
        }
        public static string GetPropertyNameFromPropertyExpression<T, TReturn>(Expression<Func<T, TReturn>> propertyExpression)
        {
            return GetProperties<T, TReturn>(propertyExpression)
                .Select(property => property.Name)
                .Intersperse(".")
                .Concat();
        }
    }
}
