﻿using Pipeline.ActionDependency.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pipeline.ActionDependency
{
    public class DynamicDependency : AbstractDependency, IDependencyRegistry
    {
        private ObjectRegistry reg = null;

        private SortedList<int, IDependencyEntry> _actions = new SortedList<int, IDependencyEntry>();

        public ErrorInfo LastError { get; set; }

        public bool StopOnFirstFailure { get; set; }
        public bool DependencySucceeded { get; private set; }
        public DynamicDependency()
        {

        }
        public DynamicDependency(ObjectRegistry objreg)
        {
            this.reg = objreg;
        }

        public int PushVoidDependency(Action action, bool executeparallal = false)
        {
            _actions.Add(_actions.Count + 1, new VoidDependencyEntry() { DependencyAction = action });

            this.IsParallal = executeparallal;
            return _actions.Keys.Last();
        }

        public void Execute()
        {

            if (IsParallal)
            {
                var temp = _actions.Select(s => s.Value).ToList();
                _actions.Clear();
                Parallel.ForEach<IDependencyEntry>(temp, p =>
                {
                    try
                    {
                        p.Execute();
                    }
                    catch (Exception ex)
                    {
                        LastError = new ErrorInfo() { HasError = true, LastException = ex, Method = p.GetMethod() };
                        this.DependencySucceeded = false;
                    }
                });
            }
            else
            {
                int i = 0;
                while (_actions.Count > 0)
                {
                    var action = _actions[i];
                    try
                    {
                        if ((StopOnFirstFailure && LastError != null) || !StopOnFirstFailure)
                        {
                            _actions.Remove(i);
                            action.Execute();
                        }
                        else
                        {
                            break;
                        }
                    }
                    catch (Exception ex)
                    {
                        LastError = new ErrorInfo() { HasError = true, LastException = ex, Method = action.GetMethod() };
                        this.DependencySucceeded = false;
                    }
                    finally
                    {
                        //clean up list if any
                        _actions.Clear();
                    }
                }
            }
        }

        //private Object HandleExecute(IDependencyEntry _entry)
        //{
        //    if(_entry is IValueDependencyEntry)
        //    {
        //        Type returnType = _entry.GetType().GetGenericArguments()[0];
        //       return (_entry as ValueDependencyEntry).ExecuteValue();

        //    }
        //    else if(_entry is IValueReturnDependencyEntry)
        //    {
        //        Type argType = _entry.GetType().GetGenericArguments()[0];
        //        Type returnType = _entry.GetType().GetGenericArguments()[1];

        //    }
        //    _entry.Execute();
        //}




        public IAsyncResult ExecuteAsync(AsyncCallback callback, object state)
        {

            Action temp = new Action(delegate
            {
                int i = 0;
                while (_actions.Count > 0)
                {
                    var action = _actions[i];
                    try
                    {
                        if ((StopOnFirstFailure && LastError != null) || !StopOnFirstFailure)
                        {
                            _actions.Remove(i);
                            action.Execute();
                        }
                        else
                        {
                            break;
                        }
                    }
                    catch (Exception ex)
                    {
                        LastError = new ErrorInfo() { HasError = true, LastException = ex, Method = action.GetMethod() };
                        this.DependencySucceeded = false;
                    }
                    finally
                    {
                        //clean up list if any
                        _actions.Clear();
                    }
                }


            });
            return temp.BeginInvoke(callback, state);



        }
     


    }
}
