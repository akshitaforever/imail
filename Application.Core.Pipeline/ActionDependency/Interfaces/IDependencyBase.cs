﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Pipeline.ActionDependency.Interfaces
{
    public interface IDependencyBaseEntry
    {
        MethodInfo GetMethod();
    }
}
