﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pipeline.ActionDependency.Interfaces
{
    public interface IDependencyWithParamEntry : IDependencyEntry
    {
        void Execute(Object args);
        IAsyncResult ExecuteAsync(Object args, AsyncCallback callback, object state);
    }
}
