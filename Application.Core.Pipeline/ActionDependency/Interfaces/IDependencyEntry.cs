﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Pipeline.ActionDependency.Interfaces
{

    public interface IDependencyEntry : IDependencyBaseEntry
    {
        void Execute();
        IAsyncResult ExecuteAsync(AsyncCallback callback, object state);
    }
}