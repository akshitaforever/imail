﻿using Pipeline.ActionDependency.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pipeline.ActionDependency
{
    public class ValueDependencyEntry<T> : IValueDependencyEntry<T>
    {
        public Func<T> DependencyAction { get; set; }
      
        public T ExecuteValue()
        {
            return DependencyAction.Invoke();
        }

        public IAsyncResult ExecuteValueAsync( AsyncCallback callback, object state)
        {
            return DependencyAction.BeginInvoke(callback, state);
        }
        public void Execute()
        {
            DependencyAction.Invoke();
        }

        public IAsyncResult ExecuteAsync(AsyncCallback callback, object state)
        {
            return DependencyAction.BeginInvoke(callback, state);
        }

        public object GetArgs()
        {
            return null;
        }

        public System.Reflection.MethodInfo GetMethod()
        {
            return DependencyAction.Method;
        }
    }
    public class ValueDependencyEntry<T1, T2> : IValueDependencyEntry<T1,T2>
    {
        public Func<T1, T2> DependencyAction { get; set; }
        public T1 arg { get; set; }
        public T2 ExecuteValue(T1 arg)
        {
            return DependencyAction.Invoke(arg);
        }

    


        public IAsyncResult ExecuteValueAsync(T1 arg, AsyncCallback callback, object state)
        {
            return DependencyAction.BeginInvoke(arg,callback, state);
        }

        public void Execute()
        {
            throw new NotImplementedException();
        }

        public IAsyncResult ExecuteAsync(AsyncCallback callback, object state)
        {
            throw new NotImplementedException();
        }

        public object GetArgs()
        {
            return arg;
        }

        public System.Reflection.MethodInfo GetMethod()
        {
            return DependencyAction.Method;
        }
    }
    public class ValueDependencyEntry : IValueDependencyEntry
    {
        public Func<Object> DependencyAction { get; set; }
        public Object arg { get; set; }
        public Object ExecuteValue()
        {
            return DependencyAction.Invoke();
        }

        public IAsyncResult ExecuteValueAsync(AsyncCallback callback, object state)
        {
            return DependencyAction.BeginInvoke(callback, state);
        }











        public void Execute()
        {
            DependencyAction.Invoke();
        }

        public IAsyncResult ExecuteAsync(AsyncCallback callback, object state)
        {
            return DependencyAction.BeginInvoke(callback, state);
        }

        public object GetArgs()
        {
            return arg;
        }

        public System.Reflection.MethodInfo GetMethod()
        {
            return DependencyAction.Method;
        }
    }
}
