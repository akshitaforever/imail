﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Pipeline.ActionDependency
{
    public class ObjectRegistry
    {
        private Dictionary<ObjectEntry, ObjectLock> _entries = new Dictionary<ObjectEntry, ObjectLock>();
        public void AddObject(Object o, Pipeline.ActionDependency.ObjectEntry.ObjectLockType locktype)
        {
            if (_entries.Keys.Any(p => p._instance.GetType() == o.GetType()))
            {
                throw new ArgumentException("Object entry for same type already exists");
            }
            ObjectEntry _entry = new ObjectEntry()
            {
                _instance = o
            };
            ObjectLock lck = new ObjectLock() { IsActive = false, LockType = locktype };
            _entries.Add(_entry, lck);
        }
        public Object GetInstance<T>()
        {
            if (ObjectExists<T>())
            {
                var objentry = _entries.Keys.Single(p => p._instance.GetType() == typeof(T));
                //get current lock
                var lck = _entries[objentry];
                if (lck.LockType == Pipeline.ActionDependency.ObjectEntry.ObjectLockType.Instance)
                {
                    if (lck.IsActive)
                    {
                        return null;
                    }
                    else
                    {
                        //acquire lock
                        lck.IsActive = true;
                        _entries[objentry] = lck;
                        return objentry._instance;
                    }
                }
                else
                {
                    return null;
                }
            }
            return null;
        }
        public bool ObjectExists<T>()
        {
            return _entries.Any(p => p.Key._instance.GetType() == typeof(T));
        }
        private T GetObject<T>() where T:class
        {
            if (ObjectExists<T>())
            {
                return _entries.Keys.Single(p => p._instance.GetType() == typeof(T))._instance as T;
            }
            return null;
        }
      
        private void UpdateObjectExclusive<T, TReturn>(Expression<Func<T, TReturn>> expression, Object value) where T : class
        {

            if (ObjectExists<T>())
            {
                if (HasValidLockState<T>(ObjectEntry.ObjectOperation.Write))
                {
                    string propertyname = ExpressionHelper.GetPropertyNameFromPropertyExpression<T, TReturn>(expression);
                    PropertyInfo pinfo = GetProperty(typeof(T), propertyname);
                    lock (this)
                    {
                        bool objnew = false;
                        bool locktaken = false;
                        //   Mutex mObjectmutex = new Mutex(true, GetObject<T>().GetHashCode().ToString(), out objnew);

                        SpinLock lck = new SpinLock(true);
                        try
                        {
                            lck.Enter(ref locktaken);
                            var objkey = _entries.Keys.Single(p => p._instance.GetType() == typeof(T));
                            var objext = GetObject<T>() as T;
                            pinfo.SetValue(objext, value);
                            objkey._instance = objext;
                        }
                        finally
                        {
                            if (locktaken)
                            {
                                lck.Exit();
                            }
                        }



                    }

                }
            }

        }
        private void UpdateObject<T, TReturn>(Expression<Func<T, TReturn>> expression, Object value) where T : class
        {
            if (ObjectExists<T>())
            {
                if (HasValidLockState<T>(ObjectEntry.ObjectOperation.Write))
                {
                    string propertyname = ExpressionHelper.GetPropertyNameFromPropertyExpression<T, TReturn>(expression);
                    PropertyInfo pinfo = GetProperty(typeof(T), propertyname);
                    //   Mutex mObjectmutex = new Mutex(true, GetObject<T>().GetHashCode().ToString(), out objnew);
                    try
                    {
                        var objkey = _entries.Keys.Single(p => p._instance.GetType() == typeof(T));
                        var objext = GetObject<T>() as T;
                        pinfo.SetValue(objext, value);
                        objkey._instance = objext;
                    }
                    catch { }
                }
            }
        }
        private ObjectEntry GetObjectEntry<T>()
        {
            if (ObjectExists<T>())
            {
                return _entries.Keys.Single(p => p._instance.GetType() == typeof(T));
            }
            return null;
        }
        private ObjectLock GetLock<T>()
        {
            if (ObjectExists<T>())
            {
                var objentry = _entries.Keys.Single(p => p._instance.GetType() == typeof(T));
                //get current lock
                return _entries[objentry];
            }
            return null;
        }
        private bool SetLock<T>(Pipeline.ActionDependency.ObjectEntry.ObjectLockType lck)
        {
            try
            {
                if (ObjectExists<T>())
                {
                    var objentry = _entries.Keys.Single(p => p._instance.GetType() == typeof(T));
                    //get current lock
                    _entries[objentry].LockType = lck;
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return false;

        }
        public TReturn GetValueExclusive<T, TReturn>(Expression<Func<T, TReturn>> expression) where T : class
        {
            if (HasValidLockState<T>(ObjectEntry.ObjectOperation.Read))
            {
                if (ObjectExists<T>())
                {
                    string propertyname = ExpressionHelper.GetPropertyNameFromPropertyExpression<T, TReturn>(expression);
                    PropertyInfo pinfo = GetProperty(typeof(T), propertyname);
                    lock (this)
                    {
                        bool objnew = false;
                        bool locktaken = false;
                        //   Mutex mObjectmutex = new Mutex(true, GetObject<T>().GetHashCode().ToString(), out objnew);

                        SpinLock lck = new SpinLock(true);
                        try
                        {
                            lck.Enter(ref locktaken);
                            var value = (TReturn)pinfo.GetValue(GetObject<T>() as T);
                            return value;
                        }
                        finally
                        {
                            if (locktaken)
                            {
                                lck.Exit();
                            }
                        }



                    }

                }
            }
            return default(TReturn);

        }
        public TReturn GetValue<T, TReturn>(Expression<Func<T, TReturn>> expression) where T : class
        {
            if (HasValidLockState<T>(ObjectEntry.ObjectOperation.Read))
            {
                if (ObjectExists<T>())
                {
                    string propertyname = ExpressionHelper.GetPropertyNameFromPropertyExpression<T, TReturn>(expression);
                    PropertyInfo pinfo = GetProperty(typeof(T), propertyname);

                    try
                    {
                        var value = (TReturn)pinfo.GetValue(GetObject<T>() as T);
                        return value;
                    }
                    catch { }

                }
            }
            return default(TReturn);
        }
        private IAsyncResult GetValueAsync<T>(AsyncCallback callback, object state) where T : class
        {
            Func<T> action = GetObject<T>;
            return action.BeginInvoke(callback, state);

        }
        public void SetValueExclusive<T, TValue>(Expression<Func<T, TValue>> expression, Object Value) where T : class
        {
            UpdateObjectExclusive<T, TValue>(expression, Value);
        }
        public void SetValue<T, TValue>(Expression<Func<T, TValue>> expression, Object Value) where T : class
        {

            UpdateObject<T, TValue>(expression, Value);

        }
        public IAsyncResult SetValueAsync<T, TValue>(Expression<Func<T, TValue>> expression, Object Value,AsyncCallback callback,object state=null) where T : class
        {
            Action<Expression<Func<T, TValue>>, T> action = UpdateObject<T, TValue>;
            return action.BeginInvoke(expression, Value as T, callback, state);

        }
        private bool HasValidLockState<T>(Pipeline.ActionDependency.ObjectEntry.ObjectOperation operation) where T : class
        {
            var lck = GetLock<T>();
            switch (lck.LockType)
            {
                case ObjectEntry.ObjectLockType.Instance:
                    return !lck.IsActive;
                    break;
                case ObjectEntry.ObjectLockType.ReadOnly:
                    return !lck.IsActive && operation == ObjectEntry.ObjectOperation.Read;
                    break;
                case ObjectEntry.ObjectLockType.WriteOnly:
                    return !lck.IsActive && operation == ObjectEntry.ObjectOperation.Write;
                    break;
                case ObjectEntry.ObjectLockType.ReadWrite:
                    return !lck.IsActive;
                    break;
                case ObjectEntry.ObjectLockType.DenyAll:
                    return false;
                    break;
                case ObjectEntry.ObjectLockType.AccessWithToken: return false;//not implemented
                    break;
                default:
                    break;
            }
            return false;
        }
        private PropertyInfo GetProperty(Type propertytype, string propertyname)
        {
            Type targetType = propertytype.GetType();
            if (propertyname.Contains("."))
            {
                string temp = propertyname.Split('.')[0];
                propertyname = propertyname.Substring(propertyname.IndexOf("."));
                return GetProperty(targetType.GetProperty(temp).PropertyType, propertyname);
            }
            else
            {
                return targetType.GetProperty(propertyname);
            }
        }

    }
}
