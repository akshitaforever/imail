﻿using Pipeline.ActionDependency.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pipeline.ActionDependency
{
    public class DependencyEntry : IDependencyEntry
    {
        private Action DependencyAction { get; set; }
        public DependencyEntry(Action method)
        {
            this.DependencyAction = method;
        }
        public void Execute()
        {
            DependencyAction();
        }

        public IAsyncResult ExecuteAsync(AsyncCallback callback, object state)
        {
            return DependencyAction.BeginInvoke(callback, state);
        }

        public System.Reflection.MethodInfo GetMethod()
        {
            return DependencyAction.Method;
        }
    }

    public class DependencyEntry<T, TReturn> : IValueDependencyEntry<T, TReturn>
    {
        public Func<T, TReturn> DependencyAction { get; set; }

        public TReturn ExecuteValue(T args)
        {
            return DependencyAction(args);
        }
        public IAsyncResult ExecuteValueAsync(T arg, AsyncCallback callback, object state)
        {
            return DependencyAction.BeginInvoke(arg, callback, state);
        }

        public void Execute()
        {
          //  DependencyAction();
        }

        public IAsyncResult ExecuteAsync(AsyncCallback callback, object state)
        {
            throw new Exception();
          // return DependencyAction.BeginInvoke(arg, callback, state);
        }

        public object GetArgs()
        {
            return null;
        }

        public System.Reflection.MethodInfo GetMethod()
        {
            return DependencyAction.Method;
        }
    }
}
