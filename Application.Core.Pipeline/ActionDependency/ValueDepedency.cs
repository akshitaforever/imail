﻿using Pipeline.ActionDependency.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pipeline.ActionDependency;
using System.Reflection;
namespace Pipeline.ActionDependency
{
    public class ValueDependency<T, TReturn> : AbstractDependency, IDependencyRegistry where T : class
    {

        private ObjectRegistry reg = null;
        private LinkedList<IValueDependencyEntry> _actions = new LinkedList<IValueDependencyEntry>();
        private Dictionary<int, ActionData> _data = new Dictionary<int, ActionData>();
        public ErrorInfo LastError { get; private set; }
        public bool DependencySucceeded { get; private set; }
        public TReturn LastResult { get; private set; }
        public ValueDependency()
        {
            LastError = new ErrorInfo();
        }
        public ValueDependency(ObjectRegistry objreg)
            : this()
        {
            this.reg = objreg;
        }


        public void PushValueDependency(Func<T> action, Object ValueToCompare, Enums.ActionPredicate Mode)
        {
            _actions.AddLast(new ValueDependencyEntry<T>() { DependencyAction = action });
            _data.Add(action.GetHashCode(), new ActionData()
            {
                Method = action.Method,
                Mode = Mode,
                Result = null,
                Success = false,
                ValueToCompare = ValueToCompare
            });
        }
        public void PushValueDependency(Func<T> action, Func<TReturn> ObjectMethod, Enums.ActionPredicate Mode)
        {
            PushValueDependency(action, ObjectMethod(), Mode);
        }
        public ActionData PushParamValueDependency(Func<T, TReturn> action, T arg, Object ValueToCompare, Enums.ActionPredicate Mode)
        {
            _actions.AddLast(new ValueDependencyEntry<T, TReturn>() { arg = arg, DependencyAction = action });
            var _actionpredicate=new ActionData()
            {
                Method = action.Method,
                Mode = Mode,
                Result = null,
                Success = false,
                ValueToCompare = ValueToCompare
            };
            _data.Add(action.GetHashCode(), _actionpredicate);
            return _actionpredicate;
        }
        public void PushParamValueDependency(Func<T, TReturn> action, T arg, Func<TReturn> ObjectMethod, Enums.ActionPredicate Mode)
        {
            PushParamValueDependency(action, arg, ObjectMethod(), Mode);
        }
        public void Execute()
        {
            bool success = true;
            Object tempresult = null;
            using (var enu = _actions.GetEnumerator())
            {
                while (enu.MoveNext())
                {
                    var action = enu.Current;


                    try
                    {

                        if (action is IValueDependencyEntry<T>)
                        {
                            tempresult = (action as IValueDependencyEntry<T>).ExecuteValue();
                        }
                        else if (action is IValueDependencyEntry<T, TReturn>)
                        {
                            tempresult = (action as IValueDependencyEntry<T, TReturn>).ExecuteValue(action.GetArgs() as T);
                        }

                    }
                    catch (Exception ex)
                    {
                        success = false;
                        LastError = new ErrorInfo() { HasError = true, LastException = ex, Method = action.GetMethod() };
                        break;
                    }
                    finally
                    {
                        var _adata = _data[action.GetHashCode()];
                        _adata.Result = tempresult;
                        switch (_adata.Mode)
                        {
                            case Enums.ActionPredicate.CompareValue:
                                if (success && tempresult == _adata.ValueToCompare)
                                {
                                    _adata.Success = true;
                                }
                                break;
                            case Enums.ActionPredicate.NoException:
                                if (success)
                                {
                                    _adata.Success = true;
                                }

                                break;
                        }
                        _data[action.GetHashCode()] = _adata;
                    }


                }
                this.DependencySucceeded = _data.Select(p => p.Value.Success).Aggregate((x, y) => x && y);
                this.LastResult = (TReturn)tempresult;
            }
        }






        public IAsyncResult ExecuteAsync(AsyncCallback callback, object state)
        {
            bool success = true;
            Object tempresult = null;
            Func<TReturn> temp = new Func<TReturn>(delegate
            {
                using (var enu = _actions.GetEnumerator())
                {
                    while (enu.MoveNext())
                    {
                        var action = enu.Current;


                        try
                        {

                            if (action is IValueDependencyEntry<T>)
                            {
                                tempresult = (action as IValueDependencyEntry<T>).ExecuteValue();
                            }
                            else if (action is IValueDependencyEntry<T, TReturn>)
                            {
                                tempresult = (action as IValueDependencyEntry<T, TReturn>).ExecuteValue(action.GetArgs() as T);
                            }

                        }
                        catch (Exception ex)
                        {
                            success = false;
                            LastError = new ErrorInfo() { HasError = true, LastException = ex, Method = action.GetMethod() };
                            break;
                        }
                        finally
                        {
                            var _adata = _data[action.GetHashCode()];
                            _adata.Result = tempresult;
                            switch (_adata.Mode)
                            {
                                case Enums.ActionPredicate.CompareValue:
                                    if (success && tempresult == _adata.ValueToCompare)
                                    {
                                        _adata.Success = true;
                                    }
                                    break;
                                case Enums.ActionPredicate.NoException:
                                    if (success)
                                    {
                                        _adata.Success = true;
                                    }

                                    break;
                            }
                            _data[action.GetHashCode()] = _adata;
                        }


                    }
                    this.DependencySucceeded = _data.Select(p => p.Value.Success).Aggregate((x, y) => x && y);
                    this.LastResult = (TReturn)tempresult;
                }
                return this.LastResult;

            });
            return temp.BeginInvoke(callback, state);



        }



    }
    public class ValueDependency<T> : AbstractDependency, IDependencyRegistry where T : class
    {
        private ObjectRegistry reg = null;
        private LinkedList<IValueDependencyEntry> _actions = new LinkedList<IValueDependencyEntry>();
        private Dictionary<int, ActionData> _data = new Dictionary<int, ActionData>();
        public ErrorInfo LastError { get; private set; }
        public bool DependencySucceeded { get; private set; }
        public T LastResult { get; private set; }
        public ValueDependency()
        {
            LastError = new ErrorInfo();
        }
        public ValueDependency(ObjectRegistry objreg)
            : this()
        {
            this.reg = objreg;
        }


        public void PushValueDependency(Func<T> action, Object ValueToCompare, Enums.ActionPredicate Mode)
        {
            _actions.AddLast(new ValueDependencyEntry<T>() { DependencyAction = action });
            _data.Add(action.GetHashCode(), new ActionData()
            {
                Method = action.Method,
                Mode = Mode,
                Result = null,
                Success = false,
                ValueToCompare = ValueToCompare
            });
        }
        public void PushValueDependency(Func<T> action, Func<T> ObjectMethod, Enums.ActionPredicate Mode)
        {
            PushValueDependency(action, ObjectMethod(), Mode);
        }

        public void Execute()
        {
            bool success = true;
            Object tempresult = null;
            using (var enu = _actions.GetEnumerator())
            {
                while (enu.MoveNext())
                {
                    var action = enu.Current;


                    try
                    {

                        if (action is IValueDependencyEntry<T>)
                        {
                            tempresult = (action as IValueDependencyEntry<T>).ExecuteValue();
                        }


                    }
                    catch (Exception ex)
                    {
                        success = false;
                        LastError = new ErrorInfo() { HasError = true, LastException = ex, Method = action.GetMethod() };
                        break;
                    }
                    finally
                    {
                        var _adata = _data[action.GetHashCode()];
                        _adata.Result = tempresult;
                        switch (_adata.Mode)
                        {
                            case Enums.ActionPredicate.CompareValue:
                                if (success && tempresult == _adata.ValueToCompare)
                                {
                                    _adata.Success = true;
                                }
                                break;
                            case Enums.ActionPredicate.NoException:
                                if (success)
                                {
                                    _adata.Success = true;
                                }

                                break;
                        }
                        _data[action.GetHashCode()] = _adata;
                    }


                }
                this.DependencySucceeded = _data.Select(p => p.Value.Success).Aggregate((x, y) => x && y);
                this.LastResult = (T)tempresult;
            }




        }






        public IAsyncResult ExecuteAsync(AsyncCallback callback, object state)
        {

            Func<T> temp = new Func<T>(delegate
            {

                try
                {
                    Execute();
                    return this.LastResult;
                }
                catch (Exception)
                {

                    this.DependencySucceeded = false;
                    return default(T);
                }


            });
            return temp.BeginInvoke(callback, state);



        }
   



    }
}
