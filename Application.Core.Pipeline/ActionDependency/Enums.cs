﻿using Pipeline.ActionDependency.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Pipeline.ActionDependency
{
    public class Enums
    {
        public enum ActionPredicate
        {
            CompareValue,
            NoException
        }

    }
    public class ActionData 
    {
        public Pipeline.ActionDependency.Enums.ActionPredicate Mode { get; set; }
        public Object Result { get; set; }
        public bool Success { get; set; }
        public MethodInfo Method { get; set; }
        public Object ValueToCompare { get; set; }
        public ActionData ExecuteWithPreviousResultValue<T>(T value)
        {
            ValueToCompare = value;
            Mode = Enums.ActionPredicate.CompareValue;
            return this;
        }
        public ActionData ExecuteWithPreviousResultPredicate<T>(Func<T> method)
        {
            Mode = Enums.ActionPredicate.CompareValue;
            ValueToCompare = method();
            return this;
        }
        public ActionData ExecuteWhenNoPreviousException()
        {
            Mode = Enums.ActionPredicate.NoException;
            return this;
        }
       
      
    }
   
    public class ErrorInfo
    {
        public Exception LastException { get; set; }
        public MethodInfo Method { get; set; }
        public bool HasError { get; set; }
    }
  
}
