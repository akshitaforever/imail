﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pipeline.ActionDependency
{
    public class ObjectEntry
    {
        public Object _instance { get; set; }
        public enum ObjectLockType
        {
            Instance,
            ReadOnly,
            WriteOnly,
            ReadWrite,
            DenyAll,
            AccessWithToken
        }
        public enum ObjectOperation
        {
            Read, Write
        }
    }
}
